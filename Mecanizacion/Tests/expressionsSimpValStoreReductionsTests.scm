#lang racket
(require redex
         "../grammar.scm"
         "../Reductions/expressionsSimpValStoreReductions.scm")

(define (exp-sv-store-red-test-suite)
  ; Implicit dereferencing
  (test-->> core-lang-sv-store-red
            (term (((\( (svr 1) \, 2 \))) \; (svr 1)))
            (term (((\( (svr 1) \, 2 \))) \; 2)))
  
  (test-->> core-lang-sv-store-red
            (term (((\( (svr 1) \, 1 \))
                    (\( (svr 2) \, 2 \))
                    (\( (svr 3) \, 3 \))
                    (\( (svr 4) \, 4 \))) \; (svr 3)))
            (term (((\( (svr 1) \, 1 \))
                    (\( (svr 2) \, 2 \))
                    (\( (svr 3) \, 3 \))
                    (\( (svr 4) \, 4 \))) \; 3)))
  (test-results))

(provide exp-sv-store-red-test-suite)