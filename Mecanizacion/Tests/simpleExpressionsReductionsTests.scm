#lang racket
; Black-box testing for expression that don't interact with some store

(require redex
         "../grammar.scm"
         "../Reductions/simpleExpressionsReductions.scm")

(define (expressions-red-test-suite)
  ; Tuples
  ; Truncate
  (test-->> core-lang-expressions-red
            (term (\( (< (1 2 3) >) \)))
            (term 1))
  
  (test-->> core-lang-expressions-red
            (term ((objr 1) \( ((< (1 2) >) 3) \)))
            (term ((objr 1) \( (1 3) \))))
  
  (test-->> core-lang-expressions-red
            (term (< ((< (1 2) >) 3) >))
            (term (< (1 3) >)))
  
  (test-->> core-lang-expressions-red
            (term (\{ (< (2 3) >) (\[ 1 \] = 1) \}))
            (term (\{ 2 (\[ 1 \] = 1) \})))
  
  (test-->> core-lang-expressions-red
            (term (local (X) = ((< (1 2) >) 3) in void end))
            (term (local (X) = (1 3) in void end)))
  
  (test-->> core-lang-expressions-red
            (term (((svr 1)) = ((< (1 2) >) 3)))
            (term (((svr 1)) = (1 3))))
  
  (test-->> core-lang-expressions-red
            (term (break X (< ((< (1 2) >) 3) >)))
            (term (break X (< (1 3) >))))
  ; Unwrap
  (test-->> core-lang-expressions-red
            (term ((objr 1) \( (1 (< (2 3) >)) \)))
            (term ((objr 1) \( (1 2 3) \))))
  
  (test-->> core-lang-expressions-red
            (term (< (1 (< (2 3) >)) >))
            (term (< (1 2 3) >)))
  
  (test-->> core-lang-expressions-red
            (term (\{ (\[ 1 \] = 1) (< (2 3) >) \}))
            (term (\{ (\[ 1 \] = 1) 2 3 \})))
  
  (test-->> core-lang-expressions-red
            (term (local (X) = (1 (< (2 3) >)) in void end))
            (term (local (X) = (1 2 3) in void end)))
  
  (test-->> core-lang-expressions-red
            (term (((svr 1)) = (1 (< (2 3) >))))
            (term (((svr 1)) = (1 2 3))))
  
  (test-->> core-lang-expressions-red
            (term (break X (< (1 (< (2 3) >)) >)))
            (term (break X (< (1 2 3) >))))
  ; Operator ()
  (test-->> core-lang-expressions-red
            (term (\( (< (1 2 3) >) \)))
            1)
  (test-->> core-lang-expressions-red
            (term (\( empty \)))
            (term nil))
  (test-->> core-lang-expressions-red
            (term (\( void \)))
            (term nil))
  (test-->> core-lang-expressions-red
            (term (\( 1 \)))
            (term 1))
  
  ; Method call
  (test-->> core-lang-expressions-red
            (term ((objr 1) : method \( (1) \)))
            (term (((objr 1) \[ "method" \]) \( ((objr 1) 1) \))))
  
  (test-->> core-lang-expressions-red
            (term ((objr 1) : method \( () \)))
            (term (((objr 1) \[ "method" \]) \( ((objr 1)) \))))
 
  ; Arithmetic Operations
  (test-->> core-lang-expressions-red
            (term (1 + 1))
            (term 2))
  (test-->> core-lang-expressions-red
            (term (1 - 1))
            (term 0))
  (test-->> core-lang-expressions-red
            (term (1 * 1))
            (term 1))
  (test-->> core-lang-expressions-red
            (term (1 / 1))
            (term 1))
  (test-->> core-lang-expressions-red
            (term (1 ^ 1))
            (term 1))
  (test-->> core-lang-expressions-red
            (term (1 % 1))
            (term 0))
  ; Equality comparison
  (test-->> core-lang-expressions-red
            (term (1 == 1))
            (term true))
  (test-->> core-lang-expressions-red
            (term ("a" == "a"))
            (term true))
  ; Number order comparison
  (test-->> core-lang-expressions-red
            (term (1 < 2))
            (term true))
  (test-->> core-lang-expressions-red
            (term (2 < 1))
            (term false))
  (test-->> core-lang-expressions-red
            (term (2 <= 1))
            (term false))
  (test-->> core-lang-expressions-red
            (term (1 <= 2))
            (term true))
   ; String order comparison
  (test-->> core-lang-expressions-red
            (term ("a" < "a"))
            (term false))
  (test-->> core-lang-expressions-red
            (term ("a" < "b"))
            (term true))
  (test-->> core-lang-expressions-red
            (term ("a" <= "a"))
            (term true))
  (test-->> core-lang-expressions-red
            (term ("a" <= "b"))
            (term true))
  ; String concatenation
  (test-->> core-lang-expressions-red
            (term ("a" .. "b"))
            (term "ab"))
  (test-->> core-lang-expressions-red
            (term ("" .. "b"))
            (term "b"))
  ; String length
  (test-->> core-lang-expressions-red
            (term (\# "a"))
            (term 1))
  ; Logical conectives
  (test-->> core-lang-expressions-red
            (term (1 and (X \( () \))))
            (term (\( (X \( () \)) \))))
  (test-->> core-lang-expressions-red
            (term (nil and 2))
            (term nil))
  (test-->> core-lang-expressions-red
            (term (false and 2))
            (term false))
  (test-->> core-lang-expressions-red
            (term (1 or 2))
            (term 1))
  (test-->> core-lang-expressions-red
            (term (false or 2))
            (term 2))
  (test-->> core-lang-expressions-red
            (term (nil or 2))
            (term 2))
  (test-->> core-lang-expressions-red
            (term (not 1))
            (term false))
  (test-->> core-lang-expressions-red
            (term (not nil))
            (term true))
  (test-->> core-lang-expressions-red
            (term (not false))
            (term true))
   ; Coercion
  (test-->> core-lang-expressions-red
            (term ("0x1.0p0" + 1.0))
            (term 2.0))
  (test-->> core-lang-expressions-red
            (term (1 + "0x1.0p0"))
            (term 2.0))
  (test-->> core-lang-expressions-red
            (term ("0x1.0p0" - 1))
            (term 0.0))
  (test-->> core-lang-expressions-red
            (term (1 - "0x1.0p0"))
            (term 0.0))
  (test-->> core-lang-expressions-red
            (term ("0x1.0p0" * 1))
            (term 1.0))
  (test-->> core-lang-expressions-red
            (term (1 * "0x1.0p0"))
            (term 1.0))
  (test-->> core-lang-expressions-red
            (term ("0x1.0p0" / 1))
            (term 1.0))
  (test-->> core-lang-expressions-red
            (term (1.0 / "0x1.0p0"))
            (term 1.0))
  (test-->> core-lang-expressions-red
            (term ("0x1.0p0" ^ 1.0))
            (term 1.0))
  (test-->> core-lang-expressions-red
            (term (1.0 ^ "0x1.0p0"))
            (term 1.0))
  (test-->> core-lang-expressions-red
            (term ("0x1.0p0" % 1.0))
            (term 0.0))
  (test-->> core-lang-expressions-red
            (term (1.0 % "0x1.0p0"))
            (term 0.0))
  (test-->> core-lang-expressions-red
            (term (- "0x1.0p0"))
            (term -1.0))
  (test-->> core-lang-expressions-red
            (term ("1" .. 2.0))
            (term "12"))
  (test-->> core-lang-expressions-red
            (term ("1" .. "2.0"))
            (term "12.0"))
  
  ; Abnormal expressions
  (test-->> core-lang-expressions-red
            (term ("a" + 1))
            (term (("a" + 1)AdditionWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 + "a"))
            (term ((1 + "a")AdditionWrongOperands)))
  (test-->> core-lang-expressions-red
            (term ("0xq" + 1))
            (term (("0xq" + 1)AdditionWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 + "0xq"))
            (term ((1 + "0xq")AdditionWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 + "0x1.q"))
            (term ((1 + "0x1.q")AdditionWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 + "0x1.1pq"))
            (term ((1 + "0x1.1pq")AdditionWrongOperands)))
  (test-->> core-lang-expressions-red
            (term ("a" - 1))
            (term (("a" - 1)SubstractionWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 - "a"))
            (term ((1 - "a")SubstractionWrongOperands)))
  (test-->> core-lang-expressions-red
            (term ("0xq" - 1))
            (term (("0xq" - 1)SubstractionWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 - "0xq"))
            (term ((1 - "0xq")SubstractionWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 - "0x1.q"))
            (term ((1 - "0x1.q")SubstractionWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 - "0x1.1pq"))
            (term ((1 - "0x1.1pq")SubstractionWrongOperands)))
  (test-->> core-lang-expressions-red
            (term ("a" * 1))
            (term (("a" * 1)MultiplicationWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 * "a"))
            (term ((1 * "a")MultiplicationWrongOperands)))
  (test-->> core-lang-expressions-red
            (term ("0xq" * 1))
            (term (("0xq" * 1)MultiplicationWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 * "0xq"))
            (term ((1 * "0xq")MultiplicationWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 * "0x1.q"))
            (term ((1 * "0x1.q")MultiplicationWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 * "0x1.1pq"))
            (term ((1 * "0x1.1pq")MultiplicationWrongOperands)))
  (test-->> core-lang-expressions-red
            (term ("a" ^ 1))
            (term (("a" ^ 1)ExponentiationWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 ^ "a"))
            (term ((1 ^ "a")ExponentiationWrongOperands)))
  (test-->> core-lang-expressions-red
            (term ("0xq" ^ 1))
            (term (("0xq" ^ 1)ExponentiationWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 ^ "0xq"))
            (term ((1 ^ "0xq")ExponentiationWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 ^ "0x1.q"))
            (term ((1 ^ "0x1.q")ExponentiationWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 ^ "0x1.1pq"))
            (term ((1 ^ "0x1.1pq")ExponentiationWrongOperands)))
  (test-->> core-lang-expressions-red
            (term ("a" % 1))
            (term (("a" % 1)ModuleWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 % "a"))
            (term ((1 % "a")ModuleWrongOperands)))
  (test-->> core-lang-expressions-red
            (term ("0xq" % 1))
            (term (("0xq" % 1)ModuleWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 % "0xq"))
            (term ((1 % "0xq")ModuleWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 % "0x1.q"))
            (term ((1 % "0x1.q")ModuleWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (1 % "0x1.1pq"))
            (term ((1 % "0x1.1pq")ModuleWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (- "a"))
            (term ((- "a")NegationWrongOperand)))
  (test-->> core-lang-expressions-red
            (term (- "0xq"))
            (term ((- "0xq")NegationWrongOperand)))
  (test-->> core-lang-expressions-red
            (term (- "0x1.q"))
            (term ((- "0x1.q")NegationWrongOperand)))
  (test-->> core-lang-expressions-red
            (term (- "0x1.1pq"))
            (term ((- "0x1.1pq")NegationWrongOperand)))
  (test-->> core-lang-expressions-red
            (term ("a" .. (objr 1)))
            (term (("a" .. (objr 1))StringConcatWrongOperands)))
  (test-->> core-lang-expressions-red
            (term (\# (objr 1)))
            (term ((\# (objr 1))StringLengthWrongOperand)))
  (test-->> core-lang-expressions-red
            (term ("a" == "b"))
            (term (("a" == "b")EqualityFail)))
  (test-->> core-lang-expressions-red
            (term (true == 1))
            (term ((true == 1)EqualityFail)))
  (test-->> core-lang-expressions-red
            (term ((objr 1) < (objr 1)))
            (term (((objr 1) < (objr 1))LessThanFail)))
  (test-->> core-lang-expressions-red
            (term ((objr 1) <= (objr 1)))
            (term (((objr 1) <= (objr 1))LessThanOrEqualFail)))
  
  ; E-BuiltInError
  (test-->> core-lang-expressions-red
            (term ($builtIn error \( ("error message") \)))
            (term (err "error message")))
  
  ; E-BuiltInAssertFail
  (test-->> core-lang-expressions-red
            (term ($builtIn assert \( (false nil) \)))
            
            (term (err "assertion failed!")))
  
  (test-->> core-lang-expressions-red
            (term ($builtIn assert \( (false "this assertion is false") \)))
            
            (term (err "this assertion is false")))
  
  (test-->> core-lang-expressions-red
            (term ($builtIn assert \( (nil nil) \)))
            
            (term (err "assertion failed!")))
  
  ; E-BuiltInAssertSuccess
  (test-->> core-lang-expressions-red
            (term ($builtIn assert \( (true "this assertion is true") \)))
            
            (term (< (true "this assertion is true") >)))
  
  (test-->> core-lang-expressions-red
            (term ($builtIn assert \( (1 "this assertion is true") \)))
            
            (term (< (1 "this assertion is true") >)))
  
  ; E-BuiltInTablePack
  (test-->> core-lang-expressions-red
            (term ($builtIn tablePack \( () \)))
            
            (term (< ((\{ (\[ "n" \] = 0) \})) >)))
  
  (test-->> core-lang-expressions-red
            (term ($builtIn tablePack \( (1) \)))
            
            (term (< ((\{ (\[ 1 \] = 1) (\[ "n" \] = 1) \})) >)))
  
  (test-->> core-lang-expressions-red
            (term ($builtIn tablePack \( (1 2) \)))
            
            (term (< ((\{ (\[ 1 \] = 1) (\[ 2 \] = 2) (\[ "n" \] = 2) \})) >)))
  
  ; E-BuiltInPcall
  (test-->> core-lang-expressions-red
            (term ($builtIn pcall \( ((objr 1) 1 2) \)))
            
            (term ((((objr 1) \( (1 2) \)))ProtectedMode)))
  
  ; E-BuiltInToNumber
  ; TODO: we are not taking into account the base...
  (test-->> core-lang-expressions-red
            (term ($builtIn toNumber \( (1 10) \)))
            
            (term (< (1) >)))
  
  (test-->> core-lang-expressions-red
            (term ($builtIn toNumber \( ("1" 10) \)))
            
            (term (< (1) >)))
  
  (test-->> core-lang-expressions-red
            (term ($builtIn toNumber \( ("0x1" 10) \)))
            
            (term (< (1) >)))
  
  ; E-BuiltInToString
  (test-->> core-lang-expressions-red
            (term ($builtIn toString \( (1) \)))
            
            (term (< ("1") >)))
  
  ; E-BuiltInSelectNormal
  (test-->> core-lang-expressions-red
            (term ($builtIn select \( ("#" 1 2 3) \)))
            
            (term (< (1 2 3) >)))
  
  (test-->> core-lang-expressions-red
            (term ($builtIn select \( (1 1 2 3) \)))
            
            (term (< (1 2 3) >)))
  
  (test-->> core-lang-expressions-red
            (term ($builtIn select \( (3 1 2 3) \)))
            
            (term (< (3) >)))
  
  (test-->> core-lang-expressions-red
            (term ($builtIn select \( (-1 1 2 3) \)))
            
            (term (< (3) >)))
  
  (test-->> core-lang-expressions-red
            (term ($builtIn select \( (-3 1 2 3) \)))
            
            (term (< (1 2 3) >)))
  
  ; E-BuiltInSelectError
  (test-->> core-lang-expressions-red
            (term ($builtIn select \( (-5 1 2 3) \)))
            
            (term (err "bad argument #1 to 'select'")))
  
  (test-->> core-lang-expressions-red
            (term ($builtIn select \( ("" 1 2 3) \)))
            
            (term (err "bad argument #1 to 'select'")))
  
  (test-results))

(provide expressions-red-test-suite)
