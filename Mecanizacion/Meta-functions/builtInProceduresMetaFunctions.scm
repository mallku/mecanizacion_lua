#lang racket
(require redex
         "../grammar.scm")

; Meta-function that implements the behaviour of the "next" standard library's
; procedure, in terms of the auxiliary function nextAux
(define-metafunction core-lang
  next : tableconstructor simplevalue -> tuple
  
  [(next (\{ field ... \}) simplevalue)
   (nextAux (field ...) simplevalue)])

(define-metafunction core-lang
  nextAux : (field ...) simplevalue -> any
  
  ; nil index, non-empty table
  [(nextAux ((\[ simplevalue_1 \] = simplevalue_2) field ...) nil)
   (< (simplevalue_1 simplevalue_2) >)]
  
  ; nil index, empty table
  [(nextAux () nil)
   (< (nil) >)]
  
  ; Not the last index
  [(nextAux (field_1 ... (\[ simplevalue_1 \] = simplevalue_2) 
                   (\[ simplevalue_3 \] = simplevalue_4) field_2 ...) simplevalue_1)
   (< (simplevalue_3 simplevalue_4) >)]
  
  ; Last index
  [(nextAux (field ... (\[ simplevalue_1 \] = simplevalue_2)) simplevalue_1)
   (< (nil) >)])

(provide next)

; Meta-function that implements the behaviour of the table.pack Lua's built-in 
; procedure. It receives a tuple with the values that we want to store.
(define-metafunction core-lang
  pack : (sv ...) -> evaluatedtable
  
  [(pack ())
   (\{ (\[ "n" \] = 0) \})]
  
  [(pack (sv_1 ...))
   ,(append (term (\{))
            (append (term (packAux (sv_1 ...) 1))
                    (term (\}))))])

(provide pack)

; Auxiliary meta-function, that returns the corresponding fields that constructs
; the table.pack Lua's built-in procedure. The first argument is a non-empty list with
; the values that we want to store in the table, and the second argument is
; the number from which we want to begin defining the keys.
(define-metafunction core-lang
  packAux : svlist Number -> (evaluatedfield ...)
  
  [(packAux (sv_1 sv_2 ...) Number)
   ,(append (term ((\[ Number \] = sv_1)))
            (term (packAux (sv_2 ...) Number_2)))
   (where Number_2 ,(+ (term Number) 1))
   (side-condition (not (eq? (term sv_1) (term nil))))]
  
  ; Nil values are discarded
  [(packAux (nil sv_2 ...) Number)
   (packAux (sv_2 ...) Number_2)
   (where Number_2 ,(+ (term Number) 1))]
  
  [(packAux () Number)
   ((\[ "n" \] = Number_2))
   (where Number_2 ,(- (term Number) 1))])


; Meta-function that implements the behaviour of the built-in service "select".
(define-metafunction core-lang
  
  ; Positive index, in the range [1;(length (sv ...))] 
  [(funcSelect Number (sv ...))
   ,(list-tail (term (sv ...)) (term Number_2))
   (side-condition (and (<= (term Number) (length (term (sv ...))))
                        (<= 1 (term Number))))
   (where Number_2 ,(- (term Number) 1))
   ]
  
  ; Positive index > (length (sv ...))
  [(funcSelect Number (sv ...))
   (nil)
   (side-condition (> (term Number) (length (term (sv ...)))))
   ]
  
  ; Negative index
  [(funcSelect Number (sv ...))
   ,(list-tail (term (sv ...)) (term Number_3))
   (where Number_2 ,(* -1 (length (term (sv ...)))))
   (side-condition (and (<= (term Number_2) (term Number))
                        (<= (term Number) -1)))
   (where Number_3 ,(+ (length (term (sv ...)))
                       (term Number)))]
  
  ; Select all
  [(funcSelect "#" (sv ...))
   (sv ...)
   ])

(provide funcSelect)