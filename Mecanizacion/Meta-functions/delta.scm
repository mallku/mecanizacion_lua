#lang racket
(require redex
         "../grammar.scm"
         "./tablesMetafunctions.scm")


; We define the semantics of the binary and unary operators of our language
; in terms of operations of PLT Scheme. The "," symbol is treated as an escape
; to PLT Scheme code. So, in general, the semantics of an expression
; (◇ op_1 op_2) is defined as the PLT Scheme code (◇ (term op_1) (term op_2))
; when ◇ is also an operator of PLT Scheme.
(define-metafunction core-lang
  ; Arithmetic operations
  [(δ (+ Number_1 Number_2)) ,(+ (term Number_1) (term Number_2))]
  [(δ (- Number_1 Number_2)) ,(- (term Number_1) (term Number_2))]
  [(δ (* Number_1 Number_2)) ,(* (term Number_1) (term Number_2))]
  [(δ (/ Number_1 Number_2)) ,(/ (term Number_1) (term Number_2))]
  [(δ (^ Number_1 Number_2)) ,(expt (term Number_1) (term Number_2))]
  [(δ (% Number_1 Number_2)) ,(remainder (term Number_1) (term Number_2))]
  [(δ (- Number)) ,(- (term Number))]
  ; Number comparison
  [(δ (< Number_1 Number_2)) 
   (toBool ,(< (term Number_1) (term Number_2)))]
  [(δ (<= Number_1 Number_2)) 
   (toBool ,(<= (term Number_1) (term Number_2)))]
  ; String comparison)
  [(δ (< String_1 String_2)) 
   (toBool ,(string<? (term String_1) (term String_2)))]
  [(δ (<= String_1 String_2)) 
   (toBool ,(string<=? (term String_1) (term String_2)))]
  ; String concatenation
  [(δ (.. String_1 String_2)) ,(string-append (term String_1) (term String_2))]
  ; String length
  [(δ (\# String)) ,(string-length (term String))]
  ; Table length
  [(δ (\# tableconstructor))
   0
   (side-condition (not (term (checkSequence tableconstructor))))]
  
  [(δ (\# evaluatedtable))
   (maxKeyNumber evaluatedtable)
   (side-condition (term (checkSequence evaluatedtable)))]
  
  ; Equality comparison
  ; Numbers needs special treatment
  [(δ (== Number_1 Number_2))
   (toBool ,(= (term Number_1) (term Number_2)))]
  
  [(δ (== sv sv_2))
   (toBool ,(equal? (term sv) (term sv_2)))]
  ; Logical connectives
  [(δ (and sv exp)) 
   sv
   (side-condition (or (equal? (term sv) (term false))
                       (equal? (term sv) (term nil))))]
  [(δ (and sv exp)) 
   exp
   (side-condition (and (not (equal? (term sv) (term false)))
                       (not (equal? (term sv) (term nil)))))]
  
  [(δ (or sv exp)) 
   sv
   (side-condition (and (not (equal? (term sv) (term false)))
                       (not (equal? (term sv) (term nil)))))]
  
  [(δ (or sv exp)) 
   exp
   (side-condition (or (equal? (term sv) (term false))
                       (equal? (term sv) (term nil))))]
  
  [(δ (not sv)) 
   true
   (side-condition (or (equal? (term sv) (term nil))
                       (equal? (term sv) (term false))))]
  
  [(δ (not sv)) 
   false
   (side-condition (and (not (equal? (term sv) (term nil)))
                        (not (equal? (term sv) (term false)))))])


; To convert booleans values in racket to boolean values in our language 
(define-metafunction core-lang
  
  [(toBool #t)
   true]
  
  [(toBool any)
   false])

; To export the delta function
(provide δ)