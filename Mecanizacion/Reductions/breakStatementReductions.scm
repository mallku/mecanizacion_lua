#lang racket
(require redex
         "../grammar.scm")

(define core-lang-break-stat-red
  (reduction-relation
   core-lang
   #:domain exp
   ; Break statement
   [--> (in-hole E (label \{ void \}))
        (in-hole E void)
        E-BlockLabelledEnd]
   
   [--> (in-hole Ed ((:: Name ::) \{ (in-hole Ej (break Name evaluatedtuple)) \}))
        (in-hole Ed void)
        E-BreakDiscardTuple]
   
   [--> (in-hole Ek ((:: Name ::) \{ (in-hole Ej (break Name evaluatedtuple)) \}))
        (in-hole Ek evaluatedtuple)
        E-BreakKeepTuple]
   
   [--> (in-hole E ((:: Name_1 ::) \{ (in-hole Ej (break Name_2 evaluatedtuple)) \}))
        (in-hole E (break Name_2 evaluatedtuple))
        E-LabelDiscarded
        (side-condition
         (not (equal? (term Name_1) (term Name_2))))]
   ; Error
   [--> (in-hole Enp (err simplevalue))
        (err simplevalue)
        E-ErrorPropagation
        (side-condition (not (eq? (term Enp) (term hole))))]
   
   ; Protected mode
   [--> (in-hole E (((in-hole Enp (err simplevalue)))ProtectedMode))
        (in-hole E (< (false simplevalue) >))
        E-ProtectedModeErrorCatched]
   
   [--> (in-hole E ((tuple)ProtectedMode))
        (in-hole E (< (true tuple) >))
        E-ProtectedModeNoErrorWithReturnedValues]
   
   [--> (in-hole E ((void)ProtectedMode))
        (in-hole E (< (true empty) >))
        E-ProtectedModeNoErrorWithoutReturnedValues]
   ))

(provide core-lang-break-stat-red)
