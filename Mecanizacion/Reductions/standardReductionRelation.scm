#lang racket
(require redex
         "../grammar.scm"
         "./simpleExpressionsReductions.scm"
         "./expressionsObjStoreReductions.scm"
         "./expressionsSimpValStoreReductions.scm"
         "./expressionsSimpValObjStoreReductions.scm"
         "./abnormalExpressionsReductions.scm"
         "./simpleStatementsReductions.scm"
         "./breakStatementReductions.scm"
         "./statementsSimpValStoreReductions.scm"
         "./statementsObjStoreReductions.scm"
         "./abnormalStatementsReductions.scm")

(define core-lan-stand-reduc-red-rel
  (reduction-relation
   core-lang
   ; Simple expressions
   [--> (σ \; θ \; (in-hole E exp))
        (σ \; θ \; (in-hole E exp_2))
        E-simpleExpressions
        (where (exp_2) ,(apply-reduction-relation core-lang-expressions-red
                                                          (term exp)))
        ]
   ; Expressions that interact with the simple value store
   [--> (σ \; θ \; (in-hole E exp))
        (σ_2 \; θ \; (in-hole E exp_2))
        E-simpValStoreExpressions
        (where ((σ_2 \; exp_2)) ,(apply-reduction-relation core-lang-sv-store-red
                                                          (term (σ \; exp))))]
   ; Expressions that interact with the object store
   [--> (σ \; θ \; (in-hole E exp))
        (σ \; θ_2 \; (in-hole E exp_2))
        E-objStoreExpressions
        (where ((θ_2 \; exp_2)) ,(apply-reduction-relation core-lang-exp-obj-store-red
                                                          (term (θ \; exp))))]
   ; Expressions that interact with the both stores
   [--> (σ \; θ \; (in-hole E exp))
        (σ_2 \; θ_2 \; (in-hole E exp_2))
        E-simpValObjStoreExpressions
        (where ((σ_2 \; θ_2 \; exp_2)) ,(apply-reduction-relation core-lang-exp-sv-obj-store-red
                                                          (term (σ \; θ \; exp))))]
   ; Abnormal expressions
   [--> (σ \; θ \; (in-hole E exp))
        (σ \; θ_2 \; (in-hole E exp_2))
        E-abnormalExpressions
        (where ((θ_2 \; exp_2)) ,(apply-reduction-relation core-lang-abnormal-expressions-red
                                                          (term (θ \; exp))))]
   ; Simple statements
   [--> (σ \; θ \; (in-hole E block))
        (σ \; θ \; (in-hole E block_2))
        E-simpleStatements
        (where (block_2) ,(apply-reduction-relation core-lang-simply-stat-red
                                                          (term block)))]
   ; Break statement
   [--> (σ \; θ \; block)
        (σ \; θ \; block_2)
        E-breakStatement
        (where (block_2) ,(apply-reduction-relation core-lang-break-stat-red
                                                          (term block)))]
   
   ; Errors
   [--> (σ \; θ \; block)
        (σ \; θ \; block_2)
        E-ErrorStatement
        (where (block_2) ,(apply-reduction-relation core-lang-break-stat-red
                                                          (term block)))]
   ; Statements that interact with the object store
   [--> (σ \; θ \; (in-hole E block))
        (σ \; θ_2 \; (in-hole E block_2))
        E-objStoreStatements
        (where ((θ_2 \; block_2)) ,(apply-reduction-relation core-lang-stat-obj-store-red
                                                          (term (θ \; block))))]
   ; Statements that interact with the simple value store
   [--> (σ \; θ \; (in-hole E block))
        (σ_2 \; θ \; (in-hole E block_2))
        E-simpValStoreStatements
        (where ((σ_2 \; block_2)) ,(apply-reduction-relation core-lang-stat-sv-store-red
                                                          (term (σ \; block))))]
   ; Abnormal statements
   [--> (σ \; θ \; (in-hole E block))
        (σ \; θ_2 \; (in-hole E block_2))
        E-abnormalStatements
        (where ((θ_2 \; block_2)) ,(apply-reduction-relation core-lang-abnormal-statements-red
                                                          (term (θ \; block))))]
   ))

(provide core-lan-stand-reduc-red-rel)