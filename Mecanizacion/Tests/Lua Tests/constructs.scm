#lang racket
(require redex
         "../../grammar.scm"
         "../../executionEnvironment.scm"
         "../../Reductions/standardReductionRelation.scm")
(define library
  (term (plugIntoExecutionEnvironment 
(((((((((((((((do 
 void
end)\;
(do 
( (((ENV \["a"\])) = (3.0))\;
 ((ENV \["assert"\]) \( (((ENV \["a"\]) == 3.0)) \)))
end))\;
((ENV \["assert"\]) \( (((2.0 ^ (3.0 ^ 2.0)) == (2.0 ^ (\((3.0 ^ 2.0)\))))) \)))\;
((ENV \["assert"\]) \( ((((2.0 ^ 3.0) * 4.0) == ((\((2.0 ^ 3.0)\)) * 4.0))) \)))\;
((ENV \["assert"\]) \( ((((2.0 ^ (- 2.0)) == (1.0 / 4.0)) and ((- (2.0 ^ (- (- 2.0)))) == (- (- (- 4.0)))))) \)))\;
((ENV \["assert"\]) \( ((((not nil) and 2.0) and (not (\(((2.0 > 3.0) or (3.0 < 2.0))\))))) \)))\;
((ENV \["assert"\]) \( (((((- 3.0) - 1.0) - 5.0) == ((0.0 + 0.0) - 9.0))) \)))\;
((ENV \["assert"\]) \( (((((- (2.0 ^ 2.0)) == (- 4.0)) and (((\((- 2.0)\)) ^ 2.0) == 4.0)) and ((((2.0 * 2.0) - 3.0) - 1.0) == 0.0))) \)))\;
((ENV \["assert"\]) \( (((((2.0 * 1.0) + (3.0 / 3.0)) == 3.0) and (((1.0 + 2.0) .. (3.0 * 1.0)) == "33"))) \)))\;
((ENV \["assert"\]) \( (((not (\(((2.0 + 1.0) > (3.0 * 1.0))\))) and (("a" .. "b") > "a"))) \)))\;
((ENV \["assert"\]) \( ((not (\(((\((true or false)\)) and nil)\)))) \)))\;
((ENV \["assert"\]) \( ((true or (false and nil))) \)))\;
((ENV \["assert"\]) \( (((\(((\(((\((1.0 or false)\)) and true)\)) or false)\)) == true)) \)))\;
((ENV \["assert"\]) \( (((\(((\(((\((nil and true)\)) or false)\)) and true)\)) == false)) \)))\;
(local (a b) = (1.0 nil) in 
(((((((((((((((((((((((      ((ENV \["assert"\]) \( ((((- (\((1.0 or 2.0)\))) == (- 1.0)) and (((\((1.0 and 2.0)\)) + (\(((- 1.2) or (- 4.0))\))) == 0.8))) \))\;
      (((ENV \["x"\])) = ((\(((((\((b or a)\)) + 1.0) == 2.0) and (((\((10.0 or a)\)) + 1.0) == 11.0))\)))))\;
      ((ENV \["assert"\]) \( ((ENV \["x"\])) \)))\;
      (((ENV \["x"\])) = ((\((((\(((\((2.0 < 3.0)\)) or 1.0)\)) == true) and ((\(((2.0 < 3.0) and 4.0)\)) == 4.0))\)))))\;
      ((ENV \["assert"\]) \( ((ENV \["x"\])) \)))\;
      (((ENV \["x"\]) (ENV \["y"\])) = (1.0 2.0)))\;
      ((ENV \["assert"\]) \( ((((\(((ENV \["x"\]) > (ENV \["y"\]))\)) and (ENV \["x"\])) or ((ENV \["y"\]) == 2.0))) \)))\;
      (((ENV \["x"\]) (ENV \["y"\])) = (2.0 1.0)))\;
      ((ENV \["assert"\]) \( ((((\(((ENV \["x"\]) > (ENV \["y"\]))\)) and (ENV \["x"\])) or ((ENV \["y"\]) == 2.0))) \)))\;
      ((ENV \["assert"\]) \( (((1234567890.0 == ((ENV \["tonumber"\]) \( ("1234567890") \))) and ((1234567890.0 + 1.0) == 1234567891.0))) \)))\;
      ((:: $while ::) \{ 
       (while false do 
        void
       end)      \}))\;
      ((:: $while ::) \{ 
       (while nil do 
        void
       end)      \}))\;
      (do 
       (local (a) = () in 
             (((ENV \["f"\])) = ((function func1266 (\( (x) \)
                                               ((:: $ret ::) \{
((                                                ((x) = ((\{ (\[ "a" \] = 1.0) \})))\;
                                                ((x) = ((\{ (\[ "x" \] = 1.0) \}))))\;
                                                ((x) = ((\{ (\[ "G" \] = 1.0) \}))))
                                               \}) 
                                              end))))
 end)
      end))\;
      (((ENV \["f"\])) = ((function func1315 (\( (i) \)
                                 ((:: $ret ::) \{
(                                  (if (not (((ENV \["type"\]) \( (i) \)) == "number")) then 
                                   (break $ret (< (i "jojo") >))
                                  else 
                                   void
                                  end)\;
                                  (if (i > 0.0) then 
                                   (break $ret (< (i ((ENV \["f"\]) \( ((i - 1.0)) \))) >))
                                  else 
                                   void
                                  end))
                                 \}) 
                                end)))))\;
      (((ENV \["x"\])) = ((\{ ((ENV \["f"\]) \( (3.0) \)) ((ENV \["f"\]) \( (5.0) \)) ((ENV \["f"\]) \( (10.0) \)) \}))))\;
      ((ENV \["assert"\]) \( ((((((((ENV \["x"\]) \[ 1.0 \]) == 3.0) and (((ENV \["x"\]) \[ 2.0 \]) == 5.0)) and (((ENV \["x"\]) \[ 3.0 \]) == 10.0)) and (((ENV \["x"\]) \[ 4.0 \]) == 9.0)) and (((ENV \["x"\]) \[ 12.0 \]) == 1.0))) \)))\;
      ((ENV \["assert"\]) \( ((((ENV \["x"\]) \[ nil \]) == nil)) \)))\;
      (((ENV \["x"\])) = ((\{ ((ENV \["f"\]) \( ("alo") \)) ((ENV \["f"\]) \( ("xixi") \)) nil \}))))\;
      ((ENV \["assert"\]) \( ((((((ENV \["x"\]) \[ 1.0 \]) == "alo") and (((ENV \["x"\]) \[ 2.0 \]) == "xixi")) and (((ENV \["x"\]) \[ 3.0 \]) == nil))) \)))\;
      (((ENV \["x"\])) = ((\{ (((ENV \["f"\]) \( ("alo") \)) .. "xixi") \}))))\;
      ((ENV \["assert"\]) \( ((((ENV \["x"\]) \[ 1.0 \]) == "aloxixi")) \)))\;
      (((ENV \["x"\])) = ((\{ ((ENV \["f"\]) \( ((\{  \})) \)) \}))))\;
      ((ENV \["assert"\]) \( (((((ENV \["x"\]) \[ 2.0 \]) == "jojo") and (((ENV \["type"\]) \( (((ENV \["x"\]) \[ 1.0 \])) \)) == "table"))) \)))\;
      (local (f) = ((function func1773(\( (i) \)       ((:: $ret ::) \{               (if (i < 10.0) then 
       (break $ret (< ("a") >))
      else 
       (if (i < 20.0) then 
        (break $ret (< ("b") >))
       else 
        (if (i < 30.0) then 
         (break $ret (< ("c") >))
        else 
         void
        end)
       end)
      end)        \})      end))) in 
(((((((((            ((ENV \["assert"\]) \( ((((((f \( (3.0) \)) == "a") and ((f \( (12.0) \)) == "b")) and ((f \( (26.0) \)) == "c")) and ((f \( (100.0) \)) == nil))) \))\;
            (do 
             (local ($1 $2 $3) = (($builtIn toNumber \( (1.0 10) \)) 
                                  ($builtIn toNumber \( (1000.0 10) \)) 
                                  ($builtIn toNumber \( (1.0 10) \)))
              in
                 ((if (not (($1 and $2) and $3)) then
                    ($builtIn error \( ("'for' limit must be a number") \))
                   else
                    void
                   end) \;
                 ((:: $while ::) \{ 
                  (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
                    (local (i) = ($1) in
                     (            (break $while empty) \; 
                     (($1) = (($1 + $3))))
                    end)
                  end)
                 \}))
             end)
            end))\;
            (((ENV \["n"\])) = (100.0)))\;
            (((ENV \["i"\])) = (3.0)))\;
            (((ENV \["t"\])) = ((\{  \}))))\;
            ((a) = (nil)))\;
            ((:: $while ::) \{ 
             (while (not a) do 
(              ((a) = (0.0))\;
              (do 
               (local ($1 $2 $3) = (($builtIn toNumber \( (1.0 10) \)) 
                                    ($builtIn toNumber \( ((ENV \["n"\]) 10) \)) 
                                    ($builtIn toNumber \( (1.0 10) \)))
                in
                   ((if (not (($1 and $2) and $3)) then
                      ($builtIn error \( ("'for' limit must be a number") \))
                     else
                      void
                     end) \;
                   ((:: $while ::) \{ 
                    (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
                      (local (i) = ($1) in
                       (              (do 
               (local ($4 $5 $6) = (($builtIn toNumber \( (i 10) \)) 
                                    ($builtIn toNumber \( (1.0 10) \)) 
                                    ($builtIn toNumber \( ((- 1.0) 10) \)))
                in
                   ((if (not (($4 and $5) and $6)) then
                      ($builtIn error \( ("'for' limit must be a number") \))
                     else
                      void
                     end) \;
                   ((:: $while ::) \{ 
                    (while (((0 < $6) and ($4 <= $5)) or (($6 <= 0) and ($5 <= $4))) do
                      (local (i) = ($4) in
                       ((              ((a) = ((a + 1.0)))\;
              ((((ENV \["t"\]) \[ i \])) = (1.0))) \; 
                       (($4) = (($4 + $6))))
                      end)
                    end)
                   \}))
               end)
              end) \; 
                       (($1) = (($1 + $3))))
                      end)
                    end)
                   \}))
               end)
              end))
             end)            \}))\;
            ((ENV \["assert"\]) \( (((a == (((ENV \["n"\]) * (\(((ENV \["n"\]) + 1.0)\))) / 2.0)) and ((ENV \["i"\]) == 3.0))) \)))\;
            ((ENV \["assert"\]) \( ((((((ENV \["t"\]) \[ 1.0 \]) and ((ENV \["t"\]) \[ (ENV \["n"\]) \])) and (not ((ENV \["t"\]) \[ 0.0 \]))) and (not ((ENV \["t"\]) \[ ((ENV \["n"\]) + 1.0) \])))) \)))\;
            (local (f) = ((function func2600(\( (i) \)             ((:: $ret ::) \{                           (if (i < 10.0) then 
             (break $ret (< ("a") >))
            else 
             (if (i < 20.0) then 
              (break $ret (< ("b") >))
             else 
              (if (i < 30.0) then 
               (break $ret (< ("c") >))
              else 
               (break $ret (< (8.0) >))
              end)
             end)
            end)              \})            end))) in 
(                  ((ENV \["assert"\]) \( ((((((f \( (3.0) \)) == "a") and ((f \( (12.0) \)) == "b")) and ((f \( (26.0) \)) == "c")) and ((f \( (100.0) \)) == 8.0))) \))\;
                  (local (a b) = (nil 23.0) in 
((((((((((((                        (((ENV \["x"\])) = ((\{ ((((f \( (100.0) \)) * 2.0) + 3.0) or a) (a or (b + 2.0)) \})))\;
                        ((ENV \["assert"\]) \( (((((ENV \["x"\]) \[ 1.0 \]) == 19.0) and (((ENV \["x"\]) \[ 2.0 \]) == 25.0))) \)))\;
                        (((ENV \["x"\])) = ((\{ (\[ "f" \] = ((2.0 + 3.0) or a)) (\[ "a" \] = (b + 2.0)) \}))))\;
                        ((ENV \["assert"\]) \( (((((ENV \["x"\]) \[ "f" \]) == 5.0) and (((ENV \["x"\]) \[ "a" \]) == 25.0))) \)))\;
                        ((a) = ((\{ (\[ "y" \] = 1.0) \}))))\;
                        (((ENV \["x"\])) = ((\{ (a \[ "y" \]) \}))))\;
                        ((ENV \["assert"\]) \( ((((ENV \["x"\]) \[ 1.0 \]) == 1.0)) \)))\;
                        ((f) = ((function func2986 (\( (i) \)
                                                        ((:: $ret ::) \{
                                                         ((:: $while ::) \{ 
                                                          (while 1.0 do 
                                                           (if (i > 0.0) then 
                                                            ((i) = ((i - 1.0)))
                                                           else 
                                                            (break $ret empty)
                                                           end)
                                                          end)                                                         \})
                                                        \}) 
                                                       end)))))\;
                        (((ENV \["g"\])) = ((function func3071 (\( (i) \)
                                                                     ((:: $ret ::) \{
                                                                      ((:: $while ::) \{ 
                                                                       (while 1.0 do 
                                                                        (if (i > 0.0) then 
                                                                         ((i) = ((i - 1.0)))
                                                                        else 
                                                                         (break $ret empty)
                                                                        end)
                                                                       end)                                                                      \})
                                                                     \}) 
                                                                    end)))))\;
                        (f \( (10.0) \)))\;
                        ((ENV \["g"\]) \( (10.0) \)))\;
                        (do 
(                         ((f) = ((function func3171 (\( () \)
                                                          ((:: $ret ::) \{
                                                           (break $ret (< (1.0 2.0 3.0) >))
                                                          \}) 
                                                         end))))\;
                         (local (a b c) = ((f \( () \))) in 
((                               ((ENV \["assert"\]) \( ((((a == 1.0) and (b == 2.0)) and (c == 3.0))) \))\;
                               ((a b c) = ((\((f \( () \))\)))))\;
                               ((ENV \["assert"\]) \( ((((a == 1.0) and (b == nil)) and (c == nil))) \)))
 end))
                        end))\;
                        (local (a b) = ((3.0 and (f \( () \)))) in 
(((((                              ((ENV \["assert"\]) \( (((a == 1.0) and (b == nil))) \))\;
                              (((ENV \["g"\])) = ((function func3369 (\( () \)
                                                                                 ((:: $ret ::) \{
(                                                                                  (f \( () \))\;
                                                                                  (break $ret empty))
                                                                                 \}) 
                                                                                end)))))\;
                              ((ENV \["assert"\]) \( ((((ENV \["g"\]) \( () \)) == nil)) \)))\;
                              (((ENV \["g"\])) = ((function func3419 (\( () \)
                                                                                 ((:: $ret ::) \{
                                                                                  (break $ret (< ((nil or (f \( () \)))) >))
                                                                                 \}) 
                                                                                end)))))\;
                              ((a b) = (((ENV \["g"\]) \( () \)))))\;
                              ((ENV \["assert"\]) \( (((a == 1.0) and (b == nil))) \)))
 end))
 end))
 end))
 end))
 end))





)))

(define (void? red)
  (and (redex-match core-lang
              (σ \; θ \; void)
              (first red))
       (eq? (length red) 1)))

(define (lua-constructs-test-suite)
  (test-predicate void? (apply-reduction-relation* core-lan-stand-reduc-red-rel library))
  (test-results))

(provide lua-constructs-test-suite)