#lang scheme
(require redex
         "../grammar.scm"
         "../Reductions/breakStatementReductions.scm")

(define (break-stat-red-test-suite)
  ; Break
  (test-->> core-lang-break-stat-red
            (term ((:: X ::) \{ void \}))
            (term void))
  (test-->> core-lang-break-stat-red
            (term ((:: X ::) \{ (break X (< (1) >)) \}))
            (term void))
  (test-->> core-lang-break-stat-red
            (term (if ((:: X ::) \{ (break X (< (1) >)) \})
                             then void else void end))
            (term (if (< (1) >) then void else void end)))
  (test-->> core-lang-break-stat-red
            (term ((:: X ::) \{ (break Y (< (1) >)) \}))
            (term (break Y (< (1) >))))
  ; Errors
  (test-->> core-lang-break-stat-red
            (term ((:: X ::) \{ (err "error") \}))
            (term (err "error")))
  
  (test-->> core-lang-break-stat-red
            (term (do (err "error") end))
            (term (err "error")))
  
  (test-->> core-lang-break-stat-red
            (term (if (err "error") then void else void end))
            (term (err "error")))
  
  (test-->> core-lang-break-stat-red
            (term (local (X) = ((err "error")) in void end))
            (term (err "error")))
  
  (test-->> core-lang-break-stat-red
            (term (err "error"))
            (term (err "error")))
  
  ; Pcall
  (test-->> core-lang-break-stat-red
            (term (((err "error"))ProtectedMode))
            (term (< (false "error") >)))
  
  (test-->> core-lang-break-stat-red
            (term (((< (1) >))ProtectedMode))
            (term (< (true (< (1) >)) >)))
  
  (test-->> core-lang-break-stat-red
            (term ((void)ProtectedMode))
            (term (< (true empty) >)))
  (test-results))

(provide break-stat-red-test-suite)