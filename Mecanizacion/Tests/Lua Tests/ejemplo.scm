#lang racket
(require redex
         "../../grammar.scm"
         "../../executionEnvironment.scm"
         "../../Reductions/standardReductionRelation.scm")
(define library
  (term (plugIntoExecutionEnvironment
      (local (f) = (nil) in 
(( ((f) = ((function func109 (\((x)\)           ((:: $ret ::) \{
(            ((x) = (nil))\;
            (break $ret (< (x) >)))
           \})
          end))))\;
      ((ENV \["assert"\]) \( (((f \( (10.0) \)) == nil)) \)))\;
      (local (f) = (nil) in 
((       ((f) = ((function func173 (\(()\)                 ((:: $ret ::) \{
                  (local (x) = () in 
                        (break $ret (< (x) >))
 end)
                 \})
                end))))\;
            ((ENV \["assert"\]) \( (((f \( (10.0) \)) == nil)) \)))\;
            (do 
             (local (i) = (10.0) in 
(((                   (do 
                    (local (i) = (100.0) in 
                          ((ENV \["assert"\]) \( ((i == 100.0)) \))
 end)
                   end)\;
                   (do 
                    (local (i) = (1000.0) in 
                          ((ENV \["assert"\]) \( ((i == 1000.0)) \))
 end)
                   end))\;
                   ((ENV \["assert"\]) \( ((i == 10.0)) \)))\;
                   (if (not (i == 10.0)) then 
                    (local (i) = (20.0) in 
                          void
 end)
                   else 
                    (local (i) = (30.0) in 
                          ((ENV \["assert"\]) \( ((i == 30.0)) \))
 end)
                   end))
 end)
            end))
 end))
 end)










 
)))

(define (void? red)
  (and (redex-match core-lang
              (σ \; θ \; void)
              (first red))
       (eq? (length red) 1)))

(define (lua-ejemplo-test-suite)
  (test-predicate void? (apply-reduction-relation* core-lan-stand-reduc-red-rel library))
  (test-results))

(provide lua-ejemplo-test-suite)