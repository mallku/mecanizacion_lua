#lang racket
; Black-box testing for simple statements

(require redex
         "../grammar.scm"
         "../Reductions/simpleStatementsReductions.scm")

(define (simple-stat-red-test-suite)
  ; Conditional
  (test-->> core-lang-simply-stat-red
            (term (if true then (X \( () \)) else (Y \( () \)) end))
            (term (X \( () \))))
  (test-->> core-lang-simply-stat-red
            (term (if (objr 1) then (X \( () \)) else (Y \( () \)) end))
            (term (X \( () \))))
  (test-->> core-lang-simply-stat-red
            (term (if false then (X \( () \)) else (Y \( () \)) end))
            (term (Y \( () \))))
  (test-->> core-lang-simply-stat-red
            (term (if nil then (X \( () \)) else (Y \( () \)) end))
            (term (Y \( () \))))
  ; While loop
  (test-->> core-lang-simply-stat-red
            (term (while false do (X \( () \)) end))
            (term void))
  ; Concatenation of statements
  (test-->> core-lang-simply-stat-red
            (term (void \; (Y \( () \))))
            (term (Y \( () \))))
  ; Block Do-End
  (test-->> core-lang-simply-stat-red
            (term (do void end))
            (term void))
  
  ; List length-equating rules
  ; E-AssignDiscardValues
  (test-->> core-lang-simply-stat-red
            (term (((svr 1)) = (1 2)))
            (term (((svr 1)) = (1))))
  ; E-AssignCompleteValues
  (test-->> core-lang-simply-stat-red
            (term (((svr 1) (svr 2)) = (1)))
            (term ((((svr 2)) = (nil)) \; (((svr 1)) = (1)))))
  
  ; E-AssignSplit
  (test-->> core-lang-simply-stat-red
            (term (((svr 1) (svr 2)) = (1 2)))
            (term ((((svr 2)) = (2)) \; (((svr 1)) = (1)))))
  
  ; E-LocalDiscardValues
  (test-->> core-lang-simply-stat-red
            (term (local (X) = (1 2) in void end))
            (term (local (X) = (1) in void end)))
  
  ; E-LocalCompleteValues
  (test-->> core-lang-simply-stat-red
            (term (local (X Y) = (1) in void end))
            (term (local (X Y) = (1 nil) in void end)))
  (test-results))

(provide simple-stat-red-test-suite)