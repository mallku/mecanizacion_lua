#lang scheme
(require redex
         "../grammar.scm"
         "../Meta-functions/delta.scm")

(define (delta-test-suite)
  ; Arithmetic operations
  (test-equal (term (δ (+ 1 1))) 2)
  (test-equal (term (δ (- 1 1))) 0)
  (test-equal (term (δ (* 1 1))) 1)
  (test-equal (term (δ (/ 1 1))) 1)
  (test-equal (term (δ (^ 1 1))) 1)
  (test-equal (term (δ (- 1))) -1)
  ; Number comparison
  (test-equal (term (δ (< 1 2))) (term true))
  (test-equal (term (δ (== 1 1))) (term true))
  (test-equal (term (δ (<= 1 1))) (term true))
  ; String comparison
  (test-equal (term (δ (< "a" "b"))) (term true))
  (test-equal (term (δ (== "a" "a"))) (term true))
  (test-equal (term (δ (<= "a" "a"))) (term true))
  ; String concatenation
  (test-equal (term (δ (.. "a" "b"))) "ab")
  ; String length
  (test-equal (term (δ (\# "abc"))) 3)
  ; Table length
  (test-equal (term (δ (\# (\{ (\[ 2 \] = 1) \})))) 0)
  (test-equal (term (δ (\# (\{ (\[ 1 \] = 1) \})))) 1)
  (test-equal (term (δ (\# (\{ (\[ 1 \] = 1) (\[ "a" \] = 1) (\[ 2 \] = 1) 
                               \})))) 2)
  (test-equal (term (δ (\# (\{ (\[ -1 \] = 1) (\[ "a" \] = 1) (\[ 1 \] = 1) 
                               \})))) 1)
  ; Equality comparison
  (test-equal (term (δ (== (objr 1) (objr 1)))) (term true))
  (test-equal (term (δ (== true true))) (term true))
  ; Logical connectives
  (test-equal (term (δ (and true (\{ \})))) (term (\{ \})))
  (test-equal (term (δ (and (objr 1) nil))) (term nil))
  (test-equal (term (δ (and (objr 1) (objr 1)))) (term (objr 1)))
  (test-equal (term (δ (or true true))) (term true))
  (test-equal (term (δ (or nil nil))) (term nil))
  (test-equal (term (δ (or nil (objr 1)))) (term (objr 1)))
  (test-equal (term (δ (not true))) (term false))
  (test-equal (term (δ (not nil))) (term true))
  (test-equal (term (δ (not (objr 1)))) (term false))
  
  (test-results))

(provide delta-test-suite)