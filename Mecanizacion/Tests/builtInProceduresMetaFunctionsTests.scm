#lang racket
(require redex
         "../grammar.scm"
         "../Meta-functions/builtInProceduresMetaFunctions.scm")

; "black-box testing"

(define (builtInProceduresMetaFunctions-test-suite)
  ; next
  (test-equal (term (next (\{ \}) nil))
              (term (< (nil) >)))
  
  (test-equal (term (next (\{ (\[ 1 \] = 2) (\[ 3 \] = 4) \}) nil))
              (term (< (1 2) >)))
  
  (test-equal (term (next (\{ (\[ 1 \] = 2) (\[ 3 \] = 4) \}) 1))
              (term (< (3 4) >)))
  
  (test-equal (term (next (\{ (\[ 1 \] = 2) (\[ 3 \] = 4) \}) 1))
              (term (< (3 4) >)))
  
  (test-equal (term (next (\{ (\[ 1 \] = 2) (\[ 3 \] = 4) (\[ 5 \] = 6) \}) 3))
              (term (< (5 6) >)))
  
  (test-equal (term (next (\{ (\[ 1 \] = 2) (\[ 3 \] = 4) (\[ 5 \] = 6) \}) 5))
              (term (< (nil) >)))
 
 (test-results))

(provide builtInProceduresMetaFunctions-test-suite)