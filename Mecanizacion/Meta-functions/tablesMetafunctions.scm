#lang scheme
(require redex
         "../grammar.scm"
         "./misc.scm")

; Operations that deals with tables and their internal representation

(define-metafunction core-lang
  getTable : intreptable -> tableconstructor
  
  [(getTable (\( tableconstructor \, any \)))
   tableconstructor])

(provide getTable)

(define-metafunction core-lang
  getMetaTableOfTable : intreptable -> any
  
  [(getMetaTableOfTable (\( tableconstructor \, any \)))
   any])

(provide getMetaTableOfTable)

; Modify an stored table
; PRE : {the store received satisfy the invariant of representation
;        and the reference belongs to the domain of the
;        store}
(define-metafunction core-lang
  modifyTable : intreptable tableconstructor -> intreptable
  
  [(modifyTable (\( tableconstructor_1 \, simplevalue \))
                tableconstructor)
   
   (\( tableconstructor \, simplevalue \))])

(provide modifyTable)

; Constructs the internal representation of a new table
(define-metafunction core-lang
  newTable : tableconstructor -> intreptable
  
  [(newTable tableconstructor)
   (\( tableconstructor \, nil \))])

(provide newTable)

; Index a table with a given key. Same semantics as Lua's rawget
(define-metafunction core-lang
  rawGet : tableconstructor simplevalue -> simplevalue
  
  [(rawGet (\{ evaluatedfield ... \}) simplevalue)
   (rawGetAux (evaluatedfield ...) simplevalue)])

(provide rawGet)

; Auxiliar meta-function used by rawGet that actually performs the
; indexing.
(define-metafunction core-lang
  rawGetAux : (evaluatedfield ...) simplevalue -> simplevalue
  
  [(rawGetAux () simplevalue)
   nil]
  
  [(rawGetAux ((\[ simplevalue_1 \] = simplevalue) evaluatedfield ...) 
               simplevalue_1)
   simplevalue]
  
  ; An integer and a floating point are equal, if they represent the
  ; same quantity
  [(rawGetAux ((\[ simplevalue_1 \] = simplevalue) evaluatedfield ...) 
               simplevalue_2)
   simplevalue
   (side-condition (and (eq? (term (simpValType simplevalue_1)) "number")
                        (eq? (term (simpValType simplevalue_2)) "number")
                        (= (term simplevalue_1) (term simplevalue_2))))]
  
  [(rawGetAux ((\[ simplevalue_1 \] = simplevalue) evaluatedfield ...) 
               simplevalue_2)
   (rawGetAux (evaluatedfield ...) simplevalue_2)])

; Table assignment. Same semantics as Lua's rawset
; PRE : {sv_1 is a valid field key and sv_2 is a valid field value}
; ret = (assignTableField table sv_1 sv_2)
; POS : {same as Lua's rawset; if the field "[sv_1] = sv_2" must be added, then it
;        appears as the first field in ret}
(define-metafunction core-lang
  assignTableField : tableconstructor simplevalue simplevalue -> tableconstructor
  
  [(assignTableField (\{ evaluatedfield ... \}) simplevalue_1 simplevalue)
   ,(append (term (\{))
            (append 
             (term (assignTableFieldAux (evaluatedfield ...) simplevalue_1 simplevalue))
             (term ( \}))))])

(provide assignTableField)

; Auxiliar meta-function used by assignTableField that actually performs the
; assignment.
(define-metafunction core-lang
  assignTableFieldAux : (evaluatedfield ...) simplevalue simplevalue -> (evaluatedfield ...)
  
  [(assignTableFieldAux (evaluatedfield_1 ... 
                         (\[ simplevalue_1 \] = simplevalue_2) evaluatedfield ...) 
               simplevalue_1 nil)
   (evaluatedfield_1 ... evaluatedfield ...)]
  
  [(assignTableFieldAux (evaluatedfield_1 ... 
                         (\[ simplevalue_1 \] = simplevalue_2) evaluatedfield ...) 
               simplevalue_1 simplevalue)
   (evaluatedfield_1 ... (\[ simplevalue_1 \] = simplevalue) evaluatedfield ...)]
  
  [(assignTableFieldAux (evaluatedfield ... ) simplevalue_1 nil)
   (evaluatedfield ...)]
  
  [(assignTableFieldAux (evaluatedfield ... ) simplevalue_1 simplevalue)
   ((\[ simplevalue_1 \] = simplevalue) evaluatedfield ...)])

; Determines if a given key belongs to a table
(define-metafunction core-lang
  keyBelongsTo? : tableconstructor simplevalue -> any
  
  [(keyBelongsTo? (\{ evaluatedfield ... \}) simplevalue)
   (keyBelongsTo?Aux (evaluatedfield ...) simplevalue)])

(provide keyBelongsTo?)

; Auxiliar meta-function used by keyBelongsTo? that actually determines
; if a given key belongs to a table
(define-metafunction core-lang
  keyBelongsTo?Aux : (evaluatedfield ...) simplevalue -> any
  
  [(keyBelongsTo?Aux () simplevalue)
   #f]
  
  [(keyBelongsTo?Aux ((\[ simplevalue_1 \] = simplevalue) evaluatedfield ...) 
               simplevalue_1)
   #t]
  
  ; An integer and a floating point are equal, if they represent the
  ; same quantity
  [(keyBelongsTo?Aux ((\[ simplevalue_1 \] = simplevalue) evaluatedfield ...) 
               simplevalue_2)
   #t
   (side-condition (and (eq? (term (simpValType simplevalue_1)) "number")
                        (eq? (term (simpValType simplevalue_2)) "number")
                        (= (term simplevalue_1) (term simplevalue_2))))]
  
  [(keyBelongsTo?Aux ((\[ simplevalue_1 \] = simplevalue) evaluatedfield ...) 
               simplevalue_2)
   (keyBelongsTo?Aux (evaluatedfield ...) simplevalue_2)])

; Given an evaluated table that can contains fields with no key specified,
; then addFields adds the corresponding numeric values, as the semantics
; of Lua 5.2 dictates
(define-metafunction core-lang
  addFields : evaluatedtable -> evaluatedtable
  
  [(addFields (\{ evaluatedfield ... \}))
   ,(append (term (\{ ))
            (append (term any_1)
                    (append (term any_2)
                            (term ( \})))))
   
   (where any_1 (addFieldsAux (extractFieldsWithoutKey
                                          (evaluatedfield ...)) 1))
   (where Number ,(length (term any_1)))
   (where any_2 (removeFieldsWithKey Number 
                                     (extractFieldsWithKey (evaluatedfield ...))))])

(provide addFields)

(define-metafunction core-lang
  [(extractFieldsWithoutKey () )
   ()]
  
  [(extractFieldsWithoutKey (sv evaluatedfield ...) )
   ,(append (term (sv))
            (term (extractFieldsWithoutKey (evaluatedfield ...) )))]
  
  [(extractFieldsWithoutKey ((\[ sv_1 \] = sv_2) evaluatedfield ...) )
   (extractFieldsWithoutKey (evaluatedfield ...))]
  )

(define-metafunction core-lang
  [(extractFieldsWithKey () )
   ()]
  
  [(extractFieldsWithKey (sv evaluatedfield ...) )
   (extractFieldsWithKey (evaluatedfield ...))]
  
  [(extractFieldsWithKey ((\[ sv_1 \] = sv_2) evaluatedfield ...) )
   ,(append (term ((\[ sv_1 \] = sv_2)))
            (term (extractFieldsWithKey (evaluatedfield ...) )))]
  )
; Auxiliar meta-function used by addFields: add numeric keys to fields
; without them, begining by some specific number
(define-metafunction core-lang
  addFieldsAux : (sv ...) Number -> (evaluatedfield ...)
  
  [(addFieldsAux () Number)
   ()]
  
  [(addFieldsAux (simplevalue evaluatedfield ...) Number_1)
   ,(append (term ((\[ Number_1 \] = simplevalue)))
            (term (addFieldsAux (evaluatedfield ...) Number_2)))
   (where Number_2 ,(+ 1 (term Number_1)))])

; To eliminate fields with a numeric key, between 1 and given n
(define-metafunction core-lang
  [(removeFieldsWithKey Number ())
   ()]
  
  [(removeFieldsWithKey Number_1 ((\[ Number_2 \] = sv_1) evaluatedfield ...))
   (removeFieldsWithKey Number_1 (evaluatedfield ...))
   (side-condition (and (>= (term Number_2) 1)
                        (<= (term Number_2) (term Number_1))))]
  
  [(removeFieldsWithKey Number_1 ((\[ Number_2 \] = sv_1) evaluatedfield ...))
   ,(append (term ((\[ Number_2 \] = sv_1)))
            (term (removeFieldsWithKey Number_1 (evaluatedfield ...))))
   (side-condition (not (and (>= (term Number_2) 1)
                        (<= (term Number_2) (term Number_1)))))]
  
  ; Any other case...
  [(removeFieldsWithKey Number ((\[ sv_1 \] = sv_2) evaluatedfield ...))
   ,(append (term ((\[ sv_1 \] = sv_2)))
            (term (removeFieldsWithKey Number (evaluatedfield ...))))]
  )

; Determines if a given table is a sequence
(define-metafunction core-lang
  checkSequence : tableconstructor -> any
  
  [(checkSequence tableconstructor)
   (checkSequenceAux tableconstructor 1 (maxKeyNumber tableconstructor))])

(provide checkSequence)

(define-metafunction core-lang
  checkSequenceAux : tableconstructor Number Number -> any
  
  [(checkSequenceAux tableconstructor Number Number_2)
   (checkSequenceAux tableconstructor Number_3 Number_2)
   (side-condition (<= (term Number) (term Number_2)))
   (side-condition (term (keyBelongsTo? tableconstructor Number)))
   (where Number_3 ,(+ (term Number) (term 1)))]
  
  [(checkSequenceAux tableconstructor Number Number_2)
   #f
   (side-condition (<= (term Number) (term Number_2)))
   (side-condition (not (term (keyBelongsTo? tableconstructor Number))))]
  
  [(checkSequenceAux tableconstructor Number Number_2)
   #t
   (side-condition (> (term Number) (term Number_2)))])

; Determines the maximum natural n such that there is a field with n as key.
; If such n exist, then it is returned. Otherwise it returns 0
(define-metafunction core-lang
  maxKeyNumber : tableconstructor -> Number
  [(maxKeyNumber (\{ field ... \}))
   (maxKeyNumberAux (field ...) 0)])

(provide maxKeyNumber)
  
; Auxiliar meta-function used by maxKeyNumber that actually performs
; the task, searching the max key number, begining by some number n.
; ret = (maxKeyNumberAux table n)
; POS : {ret is the maximum natural number, greater or equal to n, such
;        that there is a field in table with key m or ret = n, otherwise}
(define-metafunction core-lang
  maxKeyNumberAux : (field ...) Number -> Number
  
  [(maxKeyNumberAux () Number)
   Number]
  
  [(maxKeyNumberAux ((\[ Number \] = sv) field ...) Number_2)
   (maxKeyNumberAux (field ...) Number_2)
   (side-condition (<= (term Number) (term Number_2)))]
  
  [(maxKeyNumberAux ((\[ Number \] = sv) field ...) Number_2)
   (maxKeyNumberAux (field ...) Number)
   (side-condition (> (term Number) (term Number_2)))]
  
  [(maxKeyNumberAux ((\[ sv \] = sv_2) field ...) Number)
   (maxKeyNumberAux (field ...) Number)])
  
  