#lang racket
(require redex
         "../grammar.scm"
         "../Reductions/abnormalExpressionsReductions.scm")

(define (abnormal-expressions-red-test-suite)
  ; Function call
  ; E-WrongFunctionCallWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__call" \] = (objr 2)) \}) \, nil \)) \))
                    (\( (objr 2) \, 
                        (function X (\( () \) ((svr 1) \( () \)) end)) \))) 
                   \; ((1 \( (2) \))WrongFunctionCall)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__call" \] = (objr 2)) \}) \, nil \)) \))
                    (\( (objr 2) \, 
                        (function X (\( () \) ((svr 1) \( () \)) end)) \))) 
                   \; ((objr 2) \( (1 2) \)))))
  ; E-WrongFunctionCallNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, 
                        (\( (\{ \}) \, nil \)) \))) 
                   \; ((1 \( (2) \))WrongFunctionCall)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ \}) \, nil \)) \))) 
                   \; ($builtIn error \( ("attempt to call a number value") \)))))
  
  (test-->> core-lang-abnormal-expressions-red
            (term (() \; ((1 \( (2) \))WrongFunctionCall)))
            
            (term (() \; ($builtIn error \( ("attempt to call a number value") \)))))
  
  ; E-KeyNotFoundWithHandlerNormal
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, (objr 2) \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ (\[ "__index" \] = (objr 3)) \}) \, nil \)) \))
                    (\( (objr 3) \, 
                        (function X (\( () \) ((svr 1) \( () \)) end)) \))) 
                   \; (((objr 1) \[ 2 \])KeyNotFound)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, (objr 2) \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ (\[ "__index" \] = (objr 3)) \}) \, nil \)) \))
                    (\( (objr 3) \, 
                        (function X (\( () \) ((svr 1) \( () \)) end)) \))) 
                   \; ((objr 3) \( ((objr 1) 2) \)))))
  
  ; E-KeyNotFoundWithHandlerRepeat
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, (objr 2) \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ (\[ "__index" \] = 1) \}) \, nil \)) \))) 
                   \; (((objr 1) \[ 2 \])KeyNotFound)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, (objr 2) \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ (\[ "__index" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \[ 2 \]))))
  
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, (objr 2) \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ (\[ "__index" \] = (objr 3)) \}) \, nil \)) \))
                    (\( (objr 3) \, 
                        (\( (\{  \}) \, nil \)) \))) 
                   \; (((objr 1) \[ 2 \])KeyNotFound)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, (objr 2) \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ (\[ "__index" \] = (objr 3)) \}) \, nil \)) \))
                    (\( (objr 3) \, 
                        (\( (\{  \}) \, nil \)) \))) \; ((objr 3) \[ 2 \]))))
  ; E-KeyNotFoundNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, nil \)) \))) 
                   \; (((objr 1) \[ 2 \])KeyNotFound)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, nil \)) \))) \; nil)))
  
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, (objr 2) \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ \}) \, nil \)) \))) 
                   \; (((objr 1) \[ 2 \])KeyNotFound)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, (objr 2) \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ \}) \, nil \)) \))) \; nil)))
  
  ; E-NonTableIndexedWithHandlerNormal
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__index" \] = (objr 6)) \}) \, nil \)) \))
                    (\( (objr 6) \, 
                        (function X (\( () \) ((svr 1) \( () \)) end)) \))) 
                   \; ((1 \[ 2 \])NonTableIndexed)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__index" \] = (objr 6)) \}) \, nil \)) \))
                    (\( (objr 6) \, 
                        (function X (\( () \) ((svr 1) \( () \)) end)) \))) 
                   \; ((objr 6) \( (1 2) \)))))
  
  ; E-NonTableIndexedWithHandlerRepeat
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__index" \] = (objr 6)) \}) \, nil \)) \))
                    (\( (objr 6) \, 
                        (\( (\{ \}) \, nil \)) \))) 
                   \; ((1 \[ 2 \])NonTableIndexed)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__index" \] = (objr 6)) \}) \, nil \)) \))
                    (\( (objr 6) \, 
                        (\( (\{ \}) \, nil \)) \))) \; ((objr 6) \[ 2 \]))))
  
  ; E-NonTableIndexedNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() \; ((1 \[ 2 \])NonTableIndexed)))
            (term (() \; ($builtIn error \( ("attempt to index a number value") \)))))
  
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, 
                        (\( (\{ \}) \, nil \)) \))) 
                   \; ((1 \[ 2 \])NonTableIndexed)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ \}) \, nil \)) \))) \; 
                                                    ($builtIn error \( 
                                                             ("attempt to index a number value") \)))))
  
  ; E-AdditionWrongOperandsWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 4) \, (\( (\{ (\[ "__add" \] = 1) \}) \, nil \)) \))) 
                   \; (("q" + "q")AdditionWrongOperands)))
            (term (((\( (objr 4) \, (\( (\{ (\[ "__add" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( ("q" "q") \)))))
  
  ; E-AdditionWrongOperandsNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() \; (("q" + "q")AdditionWrongOperands)))
            (term (() \; ($builtIn error \( ("attempt to perform arithmetic on operands of the wrong type") \)))))
  
  ; E-SubstractionWrongOperandsWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 4) \, (\( (\{ (\[ "__sub" \] = 1) \}) \, nil \)) \))) 
                   \; (("q" - "q")SubstractionWrongOperands)))
            (term (((\( (objr 4) \, (\( (\{ (\[ "__sub" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( ("q" "q") \)))))
  
  ; E-SubstractionWrongOperandsNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() \; (("q" - "q")SubstractionWrongOperands)))
            (term (() \; ($builtIn error \( ("attempt to perform arithmetic on operands of the wrong type") \)))))
  
  ; E-MultiplicationWrongOperandsWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 4) \, (\( (\{ (\[ "__mul" \] = 1) \}) \, nil \)) \))) 
                   \; (("q" * "q")MultiplicationWrongOperands)))
            (term (((\( (objr 4) \, (\( (\{ (\[ "__mul" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( ("q" "q") \)))))
  
  ; E-MultiplicationWrongOperandsNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() \; (("q" * "q")MultiplicationWrongOperands)))
            (term (() \; ($builtIn error \( ("attempt to perform arithmetic on operands of the wrong type") \)))))
  
  ; E-DivisionWrongOperandsWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 4) \, (\( (\{ (\[ "__div" \] = 1) \}) \, nil \)) \))) 
                   \; (("q" / "q")DivisionWrongOperands)))
            (term (((\( (objr 4) \, (\( (\{ (\[ "__div" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( ("q" "q") \)))))
  
  ; E-DivisionWrongOperandsNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() \; (("q" / "q")DivisionWrongOperands)))
            (term (() \; ($builtIn error \( ("attempt to perform arithmetic on operands of the wrong type") \)))))
  
  ; E-ExponentiationWrongOperandsWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 4) \, (\( (\{ (\[ "__pow" \] = 1) \}) \, nil \)) \))) 
                   \; (("q" ^ "q")ExponentiationWrongOperands)))
            (term (((\( (objr 4) \, (\( (\{ (\[ "__pow" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( ("q" "q") \)))))
  
  ; E-ExponentiationWrongOperandsNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() \; (("q" ^ "q")ExponentiationWrongOperands)))
            (term (() \; ($builtIn error \( ("attempt to perform arithmetic on operands of the wrong type") \)))))
  
  ; E-ModuleWrongOperandsWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 4) \, (\( (\{ (\[ "__mod" \] = 1) \}) \, nil \)) \))) 
                   \; (("q" % "q")ModuleWrongOperands)))
            (term (((\( (objr 4) \, (\( (\{ (\[ "__mod" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( ("q" "q") \)))))
  
  ; E-ModuleWrongOperandsNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() \; (("q" % "q")ModuleWrongOperands)))
            (term (() \; ($builtIn error \( ("attempt to perform arithmetic on operands of the wrong type") \)))))
  
  ; E-NegationWrongOperandWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 4) \, (\( (\{ (\[ "__unm" \] = 1) \}) \, nil \)) \))) 
                   \; ((- "q")NegationWrongOperand)))
            (term (((\( (objr 4) \, (\( (\{ (\[ "__unm" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( ("q") \)))))
  
  ; E-NegationWrongOperandsNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() \; ((- "q")NegationWrongOperand)))
            (term (() \; ($builtIn error \( ("attempt to perform arithmetic on a string value") \)))))
  
  ; E-StringConcatWrongOperandsWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, (\( (\{ (\[ "__concat" \] = 1) \}) \, nil \)) \))) 
                   \; ((1 .. "q")StringConcatWrongOperands)))
            (term (((\( (objr 1) \, (\( (\{ (\[ "__concat" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( (1 "q") \)))))
  
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, (\( (\{ (\[ "__concat" \] = 1) \}) \, nil \)) \))) 
                   \; (("q" .. 1)StringConcatWrongOperands)))
            (term (((\( (objr 1) \, (\( (\{ (\[ "__concat" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( ("q" 1) \)))))
  
  ; E-StringConcatWrongOperandsWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() \; ((1 .. "q")StringConcatWrongOperands)))
            (term (() \; ($builtIn error \( ("attempt to apply string concatenation over operands of the wrong type") \)))))
  
  ; E-StringLengthWrongOperandWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, (\( (\{ (\[ "__len" \] = 1) \}) \, nil \)) \))) 
                   \; ((\# 1)StringLengthWrongOperand)))
            (term (((\( (objr 1) \, (\( (\{ (\[ "__len" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( (1) \)))))
  
  ; E-StringLengthWrongOperandTableLength
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, (\( (\{ \}) \, nil \)) \))) 
                   \; ((\# (objr 1))StringLengthWrongOperand)))
            (term (((\( (objr 1) \, (\( (\{ \}) \, nil \)) \))) 
                   \; 0)))
  
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 1)
                                            (\[ "a" \] = 1)
                                            (\[ 2 \] = 1) \}) \, nil \)) \))) 
                   \; ((\# (objr 1))StringLengthWrongOperand)))
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 1)
                                            (\[ "a" \] = 1)
                                            (\[ 2 \] = 1) \}) \, nil \)) \))) 
                   \; 2)))
  
  ; E-StringLengthWrongOperandNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() 
                   \; ((\# 1)StringLengthWrongOperand)))
            (term (() 
                   \; ($builtIn error \( ("attempt to get length of a number value") \)))))
  
  ; E-EqualityFailWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() 
                   \; ((1 == 2)EqualityFail)))
            (term (() 
                   \; false)))
  
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, (\( (\{ \}) \, nil \)) \)) 
                    (\( (objr 2) \, (\( (\{ \}) \, nil \)) \))) 
                   \; (((objr 1) == (objr 2))EqualityFail)))
            (term (((\( (objr 1) \, (\( (\{ \}) \, nil \)) \)) 
                    (\( (objr 2) \, (\( (\{ \}) \, nil \)) \))) 
                   \; false)))
  
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, (\( (\{ \}) \, (objr 3) \)) \)) 
                    (\( (objr 2) \, (\( (\{ \}) \, (objr 3) \)) \))
                    (\( (objr 3) \, (\( (\{ (\[ "__eq" \] = 1) \}) \, nil \)) \))) 
                   \; (((objr 1) == (objr 2))EqualityFail)))
            (term (((\( (objr 1) \, (\( (\{ \}) \, (objr 3) \)) \)) 
                    (\( (objr 2) \, (\( (\{ \}) \, (objr 3) \)) \))
                    (\( (objr 3) \, (\( (\{ (\[ "__eq" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( ((objr 1) (objr 2)) \)))))
  
  ; E-EqualityFailNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 1) \, (\( (\{ \}) \, (objr 3) \)) \)) 
                    (\( (objr 2) \, (\( (\{ \}) \, nil \)) \))
                    (\( (objr 3) \, (\( (\{ (\[ "__eq" \] = 1) \}) \, nil \)) \))) 
                   \; (((objr 1) == (objr 2))EqualityFail)))
            (term (((\( (objr 1) \, (\( (\{ \}) \, (objr 3) \)) \)) 
                    (\( (objr 2) \, (\( (\{ \}) \, nil \)) \))
                    (\( (objr 3) \, (\( (\{ (\[ "__eq" \] = 1) \}) \, nil \)) \))) 
                   \; false)))
  
  ; E-LessThanFailWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 4) \, (\( (\{ (\[ "__lt" \] = 1) \}) \, nil \)) \))) 
                   \; (("a" < 1)LessThanFail)))
            (term (((\( (objr 4) \, (\( (\{ (\[ "__lt" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( ("a" 1) \)))))
  
  ; E-LessThanFailNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() \; (("a" < 1)LessThanFail)))
            (term (() \; ($builtIn error \( ("attempt to compare string with number") \)))))
  
  ; E-LessThanOrEqualFailWithHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 4) \, (\( (\{ (\[ "__le" \] = 1) \}) \, nil \)) \))) 
                   \; (("a" <= 1)LessThanOrEqualFail)))
            (term (((\( (objr 4) \, (\( (\{ (\[ "__le" \] = 1) \}) \, nil \)) \))) 
                   \; (1 \( ("a" 1) \)))))
  
  ; E-LessThanOrEqualFailWithAltHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (((\( (objr 4) \, (\( (\{ (\[ "__lt" \] = 1) \}) \, nil \)) \))) 
                   \; (("a" <= 1)LessThanOrEqualFail)))
            (term (((\( (objr 4) \, (\( (\{ (\[ "__lt" \] = 1) \}) \, nil \)) \))) 
                   \; (not (1 \( (1 "a") \))))))
  
  ; E-LessThanOrEqualFailNoHandler
  (test-->> core-lang-abnormal-expressions-red
            (term (() \; (("a" <= 1)LessThanOrEqualFail)))
            (term (() \; ($builtIn error \( ("attempt to compare string with number") \)))))
  

  (test-results))

(provide abnormal-expressions-red-test-suite)