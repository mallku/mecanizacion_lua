#lang scheme
; Expressions that interact with the "simple value" store

(require redex
         "../grammar.scm"
         "../Meta-functions/substitution.scm"
         "../Meta-functions/simpValStoreMetafunctions.scm"
         "../Meta-functions/misc.scm"
         )


(define core-lang-sv-store-red
  (reduction-relation
   core-lang
   #:domain (σ \; exp)
   ; Implicit dereferencing
   [--> (σ \; r)
        (σ \; (derefSigma σ r))
        E-RefDeref]
   ))
(provide core-lang-sv-store-red)
