#lang racket
(require redex)
; Core language grammar definition
(define-language core-lang                                                          
  [block statement 
         (block \; block)
         (break Name tuple)
         blockAbnormal]
  
  ; Abnormal situations
  [blockAbnormal ((((objref \[ simplevalue \])) = (simplevalue))TableAssignmentWrongKey)
                 ((((simplevalue \[ simplevalue \])) = (simplevalue))TableAssignOverNonTableVal)]
  
  [label (:: Name ::)]
  
  [statement (label \{ block \}) 
             (do block end)
             (err simplevalue)
             ((block)ProtectedMode)
             functioncall
             (if exp then block else block end)
             (while exp do block end)
             (local namelist = explist in block end)
             (varlist = explist)
             void]
  
  [exp <<<
       var
       ; Is "err" and not "error", to free the "error" word to the error built-in
       ; procedure call 
       (err simplevalue)
       ((exp)ProtectedMode)
       functioncall 
       (\( exp \))
       nil 
       Boolean 
       Number 
       String 
       tuple 
       objref
       tableconstructor 
       functiondef 
       simpvalref
       (exp binop exp)
       (unop exp)
       ; Extension
       block
       ; Erroneous expressions
       expAbnormal]
  
  ; Abnormal expressions
  [expAbnormal ((objref \[ simplevalue \])KeyNotFound)
               ((simplevalue \[ simplevalue \])NonTableIndexed)
               ((sv + sv)AdditionWrongOperands)
               ((sv - sv)SubstractionWrongOperands)
               ((sv * sv)MultiplicationWrongOperands)
               ((sv ^ sv)ExponentiationWrongOperands)
               ((sv / sv)DivisionWrongOperands)
               ((sv % sv)ModuleWrongOperands)
               ((- sv)NegationWrongOperand)
               ((sv .. sv)StringConcatWrongOperands)
               ((\# sv)StringLengthWrongOperand)
               ((sv == sv)EqualityFail)
               ((sv < sv)LessThanFail)
               ((sv <= sv)LessThanOrEqualFail)
               ((sv \( svlist \))WrongFunctionCall)]
  
  
  [binop normalbinop shortcircuitbinop]
  
  [normalbinop + - * / ^ % .. < <= > >= ==]
  
  [shortcircuitbinop and or]
  
  [unop - not \#]
  
  [functioncall (exp \( explist \))
                (exp : Name \( explist \))
                ; Built-in procedures call
                ($builtIn Name \( explist \))
                ]
  
  [functiondef (function Name funcbody)]
  
  
  [funcbody (\( parlist \) block end)]
  
  ; Note: we separate the namelist from the vararg expression to allow extracting
  ; by pattern matching the namelist
  [parlist namelist 
           (namelist <<<)
           (Name ... <<<)]
  
  ; Non-terminal symbol and productions used just to describe some patterns
  ; in meta-functions
  [parameter Name <<<]
  
  [parameters (parameter ...)]
  
  ; Name can be anything except a keyword of the language
  [Name variable-not-otherwise-mentioned]
  
  [namelist (Name ...)]
  
  [prefixexp var functioncall (\( exp \))]
  
  [var Name 
       (exp \[ exp \])
       ; Extension
       evaluatedvar]
  
  [varlist (var ...)]
  
  [(evaluatedvar ev) simpvalref
                (objref \[ simplevalue \])]
  
  [evaluatedvarlist (evaluatedvar ...)]
  
  [tableconstructor (\{ field ... \})]
  
  [evaluatedtable (\{ evaluatedfield ... \})]
  
  [field (\[ exp \] = exp)
         ; We need to allow fields like this
         exp]
  
  ;; In PLT Redex, in order to describe an evaluation context that refers to a 
  ;; table expression that begins with fields (0 or more) whose parts are already 
  ;; evaluated, we need to use one non-terminal symbol that represents those 
  ;; constructions. That is the function of this production.
  [evaluatedfield (\[ simplevalue \] = simplevalue)
                  simplevalue]
  
  [intreptable (\( tableconstructor \, nil \)) (\( tableconstructor \, objref \))]
  
  [tuple empty (< explist >)]
  
  [evaluatedtuple empty (< (sv ...) >)]
  
  ; Values that we can store in a simple value store
  [(simplevalue sv) nil Boolean Number String objref]
  
  [svlist (sv ...)
          ]
  
  ; For simplicity in pattern description, we add this productions
  [Boolean true false]
  
  ; Number represents real (double-precision floating-point) numbers
  [Number real]
  
  ; TODO: A string in Lua represents immutable sequences of bytes.
  ; The String type of Racket is a sequence of Unicode scalar values, while
  ; the "bytes" type is exactly what we need: just a sequence of bytes. 
  [String string]
  
  ; Values that we can store in an object store
  [object functiondef 
          intreptable]
  
  [explist (exp ...)
           ; Extension
           (exp ... explist)]
  
  [(simpvalref r) (svr number)]
  
  [(objref l) (objr number)]
  
  ; For simplicity in pattern description, we add this production
  [(svstoreelement svste) (\( simpvalref \, simplevalue \))]
  
  [(objstoreelement objste) (\( objref \, object \))]
  
  [σ (svstoreelement ...)]
  
  [θ (objstoreelement ...)]
  
;                                                                  
;                                                                  
;                                                                  
;                                                                  
;     ;;;;                    ;                       ;            
;    ;   ;                    ;                       ;            
;   ;;       ;;;;   ; ;;;   ;;;;;;   ;;;;   ;;   ;  ;;;;;;   ;;;;; 
;   ;       ;;  ;;  ;;   ;    ;     ;;  ;;   ;  ;     ;     ;      
;   ;       ;    ;  ;    ;    ;     ;    ;    ;;      ;     ;;     
;   ;       ;    ;  ;    ;    ;     ;;;;;;    ;;      ;       ;;;  
;   ;;      ;    ;  ;    ;    ;     ;         ;;      ;          ; 
;    ;      ;;  ;;  ;    ;    ;     ;;       ;  ;     ;          ; 
;     ;;;;   ;;;;   ;    ;     ;;;   ;;;;;  ;    ;     ;;;  ;;;;;  
;                                                                  
;                                                                  
;                                                                  
;                                                                  
  
  ; List of expression: all of them are evaluated from left to right
  [Ele hole (simplevalue ... Ej exp ...)]
  
  ; Contexts without a break statement or a labelled block
  [Ej hole
     ; Statements
     (do Ej end)
     ((Ej)ProtectedMode)
     (if Ej then block else block_2 end)
     (local namelist = Ele in block end)
     (Ej \; block)
     ((evaluatedvar ... (Ej \[ exp \]) var ...) = explist)
     ((evaluatedvar ... (simplevalue \[ Ej \]) var ...) = explist)
     (evaluatedvarlist = Ele)
     ; Expressions
     ($builtIn Name \( Ele \))
     (Ej \( explist \))
     (simplevalue \( Ele \))
     (Ej : Name \( explist \))
     (\( Ej \))
     (Ej binop exp)
     (simplevalue normalbinop Ej)
     (unop Ej)
     (< Ele >)
     (\{ evaluatedfield ... (\[ Ej \] = exp) field ... \})
     (\{ evaluatedfield ... (\[ simplevalue \] = Ej) field ... \})
     (\{ evaluatedfield ... Ej field ... \})
     (Ej \[ exp \])
     (simplevalue \[ Ej \])] 
  
  [Eles (simplevalue ... E exp ...)]
  
  ; All possible contexts of statements
  ; TODO: it is possible to re-utitilize Ej?
  [E hole
     ; Statements
     (do E end)
     ((E)ProtectedMode)
     (if E then block else block_2 end)
     (local namelist = Eles in block end)
     (E \; block)
     ((evaluatedvar ... (E \[ exp \]) var ...) = explist)
     ((evaluatedvar ... (simplevalue \[ E \]) var ...) = explist)
     (evaluatedvarlist = Eles)
     (label \{ E \})
     (break Name (< Eles >))
     ; Expressions
     (E \( explist \))
     (simplevalue \( Eles \))
     ($builtIn Name \( Eles \))
     (E : Name \( explist \))
     (\( E \))
     (E binop exp)
     (simplevalue normalbinop E)
     (unop E)
     (< Eles >)
     (\{ evaluatedfield ... (\[ E \] = exp) field ... \})
     (\{ evaluatedfield ... (\[ simplevalue \] = E) field ... \})
     (\{ evaluatedfield ... E field ... \})
     (E \[ exp \])
     (simplevalue \[ E \])]
  
  ; Not-protected contexts
  ; TODO: it is possible to re-utitilize Ej?
  [Enples (simplevalue ... Enp exp ...)]
  [Enp hole
     ; Statements
     (do Enp end)
     (if Enp then block else block_2 end)
     (local namelist = Eles in block end)
     (Enp \; block)
     ((evaluatedvar ... (Enp \[ exp \]) var ...) = explist)
     ((evaluatedvar ... (simplevalue \[ Enp \]) var ...) = explist)
     (evaluatedvarlist = Enples)
     (label \{ Enp \})
     (break Name (< Enples >))
     ; Expressions
     (Enp \( explist \))
     (simplevalue \( Enples \))
     ($builtIn Name \( Enples \))
     (Enp : Name \( explist \))
     (\( Enp \))
     (Enp binop exp)
     (simplevalue normalbinop Enp)
     (unop Enp)
     (< Enples >)
     (\{ evaluatedfield ... (\[ Enp \] = exp) field ... \})
     (\{ evaluatedfield ... (\[ simplevalue \] = Enp) field ... \})
     (\{ evaluatedfield ... Enp field ... \})
     (Enp \[ exp \])
     (simplevalue \[ Enp \])]
  
  ; List of expressions where a tuple is truncated
  [Etel (simplevalue ... hole exp_1 exp ...)]
  ; Phrases where a tuple is truncated
  [Et (hole \( explist \))
      (simplevalue \( Etel \))
      ($builtIn Name \( Etel \))
      (hole : Name \( explist \))
      (hole binop exp)
      (simplevalue normalbinop hole)
      (unop hole)
      (\( hole \))
      (< Etel >)
      (\{ evaluatedfield ... (\[ hole \] = exp) field ... \})
      (\{ evaluatedfield ... (\[ simplevalue \] = hole) field ... \})
      (\{ evaluatedfield ... hole field_1 field ... \})
      (hole \[ exp \])
      (simplevalue \[ hole \])
      (if hole then block else block_2 end)
      (local namelist = Etel in block end)
      ((evaluatedvar ... (hole \[ exp \]) var ...) = explist)
      ((evaluatedvar ... (simplevalue \[ hole \]) var ...) = explist)
      (evaluatedvarlist = Etel)
      (break Name (< Etel >))]
  
  ; List of expressions where a tuple is unwrapped
  [Euel (simplevalue ... hole)]
  ; Phrases where a tuple is unwrapped
  [Eu (simplevalue \( Euel \))
      ($builtIn Name \( Euel \))
      (< Euel >)
      (\{ evaluatedfield ... hole \})
      (local namelist = Euel in block end)
      (evaluatedvarlist = Euel)
      (break Name (< Euel >))]
  
  ; Contexts where tuples are discarded
  [Ed hole
     (in-hole E (do hole end))
     (in-hole E (hole \; block))
     (in-hole E (label \{ hole \}))]
  
  ; Contexts where tuples are kept
  [Ek (in-hole E Et) 
      (in-hole E Eu)]
  )

; Export core-lang grammar definition
(provide core-lang)
