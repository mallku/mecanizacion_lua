#lang racket
(require redex
         "../../grammar.scm"
         "../../executionEnvironment.scm"
         "../../Reductions/standardReductionRelation.scm")
(define library
  (term (plugIntoExecutionEnvironment
(local (a) = ((\{  \})) in 
((      (do 
       (local ($1 $2 $3) = (($builtIn toNumber \( (1.0 10) \)) 
                            ($builtIn toNumber \( (100.0 10) \)) 
                            ($builtIn toNumber \( (1.0 10) \)))
        in
           ((if (not (($1 and $2) and $3)) then
              ($builtIn error \( ("'for' limit must be a number") \))
             else
              void
             end) \;
           ((:: $while ::) \{ 
            (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
              (local (i) = ($1) in
               (      (((a \[ (i .. "+") \])) = (true)) \; 
               (($1) = (($1 + $3))))
              end)
            end)
           \}))
       end)
      end)\;
      (do 
       (local ($1 $2 $3) = (($builtIn toNumber \( (1.0 10) \)) 
                            ($builtIn toNumber \( (100.0 10) \)) 
                            ($builtIn toNumber \( (1.0 10) \)))
        in
           ((if (not (($1 and $2) and $3)) then
              ($builtIn error \( ("'for' limit must be a number") \))
             else
              void
             end) \;
           ((:: $while ::) \{ 
            (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
              (local (i) = ($1) in
               (      (((a \[ (i .. "+") \])) = (nil)) \; 
               (($1) = (($1 + $3))))
              end)
            end)
           \}))
       end)
      end))\;
      (do 
       (local ($1 $2 $3) = (($builtIn toNumber \( (1.0 10) \)) 
                            ($builtIn toNumber \( (100.0 10) \)) 
                            ($builtIn toNumber \( (1.0 10) \)))
        in
           ((if (not (($1 and $2) and $3)) then
              ($builtIn error \( ("'for' limit must be a number") \))
             else
              void
             end) \;
           ((:: $while ::) \{ 
            (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
              (local (i) = ($1) in
               ((      (((a \[ i \])) = (true))\;
      ((ENV \["assert"\]) \( (((\# a) == i)) \))) \; 
               (($1) = (($1 + $3))))
              end)
            end)
           \}))
       end)
      end))
 end)








 
)))

(define (void? red)
  (and (redex-match core-lang
              (σ \; θ \; void)
              (first red))
       (eq? (length red) 1)))

(define (lua-nextvar-test-suite)
  (test-predicate void? (apply-reduction-relation* core-lan-stand-reduc-red-rel library))
  (test-results))

(provide lua-nextvar-test-suite)