#lang racket
(require redex
         "../grammar.scm"
         "./tablesMetafunctions.scm"
         "./objStoreMetafunctions.scm")


(define (eventHasHandler handler)
                 (and (not (equal? handler (term nil)))
                      (not (equal? handler (term false)))))

; Chooses a handler for a binary operation
; PRE : {sv_1, sv_2 are the operands and String is the string that serves as
;       key to index the meta-table
; ret = (getBinHandler sv_1 sv_2 String θ)
; POS : {returns the value of sv_1's meta-table indexed with key String (if
;        it belongs to the meta-table and the value is not nil or false) or
;        it returns the value of sv_2's meta-table indexed with key String}
(define-metafunction core-lang
  getBinHandler : sv sv String θ -> sv
  
  [(getBinHandler sv_1 sv_2 String θ)
   any
   
   ; Determine if sv_1 has meta-table
   (where any (indexMetaTable sv_1 String θ))
   (side-condition (eventHasHandler (term any)))]
  
  [(getBinHandler sv_1 sv_2 String θ)
   any_2
   
   ; Determine if sv_1 has meta-table
   (where any (indexMetaTable sv_1 String θ))
   (side-condition (not (eventHasHandler (term any))))
   (where any_2 (indexMetaTable sv_2 String θ))
   (side-condition (eventHasHandler (term any_2)))]
  
  ; Otherwise...
  [(getBinHandler sv_1 sv_2 String θ)
   nil])

(provide getBinHandler)

; Chooses a handler for an unary operation
; PRE : {sv is the operand and String is the string that serves as
;       key to index the meta-table
; ret = (getUnaryHandler sv String θ)
; POS : {returns the value of sv's meta-table indexed with key String (if
;        it belongs to the meta-table and the value is not nil or false) or
;        nil}
(define-metafunction core-lang
  getUnaryHandler : sv String θ -> sv
  
  [(getUnaryHandler sv String θ)
   any
   ; Determine if sv has meta-table
   (where any (indexMetaTable sv String θ))
   (side-condition (eventHasHandler (term any)))]
  
  ; Otherwise...
  [(getUnaryHandler sv String θ)
   nil])

(provide getUnaryHandler)

(define-metafunction core-lang
  getMetaTable : exp Boolean θ -> any
  
  ; Table value has a meta-table, which also has a "__metatable" key
  [(getMetaTable objref true θ)
   (rawGet tableconstructor "__metatable")
   
   (side-condition (eq? (term (type objref θ)) (term "table")))
   (where any (getMetaTableOfTable (derefTheta θ objref)))
   (side-condition (not (eq? (term any) (term nil))))
   (where tableconstructor (getTable (derefTheta θ any)))
   (side-condition (term (keyBelongsTo?  tableconstructor "__metatable")))]
  
  ; Table value has a meta-table, which does not have a "__metatable" key,
  ; or there is no interest in obtain the value associated with "__metatable".
  [(getMetaTable objref Boolean θ)
   any
   
   (side-condition (eq? (term (type objref θ)) (term "table")))
   (where any (getMetaTableOfTable (derefTheta θ objref)))
   (side-condition (not (eq? (term any) (term nil))))
   (where tableconstructor (getTable (derefTheta θ any)))
   (side-condition (or (not (term (keyBelongsTo?  tableconstructor "__metatable")))
                       (not (eq? (term Boolean) (term true)))))]
  
  ; Table value has not a meta-table
  [(getMetaTable objref Boolean θ)
   nil
   (side-condition (eq? (term (type objref θ)) (term "table")))]
  
  ; Type has a meta-table, which also has a "__metatable" key
  [(getMetaTable any true θ)
   (rawGet tableconstructor "__metatable")
   
   (where objref (getMetaTableRef (type any θ)))
   (side-condition (term (refBelongsToTheta objref θ)))
   (where tableconstructor (getTable (derefTheta θ objref)))
   (side-condition (term (keyBelongsTo?  tableconstructor "__metatable")))]
  
  ; Type has a meta-table, which does not has a "__metatable" key,
  ; or there is no interest in obtain the value associated with "__metatable".
  [(getMetaTable any Boolean θ)
   objref
   
   (where objref (getMetaTableRef (type any θ)))
   (side-condition (term (refBelongsToTheta objref θ)))
   (where tableconstructor (getTable (derefTheta θ objref)))
   (side-condition (or (not (term (keyBelongsTo?  tableconstructor "__metatable")))
                       (not (eq? (term Boolean) (term true)))))]
  
  ; Type has not a meta-table
  [(getMetaTable any Boolean θ)
   nil]
  )

(provide getMetaTable)

; Returns the predefined location where a meta-table for an indicated type,
; different from type "table", must be stored
(define-metafunction core-lang
  [(getMetaTableRef "number")
   (objr 1)]
  
  [(getMetaTableRef "nil")
   (objr 2)]
  
  [(getMetaTableRef "boolean")
   (objr 3)]
  
  [(getMetaTableRef "string")
   (objr 4)]
  
  [(getMetaTableRef "function")
   (objr 5)])


; Meta-function that tries to get the meta-table of a given value and index it
; with a given key. If it not success, it returns nil.
; PRE : {sv_1 is the value whose meta-table we want to index and sv_2 is the
;        key}
; ret = (indexMetaTable sv_1 sv_2 θ)
(define-metafunction core-lang
  [(indexMetaTable sv_1 sv_2 θ)
   (rawGet (getTable any_2) sv_2)
   
   (where any_1 (getMetaTable sv_1 false θ))
   (side-condition (not (equal? (term any_1) (term nil))))
   (side-condition (not (equal? (term sv_2) "__metatable")))
   (side-condition (term (refBelongsToTheta any_1 θ)))
   (where any_2 (derefTheta θ any_1))]
  
  [(indexMetaTable sv_1 "__metatable" θ)
   (rawGet (getTable any_2) "__metatable")
   
   (where any_1 (getMetaTable sv_1 false θ))
   (side-condition (not (equal? (term any_1) (term nil))))
   (side-condition (term (refBelongsToTheta any_1 θ)))
   (where any_2 (derefTheta θ any_1))
   ;(getMetaTable sv_1 true θ)
   ]
  
  [(indexMetaTable sv_1 sv_2 θ)
   nil
   
   (where any_1 (getMetaTable sv_1 false θ))
   (side-condition (equal? (term any_1) (term nil)))])

(provide indexMetaTable)

; Obtain a handler for an equality comparison, following the criterion defined
; in the procedure of the same name, in Lua's reference manual
(define-metafunction core-lang 
  
  ; The values compared are tables, with the same handler for the equality
  ; comparison
  [(getEqualHandler sv_1 sv_2 θ)
   any_1
   
   (side-condition (equal? (term (type sv_1 θ))
                           (term (type sv_2 θ))))
   (side-condition (equal? (term (type sv_1 θ))
                           "table"))
   (where any_1 (indexMetaTable sv_1 "__eq" θ))
   (where any_2 (indexMetaTable sv_2 "__eq" θ))
   (side-condition (equal? (term any_1) (term any_2)))]
  
  ; The values compared are tables, with the different handlers for the equality
  ; comparison
  [(getEqualHandler sv_1 sv_2 θ)
   nil
   
   (side-condition (equal? (term (type sv_1 θ))
                           (term (type sv_2 θ))))
   (side-condition (equal? (term (type sv_1 θ))
                           "table"))]
  
  ; The types of the values compared are different, or they are not tables
  [(getEqualHandler sv_1 sv_2 θ)
   nil])

(provide getEqualHandler)