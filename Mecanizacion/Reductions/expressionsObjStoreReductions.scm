#lang racket
(require redex
         "../grammar.scm"
         "../Meta-functions/objStoreMetafunctions.scm"
         "../Meta-functions/coercion.scm"
         "../Meta-functions/delta.scm"
         "../Meta-functions/misc.scm"
         "../Meta-functions/tablesMetafunctions.scm"
         "../Meta-functions/metaTableMechanismMetafunctions.scm"
         "../Meta-functions/builtInProceduresMetaFunctions.scm")

(define core-lang-exp-obj-store-red
  (reduction-relation
   core-lang
   #:domain (θ \; exp)
   ; Function creation
   [--> (θ \; (function Name funcbody))
        ((addObject θ (function Name funcbody)) \; (freshObjRef θ))
        E-StoreClosure
        (where any (functionIsStored? θ (function Name funcbody)))
        (side-condition (not (term any)))]
   
   [--> (θ \; (function Name funcbody))
        (θ \; objref)
        E-ReuseClosure
        (where objref (functionIsStored? θ (function Name funcbody)))
        (side-condition (term objref))]
   
   ; Table creation
   [--> (θ \; evaluatedtable)
        ((addObject θ (addFields evaluatedtable)) \; (freshObjRef θ))
        E-CreateTable]
   
   ; Table indexing
   [--> (θ \; (objref \[ simplevalue \]))
        (θ \; (rawGet evaluatedtable simplevalue))
        E-IndexTable
        ; objref points to a table
        (side-condition (eq? (term (type objref θ)) (term "table")))
        ; The key is present in the table
        (where evaluatedtable (getTable (derefTheta θ objref)))
        (side-condition (term (keyBelongsTo? evaluatedtable simplevalue)))]
   
   ; Abnormal situations
   [--> (θ \; (objref \[ simplevalue \]))
        (θ \; ((objref \[ simplevalue \])KeyNotFound))
        E-AlertKeyNotFound
        ; Determine if we are trying to index a table value
        (side-condition (eq? (term (type objref θ)) (term "table")))
        ; Determine that the key doesn't belongs to the table
        (side-condition (not (term (keyBelongsTo? (getTable (derefTheta θ objref))
                                                  simplevalue))))]
   
   [--> (θ \; (simplevalue \[ simplevalue_1 \]))
        (θ \; ((simplevalue \[ simplevalue_1 \])NonTableIndexed))
        E-AlertNonTableIndexed
        ; simplevalue is not a reference to a table
        (side-condition (not (eq? (term (type simplevalue θ)) (term "table"))))]
   
   ; Built-in procedures
   ; GetMetaTable
   [--> (θ \; ($builtIn getMetatable \( (simplevalue Boolean) \)))
        (θ \; (< (simplevalue_1) >))
        E-BuiltInGetMetaTable
        (where simplevalue_1 (getMetaTable simplevalue Boolean θ))]
   ; Type
   [--> (θ \; ($builtIn type \( (simplevalue_1) \)))
        (θ \; (< (simplevalue_2) >))
        E-BuiltInType
        (where simplevalue_2 (type simplevalue_1 θ))]
   ; Next
   [--> (θ \; ($builtIn next \( (simplevalue_1 simplevalue_2) \)))
        (θ \; tuple)
        E-BuiltInNext
        ; simplevalue_1 must be a pointer to a table
        (side-condition (eq? (term (type simplevalue_1 θ)) "table"))
        (where evaluatedtable (getTable (derefTheta θ simplevalue_1)))
        (side-condition (or (term (keyBelongsTo? evaluatedtable simplevalue_2))
                            (eq? (term simplevalue_2) (term nil))))
        (where tuple (next evaluatedtable simplevalue_2))
        ]
   [--> (θ \; ($builtIn next \( (simplevalue_1 simplevalue_2) \)))
        (θ \; ($builtIn error \( ("invalid key to 'next'") \)))
        E-BuiltInNextError
        ; simplevalue_1 must be a pointer to a table
        (side-condition (eq? (term (type simplevalue_1 θ)) "table"))
        (where evaluatedtable (getTable (derefTheta θ simplevalue_1)))
        (side-condition (not (or (term (keyBelongsTo? evaluatedtable simplevalue_2))
                            (eq? (term simplevalue_2) (term nil)))))
        ]
   ; RawEqual
   [--> (θ \; ($builtIn rawEqual \( (sv_1 sv_2) \)))
        (θ \; (< (sv_3) >))
        E-BuiltInRawEqual
        (where sv_3 (δ (== sv_1 sv_2)))
        ]
   ; RawGet
   [--> (θ \; ($builtIn rawGet \( (sv_1 sv_2) \)))
        (θ \; (< (sv_3) >))
        E-BuiltInRawGetNormal
        ; sv_1 must be a pointer to a table
        (side-condition (eq? (term (type sv_1 θ)) "table"))
        (where evaluatedtable (getTable (derefTheta θ sv_1)))
        (where sv_3 (rawGet evaluatedtable sv_2))
        ]
   ; RawLen
   [--> (θ \; ($builtIn rawLen \( (sv_1) \)))
        (θ \; (< (sv_2) >))
        E-BuiltInRawLenTable
        ; sv_1 must be a pointer to a table
        (side-condition (eq? (term (type sv_1 θ)) "table"))
        (where evaluatedtable (getTable (derefTheta θ sv_1)))
        (where sv_2 (δ (\# evaluatedtable)))
        ]
   [--> (θ \; ($builtIn rawLen \( (sv_1) \)))
        (θ \; (< (sv_2) >))
        E-BuiltInRawLenString
        ; sv_1 must be a pointer to a table
        (side-condition (eq? (term (type sv_1 θ)) "string"))
        (where sv_2 (δ (\# sv_1)))
        ]
   ; RawSet
   [--> (θ \; ($builtIn rawSet \( (sv_1 sv_2 sv_3) \)))
        (θ_2 \; (< (sv_1) >))
        E-BuiltInRawSet
        ; sv_1 must be a pointer to a table
        (where any (type sv_1 θ))
        (side-condition (and (eq? (term any) "table")
                             (not (eq? (term sv_2) (term nil)))))
        (where evaluatedtable_1 (getTable (derefTheta θ sv_1)))
        (where evaluatedtable_2 (assignTableField evaluatedtable_1 sv_2 sv_3))
        (where θ_2 (thetaAlter θ sv_1 evaluatedtable_2))
        ]
   ; setMetatable
   [--> (θ_1 \; ($builtIn setMetatable \( (sv_1 sv_2) \)))
        (θ_2 \; (< (sv_1) >))
        E-BuiltInSetMetatable
        ; sv_1 must be a pointer to a table
        (side-condition (eq? (term (type sv_1 θ_1)) "table"))
        (side-condition (or (eq? (term (type sv_2 θ_1)) "table")
                            (eq? (term sv_2) (term nil))))
        (side-condition (eq? (term (indexMetaTable sv_1 "__metatable" θ_1)) 
                             (term nil)))
        (where (\( tableconstructor \, sv_3 \)) (derefTheta θ_1 sv_1))
        (where θ_2 (thetaAlter θ_1 sv_1 (\( tableconstructor \, sv_2 \))))
        ]
   
   [--> (θ \; ($builtIn setMetatable \( (sv_1 sv_2) \)))
        (θ \; ($builtIn error \( (any_2) \)))
        E-BuiltInSetMetatableErrorNoMetaTable
        ; sv_1 must be a pointer to a table
        (side-condition (eq? (term (type sv_1 θ)) "table"))
        (where any (type sv_2 θ))
        (side-condition (or (and (not (eq? (term any) "table"))
                                 (not (eq? (term sv_2) (term nil))))
                            (not (eq? (term (indexMetaTable sv_1 "__metatable" θ)) 
                                      (term nil)))
                            ))
        (where any_2 ,(if (and (not (eq? (term any) "table"))
                                 (not (eq? (term sv_2) (term nil))))
                          "bad argument #2 to 'setmetatable' (nil or table expected)"
                          "cannot change a protected metatable"))
        ]
   ))

(provide core-lang-exp-obj-store-red)
