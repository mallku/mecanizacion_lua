#lang scheme
(require redex
         "../grammar.scm")

; Auxiliar meta-function that resolves the problem that occurs when
; unwrapping a list of values into another one
(define-metafunction core-lang
  [(fixUnwrap (sv_1 \( (sv_2 ... hole) \)) (< (simplevalue ...) >))
   (sv_1 \( (sv_2 ... simplevalue ...) \))]
  
  [(fixUnwrap (< (sv_1 ... hole) >) (< (sv_2 ...) >))
   (< (sv_1 ... sv_2 ...) >)]
  
  [(fixUnwrap (\{ evaluatedfield ... hole \}) (< (sv ...) >))
   (\{ evaluatedfield ... sv ... \})]
  
  [(fixUnwrap (local namelist = (sv_1 ... hole) in block end) (< (sv_2 ...) >))
   (local namelist = (sv_1 ... sv_2 ...) in block end)]
  
  [(fixUnwrap (evaluatedvarlist = (sv_1 ... hole)) (< (sv_2 ...) >))
   (evaluatedvarlist = (sv_1 ... sv_2 ...))]
  
  [(fixUnwrap (break Name (< (sv_1 ... hole) >)) (< (sv_2 ...) >))
   (break Name (< (sv_1 ... sv_2 ...) >))]
  
  [(fixUnwrap ($builtIn Name \( (sv_1 ... hole) \)) (< (sv_2 ...) >))
   ($builtIn Name \( (sv_1 ... sv_2 ...) \))]
  )

(provide fixUnwrap)

; Type function for simplevalues
(define-metafunction core-lang
  
  [(simpValType Number) ,(term "number")]
  [(simpValType nil) ,(term "nil")]
  [(simpValType Boolean) ,(term "boolean")]
  [(simpValType String) ,(term "string")]
  ;[(simpValType tuple) ,(term "Tuple")]
  [(simpValType objref) ,(term "objref")])

(provide simpValType)

(define-metafunction core-lang
  [(extractBlock (θ \; block))
   block])

(provide extractBlock)

(define-metafunction core-lang
  [(extractObjStore (θ \; block))
   θ])

(provide extractObjStore)