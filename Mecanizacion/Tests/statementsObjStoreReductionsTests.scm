#lang racket
(require redex
         "../grammar.scm"
         "../Reductions/statementsObjStoreReductions.scm")

(define (stat-obj-store-red-test-suite)
  ; Table assignment
  ; Normal assignment
;  (test-->> core-lang-stat-obj-store-red
;            (term (((\( (objr 1) \, 
;                        (\( (\{ (\[ 1 \] = 1) 
;                                (\[ 2 \] = 2) 
;                                (\[ 3 \] = nil) 
;                                (\[ 4 \] = 4) \}) \, nil \)) \))) 
;                   \; ((((objr 1) \[ 4 \])) = (5))))
;            (term (((\( (objr 1) \, 
;                        (\( (\{ (\[ 1 \] = 1) 
;                                (\[ 2 \] = 2) 
;                                (\[ 3 \] = nil) 
;                                (\[ 4 \] = 5) \}) \, nil \)) \))) 
;                   \; void)))
  
  (test-->> core-lang-stat-obj-store-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) 
                                (\[ 2 \] = 2) 
                                (\[ 3 \] = nil) 
                                (\[ 4 \] = 4) \}) \, nil \)) \))) 
                   \; ((((objr 1) \[ 4 \])) = (5))))
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) 
                                (\[ 2 \] = 2) 
                                (\[ 3 \] = nil) 
                                (\[ 4 \] = 5) \}) \, nil \)) \))) 
                   \; void)))
  
  ; Delete a field
  (test-->> core-lang-stat-obj-store-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) 
                                (\[ 2 \] = 2) 
                                (\[ 3 \] = nil) 
                                (\[ 4 \] = 4) \}) \, nil \)) \))) 
                   \; ((((objr 1) \[ 4 \])) = (nil))))
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) 
                                (\[ 2 \] = 2) 
                                (\[ 3 \] = nil) \}) \, nil \)) \))) \; void)))
  
  ; Trying to index with key nil
  (test-->> core-lang-stat-obj-store-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) 
                                (\[ 2 \] = 2) 
                                (\[ 3 \] = nil) 
                                (\[ 4 \] = 4) \}) \, nil \)) \))) 
                   \; ((((objr 1) \[ nil \])) = (1))))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) 
                                (\[ 2 \] = 2) 
                                (\[ 3 \] = nil) 
                                (\[ 4 \] = 4) \}) \, nil \)) \))) \; error)))
  
  ; E-AlertTableAssignmentWrongKey
  (test-->> core-lang-stat-obj-store-red
            (term (((\( (objr 1) \, 
                        (\( (\{ \}) \, nil \)) \))) 
                   \; ((((objr 1) \[ 1 \])) = (2))))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ \}) \, nil \)) \))) 
                   \; (((((objr 1) \[ 1 \])) = (2))TableAssignmentWrongKey))))
  
  ; E-AlertTableAssignOverNonTableVal
  (test-->> core-lang-stat-obj-store-red
            (term (() \; (((1 \[ 2 \])) = (3))))
            (term (() \; ((((1 \[ 2 \])) = (3))TableAssignOverNonTableVal))))                                                                
  
  (test-results))

(provide stat-obj-store-red-test-suite)