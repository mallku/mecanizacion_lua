#lang scheme
(require redex
         "../Meta-functions/substitution.scm")

; "black-box testing" with "equivalence class partitioning"

(define (subst-exp-test-suite)
  ; Expression without structure
  (test-equal (term (substExp nil (X) ((svr 1)))) (term nil))
  (test-equal (term (substExp true (X) ((svr 1)))) (term true))
  (test-equal (term (substExp void (X) ((svr 1)))) (term void))
  (test-equal (term (substExp "" (X) ((svr 1)))) (term ""))
  (test-equal (term (substExp 1 (X) ((svr 1)))) (term 1))
  (test-equal (term (substExp (objr 1) (X) ((svr 1)))) (term (objr 1)))
  (test-equal (term (substExp (svr 1) (X) ((svr 1)))) (term (svr 1)))
  (test-equal (term (substExp X (X) ((svr 1)))) (term (svr 1)))
  (test-equal (term (substExp X (Y) ((svr 1)))) (term X))
  (test-equal (term (substExp <<< (<<<) ((svr 1)))) (term (svr 1)))
  ; Function call
  (test-equal (term (substExp (X \( () \)) (X) ((svr 1)))) (term ((svr 1) \( () \))))
  (test-equal (term (substExp (X \( (1 2 3) \)) (X) ((svr 1)))) (term ((svr 1) \( (1 2 3) \))))
  (test-equal (term (substExp ((\( <<< \)) \( () \)) (<<<) ((svr 1))))
                    (term ((\( (svr 1) \)) \( () \))))
  ; '(' ')' operator
  (test-equal (term (substExp (\( X \)) (X) ((svr 1)))) (term (\( (svr 1) \))))
  (test-equal (term (substExp (\( <<< \)) (<<<) ((svr 1)))) (term (\( (svr 1) \))))
  ; Table indexing
  (test-equal (term (substExp (X \[ Y \]) (X Y) ((svr 1) (svr 2)))) 
              (term ((svr 1) \[ (svr 2) \])))
  (test-equal (term (substExp (X \[ <<< \]) (X <<<) ((svr 1) (svr 2)))) 
              (term ((svr 1) \[ (svr 2) \])))
  ; Tuples
  (test-equal (term (substExp (< (X Y) >) (X Y) ((svr 1) (svr 2)))) 
              (term (< ((svr 1) (svr 2)) >)))
  (test-equal (term (substExp (< (X <<<) >) (X <<<) ((svr 1) (svr 2)))) 
              (term (< ((svr 1) (svr 2)) >)))
  ; Function definition
  (test-equal (term (substExp (function A (\( () \) (X \( () \)) end)) (X) ((svr 1)))) 
              (term (function A (\( () \) ((svr 1) \( () \)) end))))
  
  (test-equal (term (substExp (function A (\( (X Y) \) ((X \( () \)) \; (Z \( () \))) 
                                 end)) (X) ((svr 1)))) 
              (term (function A (\( (X Y) \) ((X \( () \)) \; (Z \( () \))) end))))
  
  (test-equal (term (substExp (function A (\( ((X Y) <<<) \) ((X \( () \)) \; (Z \( () \))) 
                                 end)) (X) ((svr 1))))
              (term (function A (\( ((X Y) <<<) \) ((X \( () \)) \; (Z \( () \))) end))))
  
  (test-equal (term (substExp (function A (\( (X Y <<<) \) ((X \( () \)) \; (Z \( () \))) 
                                 end)) (Z <<<) ((svr 1) (svr 2))))
              (term (function A (\( (X Y <<<) \) ((X \( () \)) \; ((svr 1) \( () \))) end))))
  
  (test-equal (term (substExp (function A (\( (X Y) \) ((X \( () \)) \; (Z \( () \))) 
                                 end)) (Z) ((svr 1)))) 
              (term (function A (\( (X Y) \) ((X \( () \)) \; ((svr 1) \( () \))) end))))
  
  (test-equal (term (substExp (function A (\( (X Y <<<) \) ((X \( () \)) \; (Z \( () \))) 
                                            end)) (Z) ((svr 1)))) 
              (term (function A (\( (X Y <<<) \) ((X \( () \)) \; ((svr 1) \( () \))) end))))
  ; Table constructor
  (test-equal (term (substExp (\{ (\[ X \] = Y) (\[ Z \] = 1) \}) (X Y Z) 
                              ((svr 1) (svr 2) (svr 3))))
              (term (\{ (\[ (svr 1) \] = (svr 2)) (\[ (svr 3) \] = 1) \})))
  
  (test-equal (term (substExp (\{ (\[ X \] = Y) (\[ Z \] = 1) \}) (X Y Z) 
                              ((svr 1) (svr 2) (svr 3))))
              (term (\{ (\[ (svr 1) \] = (svr 2)) (\[ (svr 3) \] = 1) \})))
  
  (test-equal (term (substExp (\{ (\[ X \] = Y) (\[ <<< \] = 1) \}) (X Y <<<) 
                              ((svr 1) (svr 2) (svr 3))))
              (term (\{ (\[ (svr 1) \] = (svr 2)) (\[ (svr 3) \] = 1) \})))
  ; Binary operators
  (test-equal (term (substExp (X + Y) (X Y) ((svr 1) (svr 2)))) 
              (term ((svr 1) + (svr 2))))
  (test-equal (term (substExp (X + <<<) (X <<<) ((svr 1) (svr 2)))) 
              (term ((svr 1) + (svr 2))))
  ; Unary operators
  (test-equal (term (substExp (- X) (X) ((svr 1)))) (term (- (svr 1))))
  (test-results))


(define (subst-block-test-suite)
  ; Concatenation statement
  (test-equal (term (substBlock ((X \( () \)) \; (Y \( () \))) (X Y) ((svr 1) (svr 2)))) 
              (term (((svr 1) \( () \)) \; ((svr 2) \( () \)))))
  ; Labelled block
  (test-equal (term (substBlock ((:: X ::) \{ (X \( () \)) \}) (X) ((svr 1)))) 
              (term ((:: X ::) \{ ((svr 1) \( () \)) \})))
  
  (test-equal (term (substBlock ((:: X ::) \{ (X \( (<<<) \)) \}) (X <<<) ((svr 1) (svr 2)))) 
              (term ((:: X ::) \{ ((svr 1) \( ((svr 2)) \)) \})))
  ; Block Bo-End
  (test-equal (term (substBlock (do (X \( () \)) end) (X) ((svr 1)))) 
              (term (do ((svr 1) \( () \)) end)))
  ; Break statement
  (test-equal (term (substBlock (break X (< (X) >)) (X) ((svr 1)))) 
              (term (break X (< ((svr 1)) >))))
  
  (test-equal (term (substBlock (break X (< (<<<) >)) (<<<) ((svr 1)))) 
              (term (break X (< ((svr 1)) >))))
  ; Conditional
  (test-equal (term (substBlock (if X then (Y \( () \)) else (Z \( () \)) end) 
                                (X Y Z) ((svr 1) (svr 2) (svr 3))))
              (term (if (svr 1) then ((svr 2) \( () \)) else ((svr 3) \( () \)) end)))
  
  (test-equal (term (substBlock (if <<< then (Y \( () \)) else (Z \( () \)) end) 
                                (Y Z <<<) ((svr 1) (svr 2) (svr 3))))
              (term (if (svr 3) then ((svr 1) \( () \)) else ((svr 2) \( () \)) end)))
  ; While loop
  (test-equal (term (substBlock (while X do (Y \( () \)) end) (X Y) 
                                ((svr 1) (svr 2))))
              (term (while (svr 1) do ((svr 2) \( () \)) end)))
  
  (test-equal (term (substBlock (while <<< do (Y \( () \)) end) (Y <<<) 
                                ((svr 1) (svr 2))))
              (term (while (svr 2) do ((svr 1) \( () \)) end)))
  ; Local statement
  (test-equal (term (substBlock (local (X Y) = (X Y) in ((X \( () \)) \; (Y \( () \))) end) 
                                (X Y) ((svr 1) (svr 2))))
              (term (local (X Y) = ((svr 1) (svr 2)) in ((X \( () \)) \; (Y \( () \))) end)))
  
  (test-equal (term (substBlock (local (X Y) = (X Y) in ((U \( () \)) \; ((V \( () \)) \; 
                                                        ((X \( () \)) \; (Y \( () \))))) end) 
                                (U V) ((svr 1) (svr 2))))
              (term (local (X Y) = (X Y) in (((svr 1) \( () \)) \; (((svr 2) \( () \)) \; 
                                              ((X \( () \)) \; (Y \( () \))))) end)))
  
  (test-equal (term (substBlock (local (X Y) = (X Y) in ((U \( () \)) \; ((V \( (<<<) \)) \; 
                                                        ((X \( () \)) \; (Y \( () \))))) end) 
                                (U V <<<) ((svr 1) (svr 2) (svr 3))))
              (term (local (X Y) = (X Y) in (((svr 1) \( () \)) \; (((svr 2) \( ((svr 3)) \)) \; 
                                              ((X \( () \)) \; (Y \( () \))))) end)))
  ; Variable assignment
  (test-equal (term (substBlock ((X Y) = (U V)) (X Y U V) 
                                ((svr 1) (svr 2) (svr 3) (svr 4))))
              (term (((svr 1) (svr 2)) = ((svr 3) (svr 4)))))
  
  (test-equal (term (substBlock (((X \[ Y \])) = (U V)) (X Y U V) 
                                ((svr 1) (svr 2) (svr 3) (svr 4))))
              (term ((((svr 1) \[ (svr 2) \])) = ((svr 3) (svr 4)))))
  
  (test-equal (term (substBlock ((X Y) = (U <<<)) (X Y U <<<) 
                                ((svr 1) (svr 2) (svr 3) (svr 4))))
              (term (((svr 1) (svr 2)) = ((svr 3) (svr 4)))))
  
  (test-results))

(define (subs-test-suite)
  (subst-block-test-suite)
  (subst-exp-test-suite))

(provide subs-test-suite)