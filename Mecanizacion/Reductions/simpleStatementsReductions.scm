#lang racket
; Statements that don't interact with some store and whose behaviour is not
; context sensitive

(require redex
         "../grammar.scm"
         "../Meta-functions/simpValStoreMetafunctions.scm")

(define core-lang-simply-stat-red
  (reduction-relation
   core-lang
   #:domain block
   ; If statement
   [--> (if simplevalue then block_1 else block_2 end)
      block_1
      E-IfTrue
      (side-condition (and (not (eqv? (term simplevalue) (term nil)))
                           (not (eqv? (term simplevalue) (term false)))))]
   
   [--> (if simplevalue then block_1 else block_2 end)
      block_2
      E-IfFalse
      (side-condition (or (eqv? (term simplevalue) (term nil))
                          (eqv? (term simplevalue) (term false))))]
   ; While statement
   [--> (while exp do block end)
      (if exp then (block \; (while exp do block end)) else void end)
      E-While]
   ; Concatenation of statements
   [--> (void \; block)
        block
        E-ConcatBehavior]
   ; Do ... End block
   [--> (do void end)
        void
        E-DoEnd]
   ; List length-equating rules for assignment statements
   [--> (evaluatedvarlist = svlist_1)
        (evaluatedvarlist = svlist_2)
        E-AssignDiscardValues
        (side-condition
        (< (length (term evaluatedvarlist)) (length (term svlist_1))))
        (where svlist_2 (discard evaluatedvarlist svlist_1))
        ]
   
   [--> (evaluatedvarlist = svlist_1)
        (evaluatedvarlist = svlist_2)
        E-AssignCompleteValues
        (side-condition
        (> (length (term evaluatedvarlist)) (length (term svlist_1))))
        (where svlist_2 (complete evaluatedvarlist svlist_1))
        ]
   
   [--> ((ev_1 ev_2 ... ev_3) = (sv_1 sv_2 ... sv_3))
        (((ev_3) = (sv_3)) \; ((ev_1 ev_2 ...) = (sv_1 sv_2 ...)))
        E-AssignSplit
        (side-condition
         (= (length (term (ev_1 ev_2 ... ev_3))) 
            (length (term (sv_1 sv_2 ... sv_3)))))]
   
   [--> (local namelist = svlist_1 in block end)
        (local namelist = svlist_2 in block end)
        E-LocalDiscardValues
        (side-condition
         (< (length (term namelist)) (length (term svlist_1))))
        (where svlist_2 ,(term (discard namelist svlist_1)))]
   
   [--> (local namelist = svlist_1 in block end)
        (local namelist = svlist_2 in block end)
        E-LocalCompleteValues
        (side-condition
         (> (length (term namelist)) (length (term svlist_1))))
        (where svlist_2 ,(term (complete namelist svlist_1)))]
   ))

(provide core-lang-simply-stat-red)
