#lang racket
; Black-box testing for statements that interact with the simple value store

(require redex
         "../grammar.scm"
         "../Reductions/statementsSimpValStoreReductions.scm")

(define (stat-sv-store-red-test-suite)
  ; Ordinary variable assignment
  (test-->> core-lang-stat-sv-store-red
            (term (((\( (svr 1) \, 1 \)) (\( (svr 2) \, 2 \))) \; 
                  (((svr 1)) = (3))))
            (term (((\( (svr 1) \, 3 \)) (\( (svr 2) \, 2 \))) \; 
                  void)))
  ; Local statement
  (test-->> core-lang-stat-sv-store-red
            (term (() \; 
                  (local (X) = (1) in (X \( () \)) end)))
            (term (((\( (svr 1) \, 1 \))) \; 
                  ((svr 1) \( () \)))))
  (test-->> core-lang-stat-sv-store-red
            (term (() \; 
                  (local (X Y) = (1 nil) in (X \( () \)) end)))
            (term (((\( (svr 1) \, 1 \)) (\( (svr 2) \, nil \))) \; 
                  ((svr 1) \( () \)))))
  (test-results))

(provide stat-sv-store-red-test-suite)