#lang racket
(require redex
         "../grammar.scm"
         "../Reductions/expressionsSimpValObjStoreReductions.scm")

(define (exp-sv-obj-store-red-test-suite)
  ; Function call
  ; Normal case
  (test-->> core-lang-exp-sv-obj-store-red
            (term (((\( (svr 1) \, 1 \)) (\( (svr 2) \, 2 \))) 
                   \; ((\( (objr 1) \, (function X (\( (Y) \) (Y \( () \)) end)) \))) 
                   \; ((objr 1) \( (2) \))))
            
            (term (((\( (svr 1) \, 1 \)) (\( (svr 2) \, 2 \)) (\( (svr 3) \, 2 \))) 
                   \; ((\( (objr 1) \, (function X (\( (Y) \) (Y \( () \)) end)) \))) 
                   \; ((svr 3) \( () \)))))
  
  ; More values than needed passed in the call
  (test-->> core-lang-exp-sv-obj-store-red
            (term (((\( (svr 1) \, 1 \)) (\( (svr 2) \, 2 \))) 
                   \; ((\( (objr 1) \, (function X (\( (Y) \) (Y \( () \)) end)) \))) 
                   \; ((objr 1) \( (2 1) \))))
            
            (term (((\( (svr 1) \, 1 \)) (\( (svr 2) \, 2 \)) (\( (svr 3) \, 2 \))) 
                   \; ((\( (objr 1) \, (function X (\( (Y) \) (Y \( () \)) end)) \))) 
                   \; ((svr 3) \( () \)))))
  
  ; Lesser values than needed passed in the call
  (test-->> core-lang-exp-sv-obj-store-red
            (term (((\( (svr 1) \, 1 \)) (\( (svr 2) \, 2 \))) 
                   \; ((\( (objr 1) \, (function X (\( (Y Z) \) (Z \( () \)) end)) \))) 
                   \; ((objr 1) \( (2) \))))
            
            (term (((\( (svr 1) \, 1 \)) (\( (svr 2) \, 2 \)) (\( (svr 3) \, 2 \)) 
                                         (\( (svr 4) \, nil \))) 
                   \; ((\( (objr 1) \, (function X (\( (Y Z) \) (Z \( () \)) end)) \))) 
                   \; ((svr 4) \( () \)))))
  
  ; Vararg function: normal case
  (test-->> core-lang-exp-sv-obj-store-red
            (term (() 
                   \; ((\( (objr 1) \, (function X (\( (X <<<) \) ((\( <<< \)) \( () \)) 
                                                    end)) \))) 
                   \; ((objr 1) \( (1 2) \))))
            
            (term (((\( (svr 1) \, 1 \)))
                   \; ((\( (objr 1) \, (function X (\( (X <<<) \) ((\( <<< \)) \( () \)) 
                                                    end)) \))) 
                   \; ((\( (< (2) >) \)) \( () \)))))
  
  ; Vararg function: few arguments
  (test-->> core-lang-exp-sv-obj-store-red
            (term (() 
                   \; ((\( (objr 1) \, (function X (\( (X Y <<<) \) ((\( <<< \)) \( () \)) 
                                                    end)) \))) 
                   \; ((objr 1) \( (1) \))))
            
            (term (((\( (svr 1) \, 1 \)) (\( (svr 2) \, nil \))) 
                   \; ((\( (objr 1) \, (function X (\( (X Y <<<) \) 
                                                       ((\( <<< \)) \( () \)) end)) \))) 
                   \; ((\( empty \)) \( () \)))))
  
  ; E-AlertWrongFunctionCall
  (test-->> core-lang-exp-sv-obj-store-red
            (term (() \; () \; (1 \( (1) \))))
            
            (term (() \; () \; ((1 \( (1) \))WrongFunctionCall))))
  
  (test-results))

(provide exp-sv-obj-store-red-test-suite)