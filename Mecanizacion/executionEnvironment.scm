#lang racket
(require redex
         "./grammar.scm")

(define-metafunction core-lang
  [(plugIntoExecutionEnvironment block)
   (in-hole 
    (()
       \; ()
       \; (local (ENV) = ((\{ \})) 
            in (
                (; Basic functions
                (((ENV \[ "assert" \])) = ((function 
                                           $assert 
                                           (\( (v message) \) 
                                               ((:: $ret ::) 
                                                \{ 
                                                (break $ret 
                                                       (< (($builtIn assert \( (v message) \))) >))
                                                \}) 
                                               end)))) 
                \; 
                ((((ENV \[ "type" \])) = ((function 
                                         $type 
                                         (\( (value) \) 
                                             ((:: $ret ::) 
                                              \{ 
                                              (break $ret 
                                                     (< (($builtIn type \( (value) \))) >))
                                              \}) 
                                             end))))
                 \;
                 ((((ENV \[ "pcall" \])) = ((function 
                                         $pcall 
                                         (\( (sv <<<) \) 
                                             ((:: $ret ::) 
                                              \{ 
                                              (break $ret 
                                                     (< (($builtIn pcall \( (sv <<<) \))) >))
                                              \}) 
                                             end))))
                  \;
                  ((((ENV \[ "tonumber" \])) = ((function $tonumber 
                                           (\( (e base) \)
                                               (if (base == nil) then
                                                   ((:: $ret ::) \{ 
                                                                 (break $ret 
                                                       (< (($builtIn toNumber \( (e 10) \))) >))
                                                                 \})
                                                else
                                                   ((:: $ret ::) \{ 
                                                                 (break $ret 
                                                       (< (($builtIn toNumber \( (e base) \))) >))
                                                                 \}) 
                                                end)
                                               end))))
                   \;
                   ((((ENV \[ "rawget" \])) = ((function 
                                         $rawget 
                                         (\( (table index) \) 
                                             ((:: $ret ::) 
                                              \{ (local (typeTable) = (($builtIn type \( (table) \)))
                                                   in
                                                   (if (not (typeTable == "table"))
                                                   then 
                                                   ($builtIn error \( ((("bad argument #1 to 'rawget' (table expected, got "
                                                                       .. typeTable) .. ")")) \))
                                                   else
                                                   (break $ret 
                                                         (< (($builtIn 
                                                              rawGet
                                                              \( (table 
                                                                  index) 
                                                              \))) >))
                                                   end)
                                                   end)
                                              \}) 
                                             end))))
                    \;
                    (
                     (((ENV \[ "rawset" \])) = ((function 
                                         $rawset 
                                         (\( (table index value) \) 
                                             ((:: $ret ::) 
                                              \{ (local (typeTable) = (($builtIn type \( (table) \)))
                                                   in
                                                   (if (not (typeTable == "table"))
                                                   then 
                                                      ($builtIn error \( ((("bad argument #1 to 'rawset' (table expected, got "
                                                                       .. typeTable) .. ")")) \))
                                                   else
                                                      (if (index == nil) then
                                                          ($builtIn error \( ("table index is nil") \))
                                                       else 
                                                       (break $ret 
                                                         (< (($builtIn rawSet \( (table index value) \))) >))
                                                       end)
                                                   end)
                                                  end)
                                              \}) 
                                             end))))
                     \;
                     (
                      (((ENV \[ "next" \])) = ((function 
                                         $next 
                                         (\( (table index) \) 
                                             ((:: $ret ::) 
                                              \{ (local (typeTable) = (($builtIn type \( (table) \)))
                                                   in
                                                   (if (not (typeTable == "table"))
                                                   then 
                                                   ($builtIn error \( ((("bad argument #1 to 'next' (table expected, got "
                                                                       .. typeTable) .. ")")) \))
                                                   else
                                                   (break $ret 
                                                         (< (($builtIn 
                                                              next
                                                              \( (table 
                                                                  index) 
                                                              \))) >))
                                                   end)
                                                   end)
                                              \}) 
                                             end))))
                      \;
                      (
                       (((ENV \[ "rawlen" \])) = ((function 
                                         $rawlen 
                                         (\( (v) \) 
                                             ((:: $ret ::) 
                                              \{ (local (typeV) = (($builtIn type \( (v) \)))
                                                   in
                                                   (if ((not (typeV == "table")) and (not (typeV == "string")))
                                                   then 
                                                   ($builtIn error \( ("bad argument #1 to 'rawlen' (table or string expected)") \))
                                                   else
                                                   (break $ret 
                                                         (< (($builtIn rawLen \( (v) \))) >))
                                                   end)
                                                   end)
                                              \}) 
                                             end))))
                       \;
                       (
                        (((ENV \[ "select" \])) = ((function 
                                           $select 
                                           (\( (index <<<) \) 
                                               ((:: $ret ::) 
                                                \{ 
                                                (break $ret 
                                                       (< (($builtIn select \( (index <<<) \))) >))
                                                \}) 
                                               end))))
                        \;
                        (
                         (((ENV \[ "setmetatable" \])) = ((function 
                                         $setmetatable 
                                         (\( (table metatable) \) 
                                             ((:: $ret ::) 
                                              \{ (local (typeTable) = (($builtIn type \( (table) \)))
                                                   in
                                                   (if (not (typeTable == "table"))
                                                   then
                                                   ($builtIn error \( ((("bad argument #1 to 'next' (table expected, got "
                                                                       .. typeTable) .. ")")) \))
                                                   else
                                                   (break $ret 
                                                         (< (($builtIn setMetatable \( (table metatable) \))) >))
                                                   end)
                                                   end)
                                              \}) 
                                             end))))
                         \;
                         (
                          ((((ENV \[ "_G" \])) = (ENV))
                           \;
                           (((ENV \[ "_ENV" \])) = (ENV))
                           )
                          \;
                          (
                           (((ENV \[ "tostring" \])) = ((function 
                                         $tostring 
                                         (\( (v) \) 
                                             ((:: $ret ::) 
                                              \{ (local (metatable handler string type) = (($builtIn getMetatable \( (v false) \)) nil nil nil)
                                                  in
                                                   (
                                                    (if (metatable == nil)
                                                     then 
                                                      void
                                                     else
                                                      ((handler) = (($builtIn rawGet \( (metatable "__tostring") \))))
                                                     end)
                                                    \;
                                                   (if (handler == nil)
                                                    then
                                                       ((((type) = (($builtIn type \( (v) \))))
                                                         \;
                                                         ((string) = (($builtIn toString \( (v) \)))))
                                                        \;
                                                        ((if (type == "table")
                                                         then
                                                            ((string) = (("table: " .. string)))
                                                         else
                                                            (if (type == "function")
                                                             then
                                                                ((string) = (("function: " .. string)))
                                                             else
                                                                void
                                                             end)
                                                         end)
                                                         \;
                                                         (break $ret (< (string) >))
                                                         )
                                                        )
                                                    else
                                                       (break $ret (< ((handler \( (v) \))) >))
                                                    end)
                                                    )
                                                  end)
                                              \}) 
                                             end))))
                           \;
                           (
                            (((ENV \[ "ipairs" \])) = ((function 
                                           $ipairs (\( (t) \) 
                                               ((:: $ret ::) \{ 
                                                  (local (metatable handler iter typeT) = (($builtIn getMetatable \( (t false) \)) nil nil ($builtIn type \( (t) \))) 
                                                   in
                                                    ((if (metatable == nil)
                                                     then
                                                        void
                                                     else
                                                        ((handler) = (($builtIn rawGet \( (metatable "__ipairs") \))))
                                                     end)
                                                     \;
                                                     (if (handler == nil)
                                                      then
                                                         ((if (typeT == "table")
                                                          then
                                                             void
                                                          else
                                                             ($builtIn error \( ((("bad argument #1 to 'ipairs' (table expected, got " .. typeT) .. ")")) \))
                                                          end)
                                                          \;
                                                          (((iter) = ((function $iPairsIter (\( (t var) \)
                                                                      ((:: $ret ::) \{
                                                                        ((if (($builtIn type \( (t) \)) == "table")
                                                                         then
                                                                            void
                                                                         else
                                                                            ($builtIn error \( ("expected a table value") \))
                                                                         end)
                                                                         \;
                                                                         (local (result) = (nil)
                                                                          in
                                                                             ((((var) = ((var + 1)))
                                                                              \;
                                                                             ((result) = (($builtIn rawGet \( (t var) \)))))
                                                                              \;
                                                                              (if (result == nil)
                                                                               then
                                                                                  (break $ret (< (nil) >))
                                                                               else
                                                                                  (break $ret (< (var result) >))
                                                                               end))
                                                                          end))
                                                                      \})
                                                                      end))))
                                                           \;
                                                           (break $ret (< (iter t 0) >)))
                                                          )
                                                      else
                                                         ; {not (handler == nil)}
                                                         (local (v1 v2 v3) = ((handler \( (t) \)))
                                                          in
                                                             (break $ret (< (v1 v2 v3) >))
                                                          end)
                                                      end)
                                                     )
                                                   end)
                                               \}) 
                                               end))))
                            \;
                            (
                             (((ENV \[ "pairs" \])) = ((function 
                                           $pairs (\( (t) \) 
                                               ((:: $ret ::) \{ 
                                                  (local (metatable handler iter typeT) = (($builtIn getMetatable \( (t false) \)) nil nil ($builtIn type \( (t) \))) 
                                                   in
                                                    ((if (metatable == nil)
                                                     then
                                                        void
                                                     else
                                                        ((handler) = (($builtIn rawGet \( (metatable "__pairs") \))))
                                                     end)
                                                     \;
                                                     (if (handler == nil)
                                                      then
                                                         ((if (typeT == "table")
                                                          then
                                                             void
                                                          else
                                                             ($builtIn error \( ((("bad argument #1 to 'pairs' (table expected, got " .. typeT) .. ")")) \))
                                                          end)
                                                          \;
                                                          (((iter) = ((function $pairsIter (\( (t var) \)
                                                                      ((:: $ret ::) \{
                                                                        (break $ret (< (($builtIn next \( (t var) \))) >))
                                                                      \})
                                                                      end))))
                                                           \;
                                                           (break $ret (< (iter t 0) >)))
                                                          )
                                                      else
                                                         ; {not (handler == nil)}
                                                         (local (v1 v2 v3) = ((handler \( (t) \)))
                                                          in
                                                             (break $ret (< (v1 v2 v3) >))
                                                          end)
                                                      end)
                                                     )
                                                   end)
                                               \}) 
                                               end))))
                             \;
                             (((ENV \[ "getmetatable" \])) = ((function 
                                           $getmetatable 
                                           (\( (value) \) 
                                               ((:: $ret ::) 
                                                \{ 
                                                (break $ret 
                                                       (< (($builtIn getMetatable \( (value true) \))) >))
                                                \}) 
                                               end))))
                             )
                            )
                           )
                          )
                         )
                         
                        )
                       )
                      )
                     )
                    )
                   )
                  )
                 )
                ) 
                \; ; Table manipulation 
                (
                 (
                  (((ENV \[ "table" \])) = ((\{ \}))) 
                  \; 
                  ((((ENV \[ "table" \]) \[ "pack" \])) = ((function $tablePack (\( (<<<) \) 
                                                            ((:: $ret ::) 
                                                             \{ 
                                                             (break $ret 
                                                              (< (($builtIn tablePack \( (<<<) \))) >)) 
                                                             \}) 
                                                            end))))
                  ) \; ; Here comes the program
                    hole
                )) 
            end)
       )
    block)]
  )

(provide plugIntoExecutionEnvironment)