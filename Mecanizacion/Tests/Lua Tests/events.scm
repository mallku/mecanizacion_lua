#lang racket
(require redex
         "../../grammar.scm"
         "../../executionEnvironment.scm"
         "../../Reductions/standardReductionRelation.scm")
(define library
  (term (plugIntoExecutionEnvironment
      ((((((((((((((((((((((ENV \["X"\])) = (20.0))\;
(((ENV \["B"\])) = (30.0)))\;
((ENV) = (((ENV \["setmetatable"\]) \( ((\{  \}) (\{ (\[ "__index" \] = (ENV \["_G"\])) \})) \)))))\;
(((ENV \["X"\])) = (((ENV \["X"\]) + 10.0))))\;
((ENV \["assert"\]) \( ((((ENV \["X"\]) == 30.0) and (((ENV \["_G"\]) \[ "X" \]) == 20.0))) \)))\;
(((ENV \["B"\])) = (false)))\;
((ENV \["assert"\]) \( (((ENV \["B"\]) == false)) \)))\;
(((ENV \["B"\])) = (nil)))\;
((ENV \["assert"\]) \( (((ENV \["B"\]) == 30.0)) \)))\;
((ENV \["assert"\]) \( ((((ENV \["getmetatable"\]) \( ((\{  \})) \)) == nil)) \)))\;
((ENV \["assert"\]) \( ((((ENV \["getmetatable"\]) \( (4.0) \)) == nil)) \)))\;
((ENV \["assert"\]) \( ((((ENV \["getmetatable"\]) \( (nil) \)) == nil)) \)))\;
(((ENV \["a"\])) = ((\{  \}))))\;
((ENV \["setmetatable"\]) \( ((ENV \["a"\]) (\{ (\[ "__metatable" \] = "xuxu") (\[ "__tostring" \] = (function func391(\( (x) \) ((:: $ret ::) \{   (break $ret (< ((x \[ "name" \])) >))  \})end))) \})) \)))\;
((ENV \["assert"\]) \( ((((ENV \["getmetatable"\]) \( ((ENV \["a"\])) \)) == "xuxu")) \)))\;
((ENV \["assert"\]) \( ((((ENV \["tostring"\]) \( ((ENV \["a"\])) \)) == nil)) \)))\;
((ENV \["assert"\]) \( ((((ENV \["pcall"\]) \( ((ENV \["setmetatable"\]) (ENV \["a"\]) (\{  \})) \)) == false)) \)))\;
((((ENV \["a"\]) \[ "name" \])) = ("gororoba")))\;
((ENV \["assert"\]) \( ((((ENV \["tostring"\]) \( ((ENV \["a"\])) \)) == "gororoba")) \)))\;
(local (a t) = ((\{ 10.0 20.0 30.0 (\[ "x" \] = "10") (\[ "y" \] = "20") \}) (\{  \})) in 
(((((((((((((((((      ((ENV \["assert"\]) \( ((((ENV \["setmetatable"\]) \( (a t) \)) == a)) \))\;
      ((ENV \["assert"\]) \( ((((ENV \["getmetatable"\]) \( (a) \)) == t)) \)))\;
      ((ENV \["assert"\]) \( ((((ENV \["setmetatable"\]) \( (a nil) \)) == a)) \)))\;
      ((ENV \["assert"\]) \( ((((ENV \["getmetatable"\]) \( (a) \)) == nil)) \)))\;
      ((ENV \["assert"\]) \( ((((ENV \["setmetatable"\]) \( (a t) \)) == a)) \)))\;
      (((ENV \["f"\])) = ((function func833 (\( (t i e) \)
                                 ((:: $ret ::) \{
(                                  ((ENV \["assert"\]) \( ((not e)) \))\;
                                  (local (p) = (((ENV \["rawget"\]) \( (t "parent") \))) in 
                                        (break $ret (< ((\( (p and ((p \[ i \]) + 3.0)) \)) "dummy return") >))
 end))
                                 \}) 
                                end)))))\;
      (((t \[ "__index" \])) = ((ENV \["f"\]))))\;
      (((a \[ "parent" \])) = ((\{ (\[ "z" \] = 25.0) (\[ "x" \] = 12.0) (\[ 4.0 \] = 24.0) \}))))\;
      ((ENV \["assert"\]) \( ((((((a \[ 1.0 \]) == 10.0) and ((a \[ "z" \]) == 28.0)) and ((a \[ 4.0 \]) == 27.0)) and ((a \[ "x" \]) == "10"))) \)))\;
      ((a) = (((ENV \["setmetatable"\]) \( ((\{  \}) t) \)))))\;
      (((ENV \["f"\])) = ((function func1106 (\( (t i v) \)
                                 ((:: $ret ::) \{
                                  ((ENV \["rawset"\]) \( (t i (v - 3.0)) \))
                                 \}) 
                                end)))))\;
      ((ENV \["setmetatable"\]) \( (t t) \)))\;
      (((t \[ "__newindex" \])) = ((ENV \["f"\]))))\;
      (((a \[ 1.0 \])) = (30.0)))\;
      (((a \[ "x" \])) = ("101")))\;
      (((a \[ 5.0 \])) = (200.0)))\;
      ((ENV \["assert"\]) \( (((((a \[ 1.0 \]) == 27.0) and ((a \[ "x" \]) == 98.0)) and ((a \[ 5.0 \]) == 197.0))) \)))\;
      (local (c) = ((\{  \})) in 
(((((((((((            ((a) = (((ENV \["setmetatable"\]) \( ((\{  \}) t) \))))\;
            (((t \[ "__newindex" \])) = (c)))\;
            (((a \[ 1.0 \])) = (10.0)))\;
            (((a \[ 2.0 \])) = (20.0)))\;
            (((a \[ 3.0 \])) = (90.0)))\;
            ((ENV \["assert"\]) \( (((((c \[ 1.0 \]) == 10.0) and ((c \[ 2.0 \]) == 20.0)) and ((c \[ 3.0 \]) == 90.0))) \)))\;
            (do 
             (local (a) = () in 
((                   ((a) = (((ENV \["setmetatable"\]) \( ((\{  \}) (\{ (\[ "__index" \] = ((ENV \["setmetatable"\]) \( ((\{  \}) (\{ (\[ "__index" \] = ((ENV \["setmetatable"\]) \( ((\{  \}) (\{ (\[ "__index" \] = (function func1582(\( (_ n) \) ((:: $ret ::) \{   (break $ret (< (((a \[ (n - 3.0) \]) + 4.0) "lixo") >))  \})end))) \})) \))) \})) \))) \})) \))))\;
                   (((a \[ 0.0 \])) = (20.0)))\;
                   (do 
                    (local ($1 $2 $3) = (($builtIn toNumber \( (0.0 10) \)) 
                                         ($builtIn toNumber \( (10.0 10) \)) 
                                         ($builtIn toNumber \( (1.0 10) \)))
                     in
                        ((if (not (($1 and $2) and $3)) then
                           ($builtIn error \( ("'for' limit must be a number") \))
                          else
                           void
                          end) \;
                        ((:: $while ::) \{ 
                         (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
                           (local (i) = ($1) in
                            (                   ((ENV \["assert"\]) \( (((a \[ (i * 3.0) \]) == (20.0 + (i * 4.0)))) \)) \; 
                            (($1) = (($1 + $3))))
                           end)
                         end)
                        \}))
                    end)
                   end))
 end)
            end))\;
            (do 
             (local (foi) = () in 
                   (local (a) = ((\{  \})) in 
(((((((((((((((((((                         (do 
                          (local ($1 $2 $3) = (($builtIn toNumber \( (1.0 10) \)) 
                                               ($builtIn toNumber \( (10.0 10) \)) 
                                               ($builtIn toNumber \( (1.0 10) \)))
                           in
                              ((if (not (($1 and $2) and $3)) then
                                 ($builtIn error \( ("'for' limit must be a number") \))
                                else
                                 void
                                end) \;
                              ((:: $while ::) \{ 
                               (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
                                 (local (i) = ($1) in
                                  ((                         (((a \[ i \])) = (0.0))\;
                         (((a \[ ("a" .. i) \])) = (0.0))) \; 
                                  (($1) = (($1 + $3))))
                                 end)
                               end)
                              \}))
                          end)
                         end)\;
                         ((ENV \["setmetatable"\]) \( (a (\{ (\[ "__newindex" \] = (function func1824(\( (t k v) \) ((:: $ret ::) \{   (((foi) = (true))\;
((ENV \["rawset"\]) \( (t k v) \)))  \})end))) \})) \)))\;
                         ((foi) = (false)))\;
                         (((a \[ 1.0 \])) = (0.0)))\;
                         ((ENV \["assert"\]) \( ((not foi)) \)))\;
                         ((foi) = (false)))\;
                         (((a \[ "a1" \])) = (0.0)))\;
                         ((ENV \["assert"\]) \( ((not foi)) \)))\;
                         ((foi) = (false)))\;
                         (((a \[ "a11" \])) = (0.0)))\;
                         ((ENV \["assert"\]) \( (foi) \)))\;
                         ((foi) = (false)))\;
                         (((a \[ 11.0 \])) = (0.0)))\;
                         ((ENV \["assert"\]) \( (foi) \)))\;
                         ((foi) = (false)))\;
                         (((a \[ 1.0 \])) = (nil)))\;
                         ((ENV \["assert"\]) \( ((not foi)) \)))\;
                         ((foi) = (false)))\;
                         (((a \[ 1.0 \])) = (nil)))\;
                         ((ENV \["assert"\]) \( (foi) \)))
 end)
 end)
            end))\;
            ((ENV \["setmetatable"\]) \( (t nil) \)))\;
            (((ENV \["f"\])) = ((function func2132 (\( (t <<<) \)
                                             ((:: $ret ::) \{
                                              (break $ret (< (t (\{ <<< \})) >))
                                             \}) 
                                            end)))))\;
            (((t \[ "__call" \])) = ((ENV \["f"\]))))\;
            (local (b) = (((ENV \["setmetatable"\]) \( ((\{  \}) t) \))) in 
(((((((((((((((((((((((((((((((((((((((((((((((((((                  ((ENV \["setmetatable"\]) \( (b t) \))\;
                  (((ENV \["f"\])) = ((function func2558 (\( (op) \)
                                                         ((:: $ret ::) \{
                                                          (break $ret (< ((function func2582(\( ( <<<) \) ((:: $ret ::) \{   ((((ENV \["cap"\])) = ((\{ (\[ 0.0 \] = op) <<< \})))\;
(break $ret (< ((\( <<< \))) >)))  \})end))) >))
                                                         \}) 
                                                        end)))))\;
                  (((t \[ "__add" \])) = (((ENV \["f"\]) \( ("add") \)))))\;
                  (((t \[ "__sub" \])) = (((ENV \["f"\]) \( ("sub") \)))))\;
                  (((t \[ "__mul" \])) = (((ENV \["f"\]) \( ("mul") \)))))\;
                  (((t \[ "__div" \])) = (((ENV \["f"\]) \( ("div") \)))))\;
                  (((t \[ "__mod" \])) = (((ENV \["f"\]) \( ("mod") \)))))\;
                  (((t \[ "__unm" \])) = (((ENV \["f"\]) \( ("unm") \)))))\;
                  (((t \[ "__pow" \])) = (((ENV \["f"\]) \( ("pow") \)))))\;
                  (((t \[ "__len" \])) = (((ENV \["f"\]) \( ("len") \)))))\;
                  ((ENV \["assert"\]) \( (((b + 5.0) == b)) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "add") and (((ENV \["cap"\]) \[ 1.0 \]) == b)) and (((ENV \["cap"\]) \[ 2.0 \]) == 5.0)) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((ENV \["assert"\]) \( (((b + "5") == b)) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "add") and (((ENV \["cap"\]) \[ 1.0 \]) == b)) and (((ENV \["cap"\]) \[ 2.0 \]) == "5")) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((ENV \["assert"\]) \( (((5.0 + b) == 5.0)) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "add") and (((ENV \["cap"\]) \[ 1.0 \]) == 5.0)) and (((ENV \["cap"\]) \[ 2.0 \]) == b)) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((ENV \["assert"\]) \( ((("5" + b) == "5")) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "add") and (((ENV \["cap"\]) \[ 1.0 \]) == "5")) and (((ENV \["cap"\]) \[ 2.0 \]) == b)) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((b) = ((b - 3.0))))\;
                  ((ENV \["assert"\]) \( ((((ENV \["getmetatable"\]) \( (b) \)) == t)) \)))\;
                  ((ENV \["assert"\]) \( (((5.0 - a) == 5.0)) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "sub") and (((ENV \["cap"\]) \[ 1.0 \]) == 5.0)) and (((ENV \["cap"\]) \[ 2.0 \]) == a)) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((ENV \["assert"\]) \( ((("5" - a) == "5")) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "sub") and (((ENV \["cap"\]) \[ 1.0 \]) == "5")) and (((ENV \["cap"\]) \[ 2.0 \]) == a)) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((ENV \["assert"\]) \( (((a * a) == a)) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "mul") and (((ENV \["cap"\]) \[ 1.0 \]) == a)) and (((ENV \["cap"\]) \[ 2.0 \]) == a)) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((ENV \["assert"\]) \( (((a / 0.0) == a)) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "div") and (((ENV \["cap"\]) \[ 1.0 \]) == a)) and (((ENV \["cap"\]) \[ 2.0 \]) == 0.0)) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((ENV \["assert"\]) \( (((a % 2.0) == a)) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "mod") and (((ENV \["cap"\]) \[ 1.0 \]) == a)) and (((ENV \["cap"\]) \[ 2.0 \]) == 2.0)) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((ENV \["assert"\]) \( (((- a) == a)) \)))\;
                  ((ENV \["assert"\]) \( (((((ENV \["cap"\]) \[ 0.0 \]) == "unm") and (((ENV \["cap"\]) \[ 1.0 \]) == a))) \)))\;
                  ((ENV \["assert"\]) \( (((a ^ 4.0) == a)) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "pow") and (((ENV \["cap"\]) \[ 1.0 \]) == a)) and (((ENV \["cap"\]) \[ 2.0 \]) == 4.0)) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((ENV \["assert"\]) \( (((a ^ "4") == a)) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "pow") and (((ENV \["cap"\]) \[ 1.0 \]) == a)) and (((ENV \["cap"\]) \[ 2.0 \]) == "4")) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((ENV \["assert"\]) \( (((4.0 ^ a) == 4.0)) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "pow") and (((ENV \["cap"\]) \[ 1.0 \]) == 4.0)) and (((ENV \["cap"\]) \[ 2.0 \]) == a)) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((ENV \["assert"\]) \( ((("4" ^ a) == "4")) \)))\;
                  ((ENV \["assert"\]) \( (((((((ENV \["cap"\]) \[ 0.0 \]) == "pow") and (((ENV \["cap"\]) \[ 1.0 \]) == "4")) and (((ENV \["cap"\]) \[ 2.0 \]) == a)) and (((ENV \["cap"\]) \[ 3.0 \]) == nil))) \)))\;
                  ((ENV \["assert"\]) \( (((\# a) == a)) \)))\;
                  ((ENV \["assert"\]) \( (((((ENV \["cap"\]) \[ 0.0 \]) == "len") and (((ENV \["cap"\]) \[ 1.0 \]) == a))) \)))\;
                  ((t) = (((ENV \["setmetatable"\]) \( ((\{ 1.0 2.0 3.0 \}) (\{ (\[ "__len" \] = (function func4184(\( () \) ((:: $ret ::) \{   (break $ret (< (10.0) >))  \})end))) \})) \)))))\;
                  ((ENV \["assert"\]) \( ((((\# t) == 10.0) and (((ENV \["rawlen"\]) \( (t) \)) == 3.0))) \)))\;
                  ((ENV \["assert"\]) \( ((((ENV \["rawlen"\]) \( ("abc") \)) == 3.0)) \)))\;
                  ((ENV \["assert"\]) \( ((not ((ENV \["pcall"\]) \( ((ENV \["rawlen"\]) ((ENV \["io"\]) \[ "stdin" \])) \)))) \)))\;
                  ((ENV \["assert"\]) \( ((not ((ENV \["pcall"\]) \( ((ENV \["rawlen"\]) 34.0) \)))) \)))\;
                  ((ENV \["assert"\]) \( ((not ((ENV \["pcall"\]) \( ((ENV \["rawlen"\])) \)))) \)))\;
                  ((t) = ((\{  \}))))\;
                  (((t \[ "__lt" \])) = ((function func4395(\( (a b c) \)                                                           ((:: $ret ::) \{                                                             (((                                                          ((ENV \["assert"\]) \( ((c == nil)) \))\;
                                                          (if (((ENV \["type"\]) \( (a) \)) == "table") then 
                                                           ((a) = ((a \[ "x" \])))
                                                          else 
                                                           void
                                                          end))\;
                                                          (if (((ENV \["type"\]) \( (b) \)) == "table") then 
                                                           ((b) = ((b \[ "x" \])))
                                                          else 
                                                           void
                                                          end))\;
                                                          (break $ret (< ((a < b) "dummy") >)))                                                            \})                                                          end)))))\;
                  (((ENV \["Op"\])) = ((function func4560 (\( (x) \)
                                                          ((:: $ret ::) \{
                                                           (break $ret (< (((ENV \["setmetatable"\]) \( ((\{ (\[ "x" \] = x) \}) t) \))) >))
                                                          \}) 
                                                         end)))))\;
                  (local (test) = (nil) in 
(                   ((test) = ((function func4616 (\(()\)                                ((:: $ret ::) \{
(                                 ((ENV \["assert"\]) \( ((((not (\( (((ENV \["Op"\]) \( (1.0) \)) < ((ENV \["Op"\]) \( (1.0) \))) \))) and (\( (((ENV \["Op"\]) \( (1.0) \)) < ((ENV \["Op"\]) \( (2.0) \))) \))) and (not (\( (((ENV \["Op"\]) \( (2.0) \)) < ((ENV \["Op"\]) \( (1.0) \))) \))))) \))\;
                                 ((ENV \["assert"\]) \( ((((not (\( (1.0 < ((ENV \["Op"\]) \( (1.0) \))) \))) and (\( (((ENV \["Op"\]) \( (1.0) \)) < 2.0) \))) and (not (\( (2.0 < ((ENV \["Op"\]) \( (1.0) \))) \))))) \)))
                                \})
                               end))))\;
                        (test \( () \)))
 end))
 end))
 end))
 end))





 
)))

(define (void? red)
  (and (redex-match core-lang
              (σ \; θ \; void)
              (first red))
       (eq? (length red) 1)))

(define (lua-events-test-suite)
  (test-predicate void? (apply-reduction-relation* core-lan-stand-reduc-red-rel library))
  (test-results))

(provide lua-events-test-suite)
