#lang scheme
(require redex
         "../grammar.scm")

; Determine if a reference belongs to the domain of a store
; PRE : {the store received satisfy the invariant of representation}
(define-metafunction core-lang
  refBelongsTo : simpvalref sigma -> any
  ; Base case
  [(refBelongsTo simpvalref_1 ())
   #f]
  ; Inductive cases
  [(refBelongsTo simpvalref ((\( simpvalref \, simplevalue \)) svste ...))
   #t]
  [(refBelongsTo simpvalref_1 ((\( simpvalref_2 \, simplevalue \)) svste ...))
   (refBelongsTo simpvalref_1 (svste ...))])

; Extension of refBelongsTo, to manage many references
; PRE : {the store received satisfy the invariant of representation}
(define-metafunction core-lang
  ; Base case
  [(refsBelongsTo (r) σ)
   (refBelongsTo r σ)]
  ; Inductive case
  [(refsBelongsTo (r_1 r_2 ...) σ)
   ,(and (term (refBelongsTo r_1 σ)) (term (refsBelongsTo (r_2 ...) σ)))])

(provide refBelongsTo)

; Access to a store (by dereferencing a simpValRef)
; PRE : {the store received satisfy the invariant of representation
;        and the reference dereferenced belongs to the domain of the
;        store}
(define-metafunction core-lang
  derefSigma : σ simpvalref -> simplevalue
  [(derefSigma (svste_1 ... (\( r_1 \, simplevalue_1 \)) svste_2 ...) r_1)
   simplevalue_1])

(provide derefSigma)

; Modify a reference-value mapping on a given store
; PRE : {the store received satisfy the invariant of representation
;        and the reference belongs to the domain of the
;        store}
(define-metafunction core-lang
  sigmaAlter : σ simpvalref simplevalue -> σ
  
  [(sigmaAlter (svste ... (\( r_1 \, sv \)) svste_1 ...) r_1 sv_1)
   (svste ... (\( r_1 \, sv_1 \)) svste_1 ...)])

(provide sigmaAlter)

; Used to discard values to be assigned when those are greater in number than
; the variables being assigned
; PRE : {the list of values assigned is greater in number than the variables 
;        assigned}
; ret = (discard names svlist)
; POS : {ret has the first #names elements of svlist}
(define-metafunction core-lang
  discard : any svlist -> svlist
  ; Base case
  [(discard () svlist)
   ()]
  ; Inductive case
  [(discard (any_1 any ...) (sv_1 sv ...))
   ,(append (term (sv_1)) (term (discard (any ...) (sv ...))))])

(provide discard)

; Used to complete values to be assigned when those are fewer in number than
; the variables being assigned
; PRE : {namelist is the list of variable's identifiers beign assigned and svlist
;        is the list of values assigned and #(svlist) < #(namelist)}
; ret = (complete namelist svlist)
; POS : {#ret == #namelist and ret[i]==svlist[i] 
;       for 1<=i<=svlist and ret[i]==nil for #svlist+1<=i<=#ret}
(define-metafunction core-lang
  complete : any svlist -> svlist
  ; Base case
  [(complete () ())
   ()]
  ; Inductive case
  [(complete (any_1 any_2 ...) ())
   ,(append (term (nil)) (term (complete (any_2 ...) ())))]
  [(complete (any_1 any_2 ...) (sv_1 sv_2 ...))
   ,(append (term (sv_1)) (term (complete (any_2 ...) (sv_2 ...))))])

(provide complete)

; Meta-function that generates a fresh simpvalref. Its definition depends
; heavily on the fact that references are implicit generated only by this
; function and that we don't have any kind of garbage collection.
(define-metafunction core-lang
  freshSvRef : σ -> simpvalref
  ; Empty Store
  [(freshSvRef ())
   (svr 1)]
  ; An store with at least one reference
  [(freshSvRef (svste_1 ... (\( (svr Number_1) \, simplevalue \))))
   (svr Number_2)
   (where Number_2 ,(+ (term Number_1) 1))])

; Auxiliar meta-function used by freshSvRefs that generates a list of fresh 
; simpvalrefs. This is used to obtain which fresh references were added to the store in a reduction
; of a local statement, to apply the proper substitution of names by
; references.
; PRE : {n >= 1 denotes the number of simpvalref to start with and m >= 0 
;        is the number of simpvalrefs to generate}
; ret = (freshSvRefs n m)
; POS : {ret is a list of simpvalrefs of the form ((svr n) (svr n+1) ... (svr n+m-1))}
(define-metafunction core-lang
  freshSvRefsAux : Number Number -> any
  ; Base case
  [(freshSvRefsAux Number 0)
  ()]
  ; Inductive case
  [(freshSvRefsAux Number_1 Number_2)
  ,(append (term ((svr Number_1))) (term (freshSvRefsAux Number_3 Number_4)))
  (where Number_3 ,(+ (term Number_1) 1))
  (where Number_4 ,(- (term Number_2) 1))])

; Generates a list of fresh simpvalrefs, given a store. The number of the first
; reference generated is 1 + the max. number of a simpvalref present in the store.
; PRE : {σ is a proper store and m >= 0 is the number of simpvalrefs to generate}
; ret = (freshSvRefs σ m)
; POS : {ret is a list of simpvalrefs of the form ((svr n) (svr n+1) ... (svr n+m-1))
;        where n-1 is the max. number of a simpvalref present in the store σ}
(define-metafunction core-lang
  freshSvRefs : σ Number -> any
  ; Empty store
  [(freshSvRefs () Number)
   (freshSvRefsAux 1 Number)]
  ; An store with at least one mapping
  [(freshSvRefs (svste ... (\( (svr Number_1) \, sv \))) Number_2)
   (freshSvRefsAux Number_3 Number_2)
   (where Number_3 ,(+ (term Number_1) 1))]
  )

(provide freshSvRefs)

; To add simplevalues to the store
(define-metafunction core-lang
  addSimpVal : σ svlist -> σ
  ; Base case
  ; Function call with no arguments
  [(addSimpVal (svste ...) ())
   (svste ...)]
  ; Function call with one argument
  [(addSimpVal (svste ...) (sv))
   (svste ... (\( (freshSvRef (svste ...)) \, sv \)))]
  
  ; Inductive case
  [(addSimpVal (svste ...) (sv_1 sv_2 ...))
   (addSimpVal (svste ... (\( (freshSvRef (svste ...)) \, sv_1 \))) (sv_2 ...))])

(provide addSimpVal)