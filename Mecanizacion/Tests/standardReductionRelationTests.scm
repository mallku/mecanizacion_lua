#lang racket
(require redex
         "../grammar.scm"
         "../Reductions/standardReductionRelation.scm")

(define (stand-red-rel-test-suite)
  ; Full "while" loop
  (test-->> core-lan-stand-reduc-red-rel
            (term (((\( (svr 1) \, 2 \))) 
                   \; () \; (while (1 < (svr 1)) do (((svr 1)) = (((svr 1) - 1))) end)))
            
            (term (((\( (svr 1) \, 1 \))) 
                   \; () \; void)))
  
  (test-->> core-lan-stand-reduc-red-rel
            (term (((\( (svr 1) \, 0 \))) 
                   \; () \; (while ((svr 1) < 2) do (((svr 1)) = (((svr 1) + 1))) end)))
            
            (term (((\( (svr 1) \, 2 \))) 
                   \; () \; void)))
  
  ; Full while loop + short-circuit evaluation operator in the guard
  (test-->> core-lan-stand-reduc-red-rel
            (term (((\( (svr 1) \, 0 \))
                    (\( (svr 2) \, true \))) 
                   \; () \; (while (((svr 1) < 1) and (svr 2)) 
                                   do (((svr 1)) = (((svr 1) + 1))) end)))
            
            (term (((\( (svr 1) \, 1 \))
                    (\( (svr 2) \, true \))) 
                   \; () \; void)))
  
  ; While loop + break
  (test-->> core-lan-stand-reduc-red-rel
            (term (((\( (svr 1) \, 0 \))) 
                   \; () \; ((:: X ::) 
                             \{ (while ((svr 1) < 2) 
                                   do ((((svr 1)) = (((svr 1) + 1))) 
                                       \; (break X (< (1) >))) end) \})))
            
            (term (((\( (svr 1) \, 1 \))) 
                   \; () \; void)))
  ; Function definition + function call
  (test-->> core-lan-stand-reduc-red-rel
            (term (((\( (svr 1) \, nil \))) 
                   \; () 
                   \; ( (((svr 1)) = ((function X (\( () \) ((:: X ::) \{ (break X (< (1) >)) \}) end))))
                        \; (if ((svr 1) \( () \)) then 
                               (((svr 1)) = (1)) 
                               else (((svr 1)) = (2)) 
                               end))))
            ; TODO: first location is hard-coded...
            (term (((\( (svr 1) \, 1 \))) 
                   \; ((\( (objr 6) \, (function X (\( () \) ((:: X ::) \{ (break X (< (1) >)) \}) end))\))) 
                   \; void)))
  
  ; Vararg function definition + function call
  (test-->> core-lan-stand-reduc-red-rel
            (term (((\( (svr 1) \, nil \))) 
                   \; () 
                   \; ( (((svr 1)) = ((function X (\( (<<<) \) ((:: X ::) \{ (break X (< (<<<) >)) \}) end))))
                        \; (if ((svr 1) \( (1 2 3) \)) then 
                               (((svr 1)) = (1)) 
                               else (((svr 1)) = (2)) 
                               end))))
            ; TODO: first location is hard-coded...
            (term (((\( (svr 1) \, 1 \))) 
                   \; ((\( (objr 6) \, (function X (\( (<<<) \) ((:: X ::) \{ (break X (< (<<<) >)) \}) end))\))) 
                   \; void)))

  (test-results))

(provide stand-red-rel-test-suite)