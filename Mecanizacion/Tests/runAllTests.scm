#lang racket
(require redex
         ; Meta-functions test suites
         "./deltaTests.scm"
         "./objStoreMetafunctionsTests.scm"
         "./substitutionTests.scm"
         "./metaTableMechanismMetafunctionsTests.scm"
         ; Reductions test suites
         "./simpleExpressionsReductionsTests.scm"
         "./expressionsSimpValStoreReductionsTests.scm"
         "./expressionsObjStoreReductionsTests.scm"
         "./expressionsSimpValObjStoreReductionsTests.scm"
         "./abnormalExpressionsReductionsTests.scm"
         "./simpleStatementsReductionsTests.scm"
         "./breakStatementReductionsTests.scm"
         "./statementsSimpValStoreReductionsTests.scm"
         "./statementsObjStoreReductionsTests.scm"
         "./abnormalStatementsReductionsTests.scm"
         "./standardReductionRelationTests.scm")

(define (test-all-metafunctions)
  (print "delta-test-suite :")
  (delta-test-suite)
  (print "obj-store-metafunctions-test-suite :")
  (test-all-obj-store-metafunctions-suites)
  (print "substitution-test-suite :")
  (subs-test-suite)
  (print "test-all-meta-table-mech-metafunctions-test-suites: ")
  (test-all-meta-table-mech-metafunctions-test-suites)
  )

(define (test-all-reductions)
  (print "Simple expressions reductions tests suite: ")
  (expressions-red-test-suite)
  (print "Expressions that interact with the simple value store tests suite:")
  (exp-sv-store-red-test-suite)
  (print "Expressions that interact with the object store tests suite:")
  (exp-obj-store-red-test-suite)
  (print "Expressions that interact with the both stores tests suite:")
  (exp-sv-obj-store-red-test-suite)
  (print "Abnormal expressions tests suite:")
  (abnormal-expressions-red-test-suite)
  (print "Simple statements reductions tests suite: ")
  (simple-stat-red-test-suite)
  (print "Break statement tests suite: ")
  (break-stat-red-test-suite)
  (print "Statements that interact with the simple value store tests suite:")
  (stat-sv-store-red-test-suite)
  (print "Statements that interact with the object store tests suite:")
  (stat-obj-store-red-test-suite)
  (print "Abnormal expressions tests suite:")
  (abnormal-statements-red-test-suite)
  (print "Standard reduction relation tests suite: ")
  (stand-red-rel-test-suite))

(define (test-all)
  (test-all-metafunctions)
  (test-all-reductions))

(test-all)