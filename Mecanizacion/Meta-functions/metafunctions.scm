#lang scheme
(require redex
         "./grammar.scm")


;                  
;                  
;                  
;                  
;   ;;;;;; ;      ;
;   ;       ;    ; 
;   ;       ;    ; 
;   ;       ;    ; 
;   ;;;;;;   ;  ;  
;   ;        ;  ;  
;   ;         ;;   
;   ;         ;;   
;   ;         ;;   
;                  
;                  
;                  
;                  

; To obtain the free variables present in expressions
; PRE : {x is an expression}
; res = (FVexp x)
; POS : {res is a list containing all the members of x that represents the
;        free variables in x}
(define-metafunction core-lang
  FVexp : exp -> (Name ...)
  
  [(FVexp Name) ,(term (Name))]
  [(FVexp >>>) ,(term (>>>))]
  [(FVexp nil) ,null]
  [(FVexp Boolean) ,null]
  [(FVexp Number) ,null]
  [(FVexp String) ,null]
  [(FVexp void) ,null]
  [(FVexp objref) ,null]
  [(FVexp simpvalref) ,null]
  ; Tuple
  [(FVexp (\\ explist \\))
   (explistFV explist)]
  ; Binary operators
  [(FVexp (exp_1 binop exp_2)) 
   ,(append (term (FVexp exp_1)) (term (FVexp exp_2)))]
  ; Unary operators
  [(FVexp (unop exp)) (FVexp exp)]
  ; Operator ( )
  [(FVexp (\( exp \))) (FVexp exp)]
  ; Tableconstructors
  [(FVexp (\{ \})) ,null]
  [(FVexp (\{ field ... \})) 
   (FVfields (field ...))]
  [(FVexp (\{ field ... fieldsep \})) 
   (FVfields (field ...))]
  ; Table indexing
  [(FVexp (prefixexp \[ exp \])) 
   ,(append (term (FVexp prefixexp)) (term (FVexp exp)))]
  ; Function definition
  [(FVexp (function Name (\(\) block end))) (FVblock block)]
  [(FVexp (function Name (\( namelist \) block end))) 
   ,(filter (isNotBounded? (term namelist)) (term (FVblock block)))]
  [(FVexp (function Name (\( (namelist <<<) \) block end))) 
   ,(filter (isNotBounded? (append (term namelist) (term (<<<)))) 
            (term (FVblock block)))]
  ; Function call
  [(FVexp (prefixexp \(\))) (FVexp prefixexp)]
  [(FVexp (prefixexp \( explist \))) 
   ,(append (term (FVexp prefixexp)) (term (explistFV explist)))])

; Free variables of a table field
(define-metafunction core-lang
  FVfield : field -> (Name ...)
  [(FVfield exp)
   (FVexp exp)]
  [(FVfield (\[ exp_1 \] = exp_2))
   ,(append (term (FVexp exp_1)) (term (FVexp exp_2)))])

; Free variables of table fields
(define-metafunction core-lang
  FVfields : (field ...) -> (Name ...)
  
  [(FVfields ())
   ()]
  
  [(FVfields (field_1 field_2 ...))
   ,(append (term (FVfield field_1)) (term (FVfields (field_2 ...))))])

(define-metafunction core-lang
  explistFV : explist -> (Name ...)
  [(explistFV (exp))
   (FVexp exp)]
  [(explistFV (exp_1 exp_2 ...))
   ,(append (term (FVexp exp_1)) (term (explistFV (exp_2 ...))))]
  )

; To obtain the free variables present in any construction
; PRE : {x is a construction from the core-lang grammar}
; res = (FVblock x)
; POS : {res is a list containing all the members of x that represents the
;        free variables in x}
(define-metafunction core-lang
  FVblock : block -> (Name ...)
  
  ; Labelled block
  [(FVblock (label \{ block \})) (FVblock block)]
  ; Block Do-End
  [(FVblock (do block end)) (FVblock block)]
  ; Break
  [(FVblock (break Name tuple)) (FVexp tuple)]
  ; Conditional
  [(FVblock (if exp then block_1 else block_2 end)) 
   ,(append (term (FVexp exp)) (append (term (FVblock block_1)) (term (FVblock block_2))))]
  ; While loop
  [(FVblock (while exp do block end))
   ,(append (term (FVexp exp)) (term (FVblock block)))]
  ; Local statement
  [(FVblock (local namelist = explist in block))
   ,(filter (isNotBounded? (term namelist))
            (append (term (explistFV explist)) (term (FVblock block))))]
  ; Function call
  [(FVblock functioncall)
   (FVexp functioncall)]
  ; Assignation
  [(FVblock (varlist := explist))
   ,(append (term (explistFV varlist)) (term (explistFV explist)))]
  ; Statements concatenation
  [(FVblock (statement \; block))
   ,(append (term (FVblock statement)) (term (FVblock block)))])
             
; Auxiliar function used to recognize variables that are not bounded.
; This function is used in conjunction with a filter function, in the 
; definition of the FV operator, to removes variables that are already 
; bounded, when dealing with constructions that have bounding occurrences 
; of variables.
; PRE : {namelist is a list of name}
; ((isNotBounded? namelist) name)
; POS : {returns true if name isn't in the list namelist
;        returns false otherwise}
(define (isNotBounded? namelist)
    (lambda (var)
      (not (member var namelist))))

; To export the FV operator
(provide FVblock
         FVexp)

;                                                                                                  
;                                                                                                  
;                                                                                                  
;                                                                                                  
;    ;;;;;  ;    ;  ;;;;;    ;;;;; ;;;;;;;  ;;;;;  ;;;;;;;  ;    ; ;;;;;;;  ;;;;;    ;;;;   ;    ; 
;   ;    ;  ;    ;  ;    ;  ;    ;    ;       ;       ;     ;    ;    ;       ;      ;  ;   ;;   ; 
;   ;       ;    ;  ;    ;  ;         ;       ;       ;     ;    ;    ;       ;     ;    ;  ; ;  ; 
;   ;;      ;    ;  ;   ;;  ;;        ;       ;       ;     ;    ;    ;       ;     ;    ;  ; ;  ; 
;     ;;;   ;    ;  ;;;;      ;;;     ;       ;       ;     ;    ;    ;       ;     ;    ;  ; ;  ; 
;       ;;  ;    ;  ;   ;;      ;;    ;       ;       ;     ;    ;    ;       ;     ;    ;  ;  ; ; 
;        ;  ;    ;  ;    ;       ;    ;       ;       ;     ;    ;    ;       ;     ;    ;  ;  ; ; 
;       ;;  ;;  ;;  ;   ;;      ;;    ;       ;       ;     ;;  ;;    ;       ;      ;  ;   ;   ;; 
;   ;;;;;    ;;;;   ;;;;;   ;;;;;     ;     ;;;;;     ;      ;;;;     ;     ;;;;;    ;;;;   ;    ; 
;                                                                                                  
;                                                                                                  
;                                                                                                  
;                                                                                                  

;                                                                                                  

; Capture-avoiding substitution function over expressions
(define-metafunction core-lang
  ;substExp : exp parameters explist -> exp
  
  ; Variable identifier or vararg expression
  [(substExp parameter parameters explist)
   (applySubst parameter parameters explist)]
  ; Function call
  [(substExp (prefixexp \(\)) parameters explist)
   ((substExp prefixexp parameters explist) \(\))]
  
  [(substExp (prefixexp \( explist_1 \)) parameters explist_2)
   ((substExp prefixexp parameters explist_2) \( 
                                       (substexplist explist_1 parameters explist_2) 
                                       \))]
  ; Operator '( ')'
  [(substExp (\( exp \)) parameters explist)
   (\( (substExp exp parameters explist) \))]
  ; Table indexing
  [(substExp (prefixexp \[ exp \]) parameters explist)
   ((substExp prefixexp parameters explist) \[ (substExp exp parameters explist) \])]
  ; Tuple
  [(substExp (\\ explist_1 \\) parameters explist_2)
   (\\ (substexplist explist_1 parameters explist_2) \\)]
  ; Function definition
  [(substExp (function Name (\(\) block end)) parameters explist)
   (function Name (\(\) (substExp block parameters explist) end))]
  
  ; We are assuming that the identifiers parlist occur in the 
  ; same order as in namelist.
  ; When the substitution defines a substitute to a vararg expression, it is
  ; discarded
  [(substExp (function Name_1 (\( parlist \) block end)) (Name ... <<<) (exp ... exp_2))
   (substExp (function Name_1 (\( parlist \) block end)) (Name ...) (exp ...))]
  
  [(substExp (function Name (\( namelist_1 \) block end)) namelist_2 explist)
   (function Name (\( namelist_1 \) block end))
   (side-condition (equal? (term (extractDifVar namelist_2 namelist_1))
                                (list)))]
  
  [(substExp (function Name (\( (namelist_1 <<<) \) block end)) namelist_2 explist)
   (function Name (\( (namelist_1 <<<) \) block end))
   (side-condition (equal? (term (extractDifVar namelist_2 namelist_1))
                                (list)))]
  
  [(substExp (function Name (\( namelist_1 \) block end)) namelist_2 explist_1)
   (function Name (\( namelist_3 \) 
                 (substBlock block namelist_5 explist_3)
                 end))
   (where namelist_4 ,(term (extractDifVar namelist_2 namelist_1)))
   (where explist_2 ,(term (extractExp namelist_4 namelist_2 explist_1)))
   (where namelist_3 ,(variables-not-in (term (block namelist_4 explist_2)) 
                                       (term namelist_1)))
   (where namelist_5 ,(append (term namelist_1) (term namelist_4)))
   (where explist_3 ,(append (term namelist_3) (term explist_2)))
   (side-condition (not (equal? (term (extractDifVar namelist_2 namelist_1))
                                (list))))]
  ; If this fourth case is chosen, it means that parlist_1 and
  ; namelist are different
  [(substExp (function Name (\( (namelist_1 <<<) \) block end)) namelist_2 explist_1)
   (function Name (\( (namelist_3 <<<) \) 
                 (substBlock block namelist_5 explist_3)
                 end))
   (where namelist_4 ,(term (extractDifVar namelist_2 namelist_1)))
   (where explist_2 ,(term (extractExp namelist_4 namelist_2 explist_1)))
   (where namelist_3 ,(variables-not-in (term (block namelist_4 explist_2)) 
                                       (term namelist_1)))
   (where namelist_5 ,(append (term namelist_1) (term namelist_4)))
   (where explist_3 ,(append (term namelist_3) (term explist_2)))
   (side-condition (not (equal? (term (extractDifVar namelist_2 namelist_1))
                                (list))))]
  ; Table constructor
  [(substExp (\{ \}) parameters explist)
   (\{ \})]
  
  ; substfield receives and returns a list of the form (field ...)
  ; so in this case, to reconstruct the original expression that has the form
  ; of a list of symbols, we must escape to scheme code an use the append
  ; function put the result of substfield with the others symbols in one list
  [(substExp (\{ field ... \}) parameters explist)
   ,(append (term (\{)) (append (term (substfield (field ...) parameters explist)) 
                                (term (\}))))]
  
  [(substExp (\{ field ... fieldsep \}) parameters explist)
   ,(append (term (\{)) (append (term (substfield (field ...) parameters explist)) 
                                (term (fieldsep \}))))]
  
  ; Binary operators
  [(substExp (exp_1 binop exp_2) parameters explist)
   ((substExp exp_1 parameters explist) binop (substExp exp_2 parameters explist))]
  
  ; Unary operators
  [(substExp (unop exp) parameters explist)
   (unop (substExp exp parameters explist))]
  
  ; These case holds for every expression without an structure, different than
  ; a variable or a vararg exp: nil, void, boolean, number, string, 
  ; simpvalref, objref
  [(substExp any parameters explist)
   any])

; Capture-avoiding substitution function
; subst : 
(define-metafunction core-lang
  substBlock : block parameters explist -> any
  
  ; Function call
  [(substBlock functioncall parameters explist)
   (substExp functioncall parameters explist)]
  ; Concatenation of statements
  [(substBlock (statement \; block) parameters explist)
   ((substBlock statement parameters explist) \; (substBlock block 
                                                             parameters 
                                                             explist))]
  ; Labelled block
  [(substBlock (label \{ block \}) parameters explist)
   (label \{ (substBlock block parameters explist) \})]
  ; Block Do...End
  [(substBlock (do block end) parameters explist)
   (do (substBlock block parameters explist) end)]
  ; Break statement
  [(substBlock (break Name exp) parameters explist)
   (break Name (substExp exp parameters explist))]
  ; Conditional
  [(substBlock (if exp then block_1 else block_2 end) parameters explist)
   (if (substExp exp parameters explist) then (substBlock block_1 
                                                          parameters 
                                                          explist)
       else (substBlock block_2 parameters explist) end)]
  
  ; While loop
  [(substBlock (while exp do block end) parameters explist)
   (while (substExp exp parameters explist) do 
          (substBlock block parameters explist) 
          end)]
  
  ; Local statement
  ; Identifiers to be substituted and declared are the same
  [(substBlock (local namelist = explist_1 in block) namelist explist_2)
   (local namelist = (substexplist explist_1 namelist explist_2) in block)]
  
  [(substBlock (local namelist_1 = explist_1 in block) namelist_2 explist_2)
   (local namelist = (substexplist explist_1 namelist_2 explist_2) in block)
   (side-condition (equal? (term (extractDifVar namelist_2 namelist_1))
                                (list)))]
  
  ; If this case is chosen, it means that Name doesn't appears in namelist
  [(substBlock (local namelist_1 = explist_1 in block) parameters explist_2)
   (local namelist_2 = (substexplist explist_1 parameters explist_2) in 
     (substBlock block parameters_3 explist_3))
   
   (where parameters_2 ,(term (extractDifVar parameters namelist_1)))
   (where explist_4 ,(term (extractExp parameters_2 parameters explist_2)))
   (where namelist_2 ,(variables-not-in (term (block explist_4 parameters_2)) 
                                       (term namelist_1)))
   (where parameters_3 ,(append (term namelist_1) (term parameters_2)))
   (where explist_3 ,(append (term namelist_2) (term explist_4)))
   (side-condition (not (equal? (term (extractDifVar parameters namelist_1))
                                (list))))]
  
  ; Variable assignation
  [(substBlock (varlist := explist_1) parameters explist_2)
   ((substexplist varlist parameters explist_2) := 
                                                   (substexplist explist_1 
                                                                 parameters 
                                                                 explist_2))])


;                                                                  
;                                                                  
;                                                                  
;                              ;     ;;;       ;                   
;     ;;                               ;                           
;     ;;                               ;                           
;     ;;    ;    ;  ;;   ;   ;;;       ;     ;;;     ;;;;    ; ;;; 
;    ;  ;   ;    ;   ;  ;      ;       ;       ;         ;   ;;    
;    ;  ;   ;    ;    ;;       ;       ;       ;         ;   ;     
;   ;;;;;;  ;    ;    ;;       ;       ;       ;     ;;;;;   ;     
;   ;    ;  ;    ;    ;;       ;       ;       ;    ;    ;   ;     
;   ;    ;  ;   ;;   ;  ;      ;       ;       ;    ;   ;;   ;     
;  ;      ;  ;;; ;  ;    ;   ;;;;;   ;;;;;   ;;;;;   ;;; ;   ;     
;                                                                  
;                                                                  
;                                                                  
;                                                                  

(define-metafunction core-lang
  applySubst : parameter parameters explist -> exp
  
  [(applySubst parameter () ())
   parameter]
  
  [(applySubst parameter (parameter) (exp))
   exp]
  
  [(applySubst parameter_1 (parameter_1 parameter_2 ...) (exp_1 exp_2 ...))
   exp_1]
  
  [(applySubst parameter_1 (parameter_2 parameter_3 ...) (exp_1 exp_2 ...))
   (applySubst parameter_1 (parameter_3 ...) (exp_2 ...))]
  )

; Auxiliar meta-function to perform capture-avoiding substition on
; just 1 tableassignation construct
(define-metafunction core-lang
  substTableAssignation : tableassignation parameters explist -> tableassignation
  
  [(substTableAssignation (prefixexp \[ exp_1 -> exp_2 \]) parameters explist)
   ((substExp prefixexp parameters explist) \[ (substExp exp_1 parameters explist) 
                                          -> (substExp exp_2 parameters explist) \])])

; PRE : {n2 represents the identifiers that will be substituted by the expressions
; in explist (the correspondence between elements from n2 and explist is by position
; in each list) ^ #n2 = #explist ^ all the elements from n1 belongs to n2 and are 
; in the same order}
; list = (extractExp n1 n2 explist)
; POS : {list contains those expressions from explist that are in correspondence
;        to the identifiers in n2, present in n1 }
(define-metafunction core-lang
  extractExp : parameters parameters explist -> explist
  ; Base case
  [(extractExp () parameters explist)
   ()]
  
  [(extractExp (parameter_1) (parameter_1 parameter ...) (exp_1 exp ...))
   (exp_1)]
  
  ; Inductive case
  
  [(extractExp (parameter_1 parameter_2 ...) (parameter_1 parameter_3 ...) 
               (exp_1 exp_2 ...))
   ,(append (term (exp_1)) (term (extractExp (parameter_2 ...) (parameter_3 ...) 
                                             (exp_2 ...))))]
  
  [(extractExp (parameter_1 ...) (parameter_2 parameter ...) (exp_1 exp_2 ...))
   (extractExp (parameter_1 ...) (parameter ...) (exp_2 ...))]
  )

; Auxiliar meta-function that extract those identifiers, from the first list
; that it receives, that don't appear in the second list that it receives
(define-metafunction core-lang
  extractDifVar : parameters parameters -> parameters
  
  [(extractDifVar parameters (parameter))
   ,(remove (term parameter) (term parameters))]
  
  [(extractDifVar parameters (parameter_1 parameter ...))
   (extractDifVar (removeElem parameter_1 parameters) (parameter ...))
   ])

; Axiliar metafunction that removes every occurrence of a given identifier
; from a list of identifiers
(define-metafunction core-lang
  removeElem : parameter parameters -> parameters
  ; Base case
  [(removeElem parameter (parameter))
   ()]
  [(removeElem parameter_1 (parameter_2))
   (parameter_2)]
  ; Inductive case
  [(removeElem parameter_1 (parameter_1 parameter_2 ...))
  (removeElem parameter_1 (parameter_2 ...))]
  
  [(removeElem parameter_1 (parameter_2 parameter_3 ...))
   ,(append (term (parameter_2)) (term (removeElem parameter_1 
                                                   (parameter_3 ...))))])

; Auxiliar meta-function to perform a capture-avoiding substitution over list
; of exp constructions.
(define-metafunction core-lang
  substexplist : explist parameters explist -> explist
  
  [(substexplist (exp_1) parameters explist)
   ((substExp exp_1 parameters explist))]
  
  [(substexplist (exp_1 exp_2 ...) parameters explist)
   ,(append (term ((substExp exp_1 parameters explist))) 
            (term (substexplist (exp_2 ...) parameters explist)))])

; Auxiliar meta-function to perform a capture-avoiding substitution over list
; of table fields.
(define-metafunction core-lang
  substfield : (field ...) parameters explist -> (field ...)
  
  [(substfield ((\[ exp_1 \] = exp_2)) parameters explist)
   ((\[ (substExp exp_1 parameters explist) \] = (substExp exp_2 
                                                           parameters 
                                                           explist)))]
  
  [(substfield (exp) parameters explist)
   (substExp exp parameters explist)]
  
  [(substfield (field_1 field_2 ...) parameters explist)
   ,(append (term (substfield (field_1) parameters explist)) 
            (term (substfield (field_2 ...) parameters explist)))])

; Auxiliar meta-function that does a dummy variable-variable substitution 
; (i.e. not capture-avoiding). This is useful in cases where we already know
; that the variables that we are about to substitute cannot produce a capture,
; meaning that we can realize a cheaper substitution process.
; PRE : {Name_2 never occurs in any}
; res = (dummyvarsubst any Name_1 Name_2)
; POS : {res is the result of substituting all ocurrences of Name_1 in any by Name_2}
(define-metafunction core-lang
  [(dummyvarsubst Name_1 Name_1 Name_2)
   Name_2]
  )

; Export subst meta-function
(provide substBlock
         substExp)
;                                                                                                                  
;                                                                                                                  
;                                                                                                                  
;                                                                                                                  
;   ;;;;    ;;;;;;  ;      ;;;;;;;    ;;            ;;;;;;  ;    ;  ;    ;    ;;;; ;;;;;;;  ;;;;;    ;;;;   ;    ; 
;   ;   ;   ;       ;         ;       ;;            ;       ;    ;  ;;   ;   ;   ;    ;       ;      ;  ;   ;;   ; 
;   ;   ;;  ;       ;         ;       ;;            ;       ;    ;  ; ;  ;  ;;        ;       ;     ;    ;  ; ;  ; 
;   ;    ;  ;       ;         ;      ;  ;           ;       ;    ;  ; ;  ;  ;         ;       ;     ;    ;  ; ;  ; 
;   ;    ;  ;;;;;;  ;         ;      ;  ;           ;;;;;;  ;    ;  ; ;  ;  ;         ;       ;     ;    ;  ; ;  ; 
;   ;    ;  ;       ;         ;     ;;;;;;          ;       ;    ;  ;  ; ;  ;         ;       ;     ;    ;  ;  ; ; 
;   ;   ;;  ;       ;         ;     ;    ;          ;       ;    ;  ;  ; ;  ;;        ;       ;     ;    ;  ;  ; ; 
;   ;   ;   ;       ;         ;     ;    ;          ;       ;;  ;;  ;   ;;   ;        ;       ;      ;  ;   ;   ;; 
;   ;;;;    ;;;;;;  ;;;;;;    ;    ;      ;         ;        ;;;;   ;    ;    ;;;;    ;     ;;;;;    ;;;;   ;    ; 
;                                                                                                                  
;                                                                                                                  
;                                                                                                                  
;                                                                                                                  


; We define the semantics of the binary and unary operators of our language
; in terms of operations of PLT Scheme. The "," symbol is treated as an escape
; to PLT Scheme code. So, in general, the semantics of an expression
; (◇ op_1 op_2) is defined as the PLT Scheme code (◇ (term op_1) (term op_2))
; when ◇ is also an operator of PLT Scheme.
(define-metafunction core-lang
  ; Arithmetic operations
  [(δ (+ Number_1 Number_2)) ,(+ (term Number_1) (term Number_2))]
  [(δ (- Number_1 Number_2)) ,(- (term Number_1) (term Number_2))]
  [(δ (* Number_1 Number_2)) ,(* (term Number_1) (term Number_2))]
  [(δ (/ Number_1 Number_2)) ,(/ (term Number_1) (term Number_2))]
  [(δ (^ Number_1 Number_2)) ,(expt (term Number_1) (term Number_2))]
  [(δ (% Number_1 Number_2)) ,(remainder (term Number_1) (term Number_2))]
  [(δ (- Number)) ,(- (term Number))]
  ; Number comparison
  [(δ (< Number_1 Number_2)) ,(< (term Number_1) (term Number_2))]
  [(δ (> Number_1 Number_2)) ,(> (term Number_1) (term Number_2))]
  [(δ (== Number_1 Number_2)) ,(= (term Number_1) (term Number_2))]
  [(δ (<= Number_1 Number_2)) ,(<= (term Number_1) (term Number_2))]
  [(δ (>= Number_1 Number_2)) ,(>= (term Number_1) (term Number_2))]
  [(δ (~= Number_1 Number_2)) ,(not (= (term Number_1) (term Number_2)))]
  ; String comparison)
  [(δ (< String_1 String_2)) ,(string<? (term String_1) (term String_2))]
  [(δ (> String_1 String_2)) ,(string>? (term String_1) (term String_2))]
  [(δ (== String_1 String_2)) ,(string=? (term String_1) (term String_2))]
  [(δ (<= String_1 String_2)) ,(string<=? (term String_1) (term String_2))]
  [(δ (>= String_1 String_2)) ,(string>=? (term String_1) (term String_2))]
  [(δ (~= String_1 String_2)) ,(not (string=? (term String_1) (term String_2)))]
  ; String concatenation
  [(δ (.. String_1 String_2)) ,(string-append (term String_1) (term String_2))]
  ; String length
  [(δ (\# String)) ,(string-length (term String))]
  ; Boolean comparison
  [(δ (== Boolean_1 Boolean_2)) (δbinbool == Boolean_1 Boolean_2)]
  [(δ (~= Boolean_1 Boolean_2)) (δbinbool ~= Boolean_1 Boolean_2)]
  ; Logical connectives
  [(δ (and exp_1 exp_2)) (δbinbool and exp_1 exp_2)]
  [(δ (or exp_1 exp_2)) (δbinbool or exp_1 exp_2)]
  [(δ (not exp)) (δunbool not exp)])
;  ; Boolean comparison
;  [(δ (== Boolean_1 Boolean_2)) (,(eqv? (convbool (term Boolean_1)) 
;                                       (convbool (term Boolean_2)))]
;  [(δ (~= Boolean_1 Boolean_2)) ,(not (eqv? (convbool (term Boolean_1)) 
;                                            (convbool (term Boolean_2))))]
;  ; Logical connectives
;  [(δ (and Boolean_1 Boolean_2)) ,(and (convbool (term Boolean_1)) 
;                                       (convbool (term Boolean_2)))]
;  [(δ (or Boolean_1 Boolean_2)) ,(or (convbool (term Boolean_1)) 
;                                     (convbool (term Boolean_2)))]
;  [(δ (not Boolean)) ,(not (term Boolean))])

; Auxiliar meta-function that describes the meaning of binary operations over
; booleans. Helps to enforces a modular definition of δ and a domain and codomain 
; checking.
(define-metafunction core-lang
  δbinbool : binop exp exp -> any 
  ; Boolean comparison
  [(δbinbool == Boolean_1 Boolean_2) ,(eqv? (convbool (term Boolean_1)) 
                                       (convbool (term Boolean_2)))]
  [(δbinbool ~= Boolean_1 Boolean_2) ,(not (eqv? (convbool (term Boolean_1)) 
                                            (convbool (term Boolean_2))))]
  ; Logical connectives
  [(δbinbool and exp_1 exp_2) ,(and (convbool (term exp_1)) 
                                       (convbool (term exp_2)))]
  [(δbinbool or exp_1 exp_2) ,(or (convbool (term exp_1)) 
                                     (convbool (term exp_2)))])

(define-metafunction core-lang
  δunbool : unop exp -> any
  ; Boolean comparison
  [(δunbool not exp) ,(not (convbool (term exp)))])


; To convert expressions that represent booleans in our language to boolean values
; in scheme
(define (convbool bool)
  (if (or (eqv? bool 'nil) (eqv? bool 'false) (eqv? bool 'void))
      #f
      #t))

; To export the delta function
(provide δ)

;                                                                                                                  
;                                                                                                                  
;                                                                                                                  
;                                                                                                                  
;  ;;;;;;; ;     ;  ;;;;;   ;;;;;;          ;;;;;;  ;    ;  ;    ;    ;;;; ;;;;;;; ;;;;;;;  ;;;;;    ;;;;   ;    ; 
;     ;     ;   ;   ;   ;;  ;               ;       ;    ;  ;;   ;   ;   ;    ;       ;       ;      ;  ;   ;;   ; 
;     ;     ;; ;;   ;    ;  ;               ;       ;    ;  ; ;  ;  ;;        ;       ;       ;     ;    ;  ; ;  ; 
;     ;      ; ;    ;    ;  ;               ;       ;    ;  ; ;  ;  ;         ;       ;       ;     ;    ;  ; ;  ; 
;     ;       ;     ;   ;;  ;;;;;;          ;;;;;;  ;    ;  ; ;  ;  ;         ;       ;       ;     ;    ;  ; ;  ; 
;     ;       ;     ;;;;;   ;               ;       ;    ;  ;  ; ;  ;         ;       ;       ;     ;    ;  ;  ; ; 
;     ;       ;     ;       ;               ;       ;    ;  ;  ; ;  ;;        ;       ;       ;     ;    ;  ;  ; ; 
;     ;       ;     ;       ;               ;       ;;  ;;  ;   ;;   ;        ;       ;       ;      ;  ;   ;   ;; 
;     ;       ;     ;       ;;;;;;          ;        ;;;;   ;    ;    ;;;;    ;       ;     ;;;;;    ;;;;   ;    ; 
;                                                                                                                  
;                                                                                                                  
;                                                                                                                  
;                                                                                                                  

; Type function
(define-metafunction core-lang
  [(type Number) ,(term "Number")]
  [(type nil) ,(term "Nil")]
  [(type Boolean) ,(term "Boolean")]
  [(type String) ,(term "String")]
  [(type void) ,(term "Void")]
  [(type tuple) ,(term "Tuple")]
  [(type objref) ,(term "ObjRef")]
  [(type functiondef) ,(term "Function")]
  [(type tableconstructor) ,(term "Table")]
  [(type simpvalref) ,(term "SimpValRef")])




;                                                                  
;                                                                  
;                                                                  
;                                                                  
;    ;;;;; ;      ;          ;;;;;    ;                            
;   ;    ;  ;    ;          ;    ;    ;                            
;   ;       ;    ;          ;       ;;;;;;   ;;;;    ; ;;;   ;;;;  
;   ;;      ;    ;          ;;        ;     ;;  ;;   ;;     ;;  ;; 
;     ;;;    ;  ;             ;;;     ;     ;    ;   ;      ;    ; 
;       ;;   ;  ;               ;;    ;     ;    ;   ;      ;;;;;; 
;        ;    ;;                 ;    ;     ;    ;   ;      ;      
;       ;;    ;;                ;;    ;     ;;  ;;   ;      ;;     
;   ;;;;;     ;;            ;;;;;      ;;;   ;;;;    ;       ;;;;; 
;                                                                  
;                                                                  
;                                                                  
;                                                                  
                                       

; Domain of a store
; PRE : {the store received satisfy the invariant of representation}
(define-metafunction core-lang
  ; Base case
  [(sigmaDom ())
   ,null]
  ; Inductive case
  [(sigmaDom ((\( simpvalref \, simplevalue \)) svste ...))
   (simpvalref (sigmaDom (svste ...)))])

; Determine if a reference belongs to the domain of a store
; PRE : {the store received satisfy the invariant of representation}
(define-metafunction core-lang
  refBelongsTo : simpvalref sigma -> any
  ; Base case
  [(refBelongsTo simpvalref_1 ())
   #f]
  ; Inductive cases
  [(refBelongsTo simpvalref ((\( simpvalref \, simplevalue \)) svste ...))
   #t]
  [(refBelongsTo simpvalref_1 ((\( simpvalref_2 \, simplevalue \)) svste ...))
   (refBelongsTo simpvalref_1 (svste ...))])

; Extension of refBelongsTo, to manage many references
; PRE : {the store received satisfy the invariant of representation}
(define-metafunction core-lang
  ; Base case
  [(refsBelongsTo (r) σ)
   (refBelongsTo r σ)]
  ; Inductive case
  [(refsBelongsTo (r_1 r_2 ...) σ)
   ,(and (term (refBelongsTo r_1 σ)) (term (refsBelongsTo (r_2 ...) σ)))])

(provide refBelongsTo)

; Access to a store (by dereferencing a simpValRef)
; PRE : {the store received satisfy the invariant of representation
;        and the reference dereferenced belongs to the domain of the
;        store}
(define-metafunction core-lang
  deref : σ simpvalref -> simplevalue
  [(deref (svste_1 ... (\( r_1 \, simplevalue_1 \)) svste_2 ...) r_1)
   simplevalue_1])

(provide deref)

; Modify a reference-value mapping on a given store
; PRE : {the store received satisfy the invariant of representation
;        and the reference belongs to the domain of the
;        store}
(define-metafunction core-lang
  sigmaAlter : σ simpvalref simplevalue -> σ
  
  [(sigmaAlter (svste_1 ... (\( r_1 \, sv_1 \)) svste_2 ...) r_1 sv_2)
   (svste_1 ... (\( r_1 \, sv_2 \)) svste_2 ...)])

(provide sigmaAlter)

; Used to discard values to be assigned when those are greater in number than
; the variables being assigned
; PRE : {the list of values assigned is greater in number than the variables 
;        assigned}
(define-metafunction core-lang
  discard : any svlist -> svlist
  ; Base case
  [(discard () svlist)
   ()]
  ; Inductive case
  [(discard (any_1 any_2 ...) (sv_1 sv_2 ...))
   ,(append (term (sv_1)) (term (discard (any_2 ...) (sv_2 ...))))])

(provide discard)

; Used to complete values to be assigned when those are fewer in number than
; the variables being assigned
; PRE : {namelist is the list of variable's identifiers beign assigned and svlist
;        is the list of values assigned and #(svlist) < #(namelist)}
; ret = (complete namelist svlist)
(define-metafunction core-lang
  complete : any svlist -> svlist
  ; Base case
  [(complete () ())
   ()]
  ; Inductive case
  [(complete (any_1 any_2 ...) ())
   ,(append (term (nil)) (term (complete (any_2 ...) ())))]
  [(complete (any_1 any_2 ...) (sv_1 sv_2 ...))
   ,(append (term (sv_1)) (term (complete (any_2 ...) (sv_2 ...))))])

(provide complete)

; Meta-function that generates a fresh simpvalref. Its definition depends
; heavily on the fact that references are implicit generated only by this
; function and that we don't have any kind of garbage collection.
(define-metafunction core-lang
  freshSvRef : σ -> simpvalref
  ; Empty Store
  [(freshSvRef ())
   (svr 1)]
  ; An store with at least one reference
  [(freshSvRef (svste_1 ... (\( (svr Number_1) \, simplevalue \))))
   (svr Number_2)
   (where Number_2 ,(+ (term Number_1) 1))])

; Auxiliar meta-function used by freshSvRefs that generates a list of fresh 
; simpvalrefs. This is used to obtain which fresh references were added to the store in a reduction
; of a local statement, to apply the proper substitution of names by
; references.
; PRE : {n >= 1 denotes the number of simpvalref to start with and m >= 0 
;        is the number of simpvalrefs to generate}
; ret = (freshSvRefs n m)
; POS : {ret is a list of simpvalrefs of the form ((svr n) (svr n+1) ... (svr n+m-1))}
(define-metafunction core-lang
  freshSvRefsAux : Number Number -> any
  ; Base case
  [(freshSvRefsAux Number 0)
  ()]
  ; Inductive case
  [(freshSvRefsAux Number_1 Number_2)
  ,(append (term ((svr Number_1))) (term (freshSvRefsAux Number_3 Number_4)))
  (where Number_3 ,(+ (term Number_1) 1))
  (where Number_4 ,(- (term Number_2) 1))])

; Generates a list of fresh simpvalrefs, given a store. The number of the first
; reference generated is 1 + the max. number of a simpvalref present in the store.
; PRE : {σ is a proper store and m >= 0 is the number of simpvalrefs to generate}
; ret = (freshSvRefs σ m)
; POS : {ret is a list of simpvalrefs of the form ((svr n) (svr n+1) ... (svr n+m-1))
;        where n-1 is the max. number of a simpvalref present in the store σ}
(define-metafunction core-lang
  freshSvRefs : σ Number -> any
  ; Empty store
  [(freshSvRefs () Number)
   (freshSvRefsAux 1 Number)]
  ; An store with at least one mapping
  [(freshSvRefs (svste ... (\( (svr Number_1) \, sv \))) Number_2)
   (freshSvRefsAux Number_3 Number_2)
   (where Number_3 ,(+ (term Number_1) 1))]
  )

(provide freshSvRefs)

; To store simplevalues
(define-metafunction core-lang
  addSimpVal : σ svlist -> σ
  ; Base case
  [(addSimpVal (svste ...) (sv))
   (svste ... (\( (freshSvRef (svste ...)) \, sv \)))]
  ; Inductive case
  [(addSimpVal (svste ...) (sv_1 sv_2 ...))
   (addSimpVal (svste ... (\( (freshSvRef (svste ...)) \, sv_1 \))) (sv_2 ...))])

(provide addSimpVal)


;                                                                          
;                                                                          
;                                                                          
;           ;           ;                                                  
;    ;;;;   ;                        ;;;;;    ;                            
;    ;  ;   ;                       ;    ;    ;                            
;   ;    ;  ; ;;;    ;;;;           ;       ;;;;;;   ;;;;    ; ;;;   ;;;;  
;   ;    ;  ;;  ;;      ;           ;;        ;     ;;  ;;   ;;     ;;  ;; 
;   ;    ;  ;    ;      ;             ;;;     ;     ;    ;   ;      ;    ; 
;   ;    ;  ;    ;      ;               ;;    ;     ;    ;   ;      ;;;;;; 
;   ;    ;  ;    ;      ;                ;    ;     ;    ;   ;      ;      
;    ;  ;   ;;  ;;      ;               ;;    ;     ;;  ;;   ;      ;;     
;    ;;;;   ; ;;;       ;           ;;;;;      ;;;   ;;;;    ;       ;;;;; 
;                       ;                                                  
;                       ;                                                  
;                   ;;;;                                                   
;                                                                          

; Access to a store (by dereferencing a simpValRef)
; PRE : {the store received satisfy the invariant of representation
;        and the reference dereferenced belongs to the domain of the
;        store}
(define-metafunction core-lang
  deref : σ simpvalref -> simplevalue
  [(deref (svste_1 ... (\( r_1 \, simplevalue_1 \)) svste_2 ...) r_1)
   simplevalue_1])

(provide deref)

; Meta-function that generates a fresh objref. Its definition depends
; heavily on the fact that references are implicit generated only by this
; function and that we don't have any kind of garbage collection.
(define-metafunction core-lang
  freshObjRef : θ -> objref
  ; Empty Store
  [(freshObjRef ())
   (objr 1)]
  ; An store with at least one reference
  [(freshObjRef (objste_1 ... (\( (objr Number_1) \, object \))))
   (objr Number_2)
   (where Number_2 ,(+ (term Number_1) 1))])

(provide freshObjRef)

; Auxiliar meta-function that deals with closure storing
(define-metafunction core-lang
  addFunction : θ functiondef -> θ
  
  [(addFunction (objste ...) functiondef)
   (objste ... (\( (freshObjRef (objste ...)) \, 
                   functiondef \)))])

(provide addFunction)

; Auxiliar meta-function that deals with table storing
(define-metafunction core-lang
  addTable : θ evaluatedtable -> θ
  
  [(addTable (objste ...) evaluatedtable)
   (objste ... (\( (freshObjRef (objste ...)) \, 
                   (\( evaluatedtable \, nil \)) \)))])

(provide addTable)

(define-metafunction core-lang
  addFields : evaluatedtable -> evaluatedtable
  
  [(addFields (\{ evaluatedfield ... \}))
   ,(append (term (\{))
            (append (term (addFieldsAux (evaluatedfield ...) 1))
                    (term (\}))))])

(provide addFields)

(define-metafunction core-lang
  addFieldsAux : (evaluatedfield ...) Number -> (evaluatedfield ...)
  
  [(addFieldsAux () Number)
   ()]
  
  ; When a field with a number is encountered, and that number 
  [(addFieldsAux ((\[ Number_1 \] = simplevalue) evaluatedfield ...) Number_2)
   (addFieldsAux (evaluatedfield ...) Number_2)
   (side-condition (< (term Number_1) (term Number_2)))]
  
  [(addFieldsAux ((\[ Number \] = simplevalue) evaluatedfield ...) Number)
   (addFieldsAux (evaluatedfield ...) Number)]
  
  [(addFieldsAux ((\[ simplevalue \] = simplevalue) evaluatedfield ...) Number)
   ,(append (term ((\[ simplevalue \] = simplevalue)))
            (term (addFieldsAux (evaluatedfield ...) Number)))]
  
  [(addFieldsAux (simplevalue evaluatedfield ...) Number_1)
   ,(append (term ((\[ Number_1 \] = simplevalue)))
            (term (addFieldsAux (evaluatedfield ...) Number_2)))
   (where Number_2 ,(+ 1 (term Number_1)))])

; Determines if there is a closured stored with a given tag and the same body
; PRE : {θ is a proper store && functiondef is the function to be stored}
; ret = (functionIsStored? θ functiondef)
; POS : {if there is a function stored with the same name and the same body as 
;        functiondef => ret is its objref && any other case => ret == #f}
(define-metafunction core-lang
  functionIsStored? : θ functiondef -> any
  ; Base case
  [(functionIsStored? () functiondef)
   ,#f]
  ; Inductive cases
  ; A function with the same tag and the same body is already stored 
  [(functionIsStored? ((\( objref \, (function Name (\(\) block end)) \)) objste ...) 
                      (function Name (\(\) block end)))
   objref]
  
  ; A function with the same tag but different body is stored
  [(functionIsStored? ((\( objref \, (function Name (\(\) block_1 end)) \)) objste ...) 
                      (function Name (\(\) block_2 end)))
   (functionIsStored? (objste ...) 
                      (function Name (\(\) block_2 end)))]
  
  ; A function with different tag is stored 
  [(functionIsStored? ((\( objref \, (function Name_1 funcbody) \)) objste ...)
                      (function Name_2 funcbody))
   (functionIsStored? (objste ...) (function Name_2 funcbody))]
  
  ; This case is for objects that are not functions
  [(functionIsStored? ((\( objref \, object \)) objste ...) Name)
   (functionIsStored? (objste ...) Name)]
  )

(provide functionIsStored?)
;                                  
;                                  
;                                  
;                                  
;  ;     ;  ;;;;;    ;;;;;    ;;;; 
;  ;;   ;;    ;     ;    ;   ;   ; 
;  ;;   ;;    ;     ;       ;;     
;  ;;  ; ;    ;     ;;      ;      
;  ; ; ; ;    ;       ;;;   ;      
;  ; ; ; ;    ;         ;;  ;      
;  ; ; ; ;    ;          ;  ;;     
;  ;  ;  ;    ;         ;;   ;     
;  ;  ;  ;  ;;;;;   ;;;;;     ;;;; 
;                                  
;                                  
;                                  
;                                  

; Auxiliar meta-function that resolves the problem that occurs when
; unwrapping a list of values into another one
(define-metafunction core-lang
  [(fixUnwrap (sv ... svlist))
   ,(append (term (sv ...)) (term svlist))])

(provide fixUnwrap)