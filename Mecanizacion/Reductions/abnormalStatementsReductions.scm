#lang racket
(require redex
         "../grammar.scm"
         "../Meta-functions/objStoreMetafunctions.scm"
         "../Meta-functions/tablesMetafunctions.scm"
         "../Meta-functions/metaTableMechanismMetafunctions.scm")

(define core-lang-abnormal-statements-red
  (reduction-relation
   core-lang
   #:domain (θ \; block)
   ; Abnormal statements involving table field assignment
   [--> (θ \; ((((objref \[ simplevalue \])) = (simplevalue_1))TableAssignmentWrongKey))
        (θ \; (any \( (objref simplevalue simplevalue_1) \)))
        E-TableAssignmentWrongKeyNormal
        ; Determine if the table efectively has a meta-table
        (where any (indexMetaTable objref "__newindex" θ))
        (side-condition (eq? (term (type any θ)) (term "function")))]
   
   [--> (θ \; ((((objref \[ simplevalue \])) = (simplevalue_1))TableAssignmentWrongKey))
        (θ \; (((any \[ simplevalue \])) = (simplevalue_1)))
        E-TableAssignmentWrongKeyRepeat
        ; Determine if the table efectively has a meta-table
        (where any (indexMetaTable objref "__newindex" θ))
        ; Determine if in that field we don't have a reference to a function...
        (side-condition (and (not (eq? (term any) (term nil)))
                             (not (eq? (term (type any θ)) (term "function")))))]
   
   [--> (θ_1 \; ((((objref \[ simplevalue \])) = (simplevalue_2))TableAssignmentWrongKey))
        (θ_2 \; void)
        E-TableAssignmentWrongKeyNoHandler
        ; Determine if the table has no meta-table or its meta-table
        ; doesn't has "__newindex" as a key
        (where any (indexMetaTable objref "__newindex" θ_1))
        (side-condition (eq? (term any) (term nil)))
        ; Modify table
        (where tableconstructor (assignTableField (getTable (derefTheta θ_1 objref)) 
                                                  simplevalue 
                                                  simplevalue_2))
        ; Store it
        (where θ_2 (thetaAlter θ_1 objref tableconstructor))]
   
   [--> (θ \; ((((simplevalue \[ simplevalue_1 \])) = (simplevalue_2))TableAssignOverNonTableVal))
        (θ \; (any \( (simplevalue simplevalue_1 simplevalue_2) \)))
        E-TableAssignOverNonTableValNormal
        ; Determine if simplevalue efectively has a meta-table
        (where any (indexMetaTable simplevalue "__newindex" θ))
        (side-condition (eq? (term (type any θ)) (term "function")))]
   
   [--> (θ \; ((((simplevalue \[ simplevalue_1 \])) = (simplevalue_2))TableAssignOverNonTableVal))
        (θ \; (((any \[ simplevalue_1 \])) = (simplevalue_2)))
        E-TableAssignOverNonTableValRepeat
        ; Determine if simplevalue efectively has a meta-table
        (where any (indexMetaTable simplevalue "__newindex" θ))
        (side-condition (and (not (eq? (term any) (term nil)))
                             (not (eq? (term (type any θ)) (term "function")))))]
   
   [--> (θ \; ((((simplevalue \[ simplevalue_1 \])) = (simplevalue_2))TableAssignOverNonTableVal))
        (θ \; ($builtIn error \( (any_2) \)))
        E-TableAssignOverNonTableValNoHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any (indexMetaTable simplevalue "__newindex" θ))
        (side-condition (eq? (term any) (term nil)))
        (where any_2 ,(string-append "attempt to index a "
                                    (term (type simplevalue θ))
                                    " value"))]
   ))

(provide core-lang-abnormal-statements-red)