#lang racket
; Statements that interact with the "simple value" store

(require redex
         "../grammar.scm"
         "../Meta-functions/substitution.scm"
         "../Meta-functions/simpValStoreMetafunctions.scm"
         "../Meta-functions/misc.scm"
         )

(define core-lang-stat-sv-store-red
  (reduction-relation
   core-lang
   #:domain (σ \; block)
   ; State change
   [--> (σ \; ((r_1) = (sv_1)))
        ((sigmaAlter σ r_1 sv_1) \; void)
        E-RefMapChange
        (side-condition
         (term (refsBelongsTo (r_1) σ)))]
   
   ; Local variables
   [--> (σ \; (local namelist = svlist in block end))
        ((addSimpVal σ svlist) \; (substBlock block namelist 
                                          (freshSvRefs σ Number)))
        E-Local
        (side-condition
         (= (length (term namelist)) 
            (length (term svlist))))
        (where Number ,(length (term svlist)))]
   ))
(provide core-lang-stat-sv-store-red)
