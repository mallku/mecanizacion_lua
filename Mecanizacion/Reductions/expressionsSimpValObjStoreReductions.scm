#lang racket
; Expressions that interact with both stores

(require redex
         "../grammar.scm"
         "../Meta-functions/substitution.scm"
         "../Meta-functions/simpValStoreMetafunctions.scm"
         "../Meta-functions/objStoreMetafunctions.scm")

(define core-lang-exp-sv-obj-store-red
  (reduction-relation
   core-lang
   #:domain (σ \; θ \; exp)
   ; Function call
   ; Normal case
   [--> (σ \; θ \; (sv \( svlist \)))
        ((addSimpVal σ svlist) \; θ \; (substBlock block namelist 
                                          (freshSvRefs σ Number)))
        E-Apply
        ; sv is a reference to a function
        (side-condition (eq? (term (type sv θ)) (term "function")))
        ; Obtain the function value
        (where (function Name (\( namelist \) block end)) (derefTheta θ sv))
        (where Number ,(length (term svlist)))
        ; Compare the lengths of the funtion parameter's list and the list of
        ; the given arguments in the call
        (side-condition
         (= (length (term namelist)) 
            (term Number)))]
   
   ; More arguments than those declared in the function prototype
   [--> (σ \; θ \; (sv \( svlist \)))
        ((addSimpVal σ svlist_2) \; θ \; (substBlock block namelist 
                                          (freshSvRefs σ Number)))
        E-ApplyDiscardArgs
        ; Determine that sv is a reference to a function
        (side-condition (eq? (term (type sv θ)) (term "function"))) 
        (where (function Name (\( namelist \) block end)) (derefTheta θ sv))
        (side-condition
         (< (length (term namelist)) 
            (length (term svlist))))
        (where svlist_2 (discard namelist svlist))
        (where Number ,(length (term svlist_2)))]
   
   ; Less arguments than those declared in the function prototype
   [--> (σ \; θ \; (sv \( svlist \)))
        ((addSimpVal σ svlist_2) \; θ \; (substBlock block namelist 
                                          (freshSvRefs σ Number)))
        E-ApplyFewArgs
        ; Determine that sv is a reference to a function
        (side-condition (eq? (term (type sv θ)) (term "function")))
        (where (function Name (\( namelist \) block end)) (derefTheta θ sv))
        (side-condition
         (> (length (term namelist)) 
            (length (term svlist))))
        (where svlist_2 (complete namelist svlist))
        (where Number ,(length (term svlist_2)))]
   
   ; Vararg, normal case
   [--> (σ \; θ \; (sv \( svlist \)))
        (σ_2 \; θ \; block_3)
        E-ApplyVararg
        ; Determine that sv is a reference to a function
        (side-condition (eq? (term (type sv θ)) (term "function")))
        ; The "where" clause acts as a side condition: if the function is a
        ; vararg one, then the pattern matching is successful
        (where (function Name (\( (Name_1 ... <<<) \) block end)) (derefTheta θ sv)) 
        (where Number_1 ,(length (term (Name_1 ...))))
        (where Number_2 ,(length (term svlist)))
        (side-condition
         (< (term Number_1) 
            (term Number_2)))
        ; Number_3 is the quantity of the surplus values: those that go wrapped
        ; in the tuple
        (where Number_3 ,(- (term Number_2) (term Number_1)))
        ; svlist_2 are the values that correspond to each argument
        (where svlist_2 ,(drop-right (term svlist) (term Number_3)))
        ; svlist_3 are the values that go wrapped in the tuple
        (where svlist_3 ,(take-right (term svlist) (term Number_3)))
        ; Add to the store the arguments received, before the vararg
        (where σ_2 (addSimpVal σ svlist_2))
        ; Substitute each variable occurrence by its reference
        (where block_2 (substBlock block (Name_1 ...) (freshSvRefs σ Number_1)))
        ; The vararg expression is replaced by a non-empty tuple
        (where block_3 (substBlock block_2 (<<<) ((< svlist_3 >))))]
   
   ; Vararg, few arguments
   [--> (σ \; θ \; (sv \( svlist \)))
        (σ_2 \; θ \; block_3)
        E-ApplyVarargFew
        ; Determine that sv is a reference to a function
        (side-condition (eq? (term (type sv θ)) (term "function")))
        (where (function Name (\( (Name_1 ... <<<) \) block end)) (derefTheta θ sv))
        (where Number_1 ,(length (term (Name_1 ...))))
        (where Number_2 ,(length (term svlist)))
        (side-condition
         (>= (term Number_1) 
            (term Number_2)))
        ; List of values mapped to each parameter, completed with nil values
        (where svlist_2 ,(term (complete (Name_1 ...) svlist)))
        ; Store the arguments for every variable in the function's signature
        ; except for the value that corresponds to the vararg expression
        (where σ_2 (addSimpVal σ svlist_2))
        ; Substitute each variable occurrence for its reference
        (where block_2 (substBlock block (Name_1 ...) (freshSvRefs σ Number_1)))
        ; The vararg takes the empty tuple value
        (where block_3 (substBlock block_2 (<<<) (empty)))]
   
   ; Call over a non-function value
   [--> (σ \; θ \; (sv \( svlist \)))
        (σ \; θ \; ((sv \( svlist \))WrongFunctionCall))
        E-AlertWrongFunctionCall
        ; Determine that sv is not a reference to a function
        (side-condition (not (eq? (term (type sv θ)) (term "function"))))]
   
   ))

(provide core-lang-exp-sv-obj-store-red)