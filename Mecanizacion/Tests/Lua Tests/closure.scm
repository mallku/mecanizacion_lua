#lang racket
(require redex
         "../../grammar.scm"
         "../../executionEnvironment.scm"
         "../../Reductions/standardReductionRelation.scm")
(define library
  (term (plugIntoExecutionEnvironment
((((((((((((ENV \["a"\])) = ((\{  \})))\;
(do 
 (local ($1 $2 $3) = (($builtIn toNumber \( (1.0 10) \)) 
                      ($builtIn toNumber \( (10.0 10) \)) 
                      ($builtIn toNumber \( (1.0 10) \)))
  in
     ((if (not (($1 and $2) and $3)) then
        ($builtIn error \( ("'for' limit must be a number") \))
       else
        void
       end) \;
     ((:: $while ::) \{ 
      (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
        (local (i) = ($1) in
         ((((((ENV \["a"\]) \[ i \])) = ((\{ (\[ "set" \] = (function func1519(\( (x) \) ((:: $ret ::) \{   ((i) = (x))  \})end))) (\[ "get" \] = (function func1546(\( () \) ((:: $ret ::) \{   (break $ret (< (i) >))  \})end))) \})))\;
(if (i == 3.0) then 
 (break $while empty)
else 
 void
end)) \; 
         (($1) = (($1 + $3))))
        end)
      end)
     \}))
 end)
end))\;
((ENV \["assert"\]) \( ((((ENV \["a"\]) \[ 4.0 \]) == nil)) \)))\;
((((ENV \["a"\]) \[ 1.0 \]) \[ "set" \]) \( (10.0) \)))\;
((ENV \["assert"\]) \( ((((((ENV \["a"\]) \[ 2.0 \]) \[ "get" \]) \( () \)) == 2.0)) \)))\;
((((ENV \["a"\]) \[ 2.0 \]) \[ "set" \]) \( ("a") \)))\;
((ENV \["assert"\]) \( ((((((ENV \["a"\]) \[ 3.0 \]) \[ "get" \]) \( () \)) == 3.0)) \)))\;
((ENV \["assert"\]) \( ((((((ENV \["a"\]) \[ 2.0 \]) \[ "get" \]) \( () \)) == "a")) \)))\;
(((ENV \["a"\])) = ((\{  \}))))\;
(local (t) = ((\{ "a" "b" \})) in 
((      (do 
       (local ($1 $2 $3) = (($builtIn toNumber \( (1.0 10) \)) 
                            ($builtIn toNumber \( ((\# t) 10) \)) 
                            ($builtIn toNumber \( (1.0 10) \)))
        in
           ((if (not (($1 and $2) and $3)) then
              ($builtIn error \( ("'for' limit must be a number") \))
             else
              void
             end) \;
           ((:: $while ::) \{ 
            (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
              (local (i) = ($1) in
               (      (local (k) = ((t \[ i \])) in 
(            ((((ENV \["a"\]) \[ i \])) = ((\{ (\[ "set" \] = (function func1804(\( (x y) \) ((:: $ret ::) \{   (((i) = (x))\;
((k) = (y)))  \})end))) (\[ "get" \] = (function func1849(\( () \) ((:: $ret ::) \{   (break $ret (< (i k) >))  \})end))) \})))\;
            (if (i == 2.0) then 
             (break $while empty)
            else 
             void
            end))
 end) \; 
               (($1) = (($1 + $3))))
              end)
            end)
           \}))
       end)
      end)\;
      ((((ENV \["a"\]) \[ 1.0 \]) \[ "set" \]) \( (10.0 20.0) \)))\;
      (local (r s) = (((((ENV \["a"\]) \[ 2.0 \]) \[ "get" \]) \( () \))) in 
(((((((((((            ((ENV \["assert"\]) \( (((r == 2.0) and (s == "b"))) \))\;
            ((r s) = (((((ENV \["a"\]) \[ 1.0 \]) \[ "get" \]) \( () \)))))\;
            ((ENV \["assert"\]) \( (((r == 10.0) and (s == 20.0))) \)))\;
            ((((ENV \["a"\]) \[ 2.0 \]) \[ "set" \]) \( ("a" "b") \)))\;
            ((r s) = (((((ENV \["a"\]) \[ 2.0 \]) \[ "get" \]) \( () \)))))\;
            ((ENV \["assert"\]) \( (((r == "a") and (s == "b"))) \)))\;
            (do 
             (local ($1 $2 $3) = (($builtIn toNumber \( (1.0 10) \)) 
                                  ($builtIn toNumber \( (3.0 10) \)) 
                                  ($builtIn toNumber \( (1.0 10) \)))
              in
                 ((if (not (($1 and $2) and $3)) then
                    ($builtIn error \( ("'for' limit must be a number") \))
                   else
                    void
                   end) \;
                 ((:: $while ::) \{ 
                  (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
                    (local (i) = ($1) in
                     ((            (((ENV \["f"\])) = ((function func2165(\( () \)                                             ((:: $ret ::) \{                                                                                           (break $ret (< (i) >))                                              \})                                            end))))\;
            (break $while empty)) \; 
                     (($1) = (($1 + $3))))
                    end)
                  end)
                 \}))
             end)
            end))\;
            ((ENV \["assert"\]) \( ((((ENV \["f"\]) \( () \)) == 1.0)) \)))\;
            (do 
             (local ($1 $2 $3) = (($builtIn toNumber \( (1.0 10) \)) 
                                  ($builtIn toNumber \( ((\# t) 10) \)) 
                                  ($builtIn toNumber \( (1.0 10) \)))
              in
                 ((if (not (($1 and $2) and $3)) then
                    ($builtIn error \( ("'for' limit must be a number") \))
                   else
                    void
                   end) \;
                 ((:: $while ::) \{ 
                  (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
                    (local (k) = ($1) in
                     (            (local (v) = ((t \[ k \])) in 
(                  (((ENV \["f"\])) = ((function func2260(\( () \)                                                         ((:: $ret ::) \{                                                                                                                   (break $ret (< (k v) >))                                                          \})                                                        end))))\;
                  (break $while empty))
 end) \; 
                     (($1) = (($1 + $3))))
                    end)
                  end)
                 \}))
             end)
            end))\;
            ((ENV \["assert"\]) \( ((((\((\{ ((ENV \["f"\]) \( () \)) \})\)) \[ 1.0 \]) == 1.0)) \)))\;
            ((ENV \["assert"\]) \( ((((\((\{ ((ENV \["f"\]) \( () \)) \})\)) \[ 2.0 \]) == "a")) \)))\;
            (local (b) = () in 
((((((                  (((ENV \["f"\])) = ((function func2419 (\( (x) \)
                                                         ((:: $ret ::) \{
                                                          (local (first) = (1.0) in 
                                                                ((:: $while ::) \{ 
                                                                 (while 1.0 do 
(                                                                  (if ((x == 3.0) and (not first)) then 
                                                                   (break $ret empty)
                                                                  else 
                                                                   void
                                                                  end)\;
                                                                  (local (a) = ("xuxu") in 
((                                                                        ((b) = ((function func2537(\( (op y) \)                                                                                                                                                        ((:: $ret ::) \{                                                                                                                                                                                                                                                                                                                 (if (op == "set") then 
                                                                                                                                                        ((a) = ((x + y)))
                                                                                                                                                       else 
                                                                                                                                                        (break $ret (< (a) >))
                                                                                                                                                       end)                                                                                                                                                         \})                                                                                                                                                       end))))\;
                                                                        (if (x == 1.0) then 
                                                                         (do 
                                                                          (break $while empty)
                                                                         end)
                                                                        else 
                                                                         (if (x == 2.0) then 
                                                                          (break $ret empty)
                                                                         else 
                                                                          (if (not (x == 3.0)) then 
                                                                           ((ENV \["error"\]) \( () \))
                                                                          else 
                                                                           void
                                                                          end)
                                                                         end)
                                                                        end))\;
                                                                        ((first) = (nil)))
 end))
                                                                 end)                                                                \})
 end)
                                                         \}) 
                                                        end))))\;
                  (do 
                   (local ($1 $2 $3) = (($builtIn toNumber \( (1.0 10) \)) 
                                        ($builtIn toNumber \( (3.0 10) \)) 
                                        ($builtIn toNumber \( (1.0 10) \)))
                    in
                       ((if (not (($1 and $2) and $3)) then
                          ($builtIn error \( ("'for' limit must be a number") \))
                         else
                          void
                         end) \;
                       ((:: $while ::) \{ 
                        (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
                          (local (i) = ($1) in
                           (((((                  ((ENV \["f"\]) \( (i) \))\;
                  ((ENV \["assert"\]) \( (((b \( ("get") \)) == "xuxu")) \)))\;
                  (b \( ("set" 10.0) \)))\;
                  ((ENV \["assert"\]) \( (((b \( ("get") \)) == (10.0 + i))) \)))\;
                  ((b) = (nil))) \; 
                           (($1) = (($1 + $3))))
                          end)
                        end)
                       \}))
                   end)
                  end))\;
                  ((ENV \["pcall"\]) \( ((ENV \["f"\]) 4.0) \)))\;
                  ((ENV \["assert"\]) \( (((b \( ("get") \)) == "xuxu")) \)))\;
                  (b \( ("set" 10.0) \)))\;
                  ((ENV \["assert"\]) \( (((b \( ("get") \)) == 14.0)) \)))\;
                  (local (w) = () in 
((((                        (((ENV \["f"\])) = ((function func3035 (\( (x) \)
                                                                     ((:: $ret ::) \{
                                                                      (break $ret (< ((function func3058(\( (y) \) ((:: $ret ::) \{   (break $ret (< ((function func3082(\( (z) \) ((:: $ret ::) \{   (break $ret (< ((((w + x) + y) + z)) >))  \})end))) >))  \})end))) >))
                                                                     \}) 
                                                                    end))))\;
                        (((ENV \["y"\])) = (((ENV \["f"\]) \( (10.0) \)))))\;
                        ((w) = (1.3)))\;
                        ((ENV \["assert"\]) \( (((((ENV \["y"\]) \( (20.0) \)) \( (30.0) \)) == (60.0 + w))) \)))\;
                        (local (t) = (nil) in 
(                         ((t) = ((function func4305 (\(()\)                                   ((:: $ret ::) \{
                                    (local (c) = (nil) in 
(                                     ((c) = ((function func4327 (\((a b)\)                                               ((:: $ret ::) \{
                                                ((ENV \["assert"\]) \( (((a == "test") and (b == "OK"))) \))
                                               \})
                                              end))))\;
                                          (local (v) = (nil) in 
(                                           ((v) = ((function func4385 (\((f <<<)\)                                                     ((:: $ret ::) \{
                                                      (c \( ("test" (((not ((f \( () \)) == 1.0)) and "FAILED") or "OK")) \))
                                                     \})
                                                    end))))\;
                                                (local (x) = (1.0) in 
                                                      (break $ret (< ((v \( ((function func4474(\( () \) ((:: $ret ::) \{   (break $ret (< (x) >))  \})end))) \))) >))
 end))
 end))
 end)
                                   \})
                                  end))))\;
                              (t \( () \)))
 end))
 end))
 end))
 end))
 end))





)))

(define (void? red)
  (and (redex-match core-lang
              (σ \; θ \; void)
              (first red))
       (eq? (length red) 1)))

(define (lua-closure-test-suite)
  (test-predicate void? (apply-reduction-relation* core-lan-stand-reduc-red-rel library))
  (test-results))

(provide lua-closure-test-suite)