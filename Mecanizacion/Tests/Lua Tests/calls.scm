#lang racket
(require redex
         "../../grammar.scm"
         "../../executionEnvironment.scm"
         "../../Reductions/standardReductionRelation.scm")
(define library
  (term (plugIntoExecutionEnvironment
         ((((((((((((((((((((ENV \["assert"\]) \( ((((ENV \["type"\]) \( ((1.0 < 2.0)) \)) == "boolean")) \))\;
((ENV \["assert"\]) \( (((((ENV \["type"\]) \( (true) \)) == "boolean") and (((ENV \["type"\]) \( (false) \)) == "boolean"))) \)))\;
((ENV \["assert"\]) \( ((((((((ENV \["type"\]) \( (nil) \)) == "nil") and (((ENV \["type"\]) \( ((- 3.0)) \)) == "number")) and (((ENV \["type"\]) \( ("x") \)) == "string")) and (((ENV \["type"\]) \( ((\{  \})) \)) == "table")) and (((ENV \["type"\]) \( ((ENV \["type"\])) \)) == "function"))) \)))\;
(((ENV \["f"\])) = (nil)))\;
(((ENV \["f"\])) = ((function func394 (\( (x) \)
                     ((:: $ret ::) \{
                      (break $ret (< (((ENV \["a"\]) : x \( (x) \))) >))
                     \}) 
                    end)))))\;
((ENV \["assert"\]) \( ((((ENV \["type"\]) \( ((ENV \["f"\])) \)) == "function")) \)))\;
(((ENV \["fact"\])) = (false)))\;
(do 
 (local (res) = (1.0) in 
       (local (fact) = (nil) in 
(        ((fact) = ((function func536 (\((n)\)                     ((:: $ret ::) \{
                      (if (n == 0.0) then 
                       (break $ret (< (res) >))
                      else 
                       (break $ret (< ((n * (fact \( ((n - 1.0)) \)))) >))
                      end)
                     \})
                    end))))\;
             ((ENV \["assert"\]) \( (((fact \( (5.0) \)) == 120.0)) \)))
 end)
 end)
end))\;
((ENV \["assert"\]) \( (((ENV \["fact"\]) == false)) \)))\;
(((ENV \["a"\])) = ((\{ (\["i"\] = 10.0) \}))))\;
(((ENV \["self"\])) = (20.0)))\;
((((ENV \["a"\]) \["x"\])) = ((function func723 (\( (self x) \)
                               ((:: $ret ::) \{
                                (break $ret (< ((x + (self \[ "i" \]))) >))
                               \}) 
                              end)))))\;
((((ENV \["a"\]) \["y"\])) = ((function func760 (\( (x) \)
                               ((:: $ret ::) \{
                                (break $ret (< ((x + (ENV \["self"\]))) >))
                               \}) 
                              end)))))\;
((ENV \["assert"\]) \( (((((ENV \["a"\]) : x \( (1.0) \)) + 10.0) == (((ENV \["a"\]) \[ "y" \]) \( (1.0) \)))) \)))\;
((((ENV \["a"\]) \[ "t" \])) = ((\{ (\["i"\] = (- 100.0)) \}))))\;
(((((ENV \["a"\]) \[ "t" \]) \[ "x" \])) = ((function func851(\( (self a b) \)                                             ((:: $ret ::) \{                                                                                           (break $ret (< ((((self \[ "i" \]) + a) + b)) >))                                              \})                                            end)))))\;
((ENV \["assert"\]) \( (((((ENV \["a"\]) \[ "t" \]) : x \( (2.0 3.0) \)) == (- 95.0))) \)))\;
(do 
 (local (a) = ((\{ (\["x"\] = 0.0) \})) in 
(       (((a \["add"\])) = ((function func945 (\( (self x) \)
                                  ((:: $ret ::) \{
(                                   (((self \[ "x" \]) (a \[ "y" \])) = (((self \[ "x" \]) + x) 20.0))\;
                                   (break $ret (< (self) >)))
                                  \}) 
                                 end))))\;
       ((ENV \["assert"\]) \( (((((((a : add \( (10.0) \)) : add \( (20.0) \)) : add \( (30.0) \)) \[ "x" \]) == 60.0) and ((a \[ "y" \]) == 20.0))) \)))
 end)
end))\;
(local (a) = ((\{ (\["b"\] = (\{ (\["c"\] = (\{  \})) \})) \})) in 
(((((((((((((((((((((((      (((((a \["b"\]) \["c"\]) \["f1"\])) = ((function func1093 (\( (x) \)
                                                   ((:: $ret ::) \{
                                                    (break $ret (< ((x + 1.0)) >))
                                                   \}) 
                                                  end))))\;
      (((((a \["b"\]) \["c"\]) \["f2"\])) = ((function func1130 (\( (self x y) \)
                                                   ((:: $ret ::) \{
                                                    (((self \[ x \])) = (y))
                                                   \}) 
                                                  end)))))\;
      ((ENV \["assert"\]) \( ((((((a \[ "b" \]) \[ "c" \]) \[ "f1" \]) \( (4.0) \)) == 5.0)) \)))\;
      (((a \[ "b" \]) \[ "c" \]) : f2 \( ("k" 12.0) \)))\;
      ((ENV \["assert"\]) \( (((((a \[ "b" \]) \[ "c" \]) \[ "k" \]) == 12.0)) \)))\;
      (((ENV \["t"\])) = (nil)))\;
      (((ENV \["f"\])) = ((function func1278 (\( (a b c) \)
                                 ((:: $ret ::) \{
                                  (local (d) = ("a") in 
                                        (((ENV \["t"\])) = ((\{ a b c d \})))
 end)
                                 \}) 
                                end)))))\;
      ((ENV \["f"\]) \( (1.0 2.0) \)))\;
      ((ENV \["assert"\]) \( (((((((ENV \["t"\]) \[ 1.0 \]) == 1.0) and (((ENV \["t"\]) \[ 2.0 \]) == 2.0)) and (((ENV \["t"\]) \[ 3.0 \]) == nil)) and (((ENV \["t"\]) \[ 4.0 \]) == "a"))) \)))\;
      ((ENV \["f"\]) \( (1.0 2.0 3.0 4.0) \)))\;
      ((ENV \["assert"\]) \( (((((((ENV \["t"\]) \[ 1.0 \]) == 1.0) and (((ENV \["t"\]) \[ 2.0 \]) == 2.0)) and (((ENV \["t"\]) \[ 3.0 \]) == 3.0)) and (((ENV \["t"\]) \[ 4.0 \]) == "a"))) \)))\;
      (((ENV \["err_on_n"\])) = ((function func1863 (\( (n) \)
                                        ((:: $ret ::) \{
                                         (if (n == 0.0) then 
(                                          ((ENV \["error"\]) \( () \))\;
                                          ((ENV \["exit"\]) \( (1.0) \)))
                                         else 
(                                          ((ENV \["err_on_n"\]) \( ((n - 1.0)) \))\;
                                          ((ENV \["exit"\]) \( (1.0) \)))
                                         end)
                                        \}) 
                                       end)))))\;
      (do 
       (((ENV \["dummy"\])) = ((function func1967 (\( (n) \)
                                       ((:: $ret ::) \{
                                        (if (n > 0.0) then 
(                                         ((ENV \["assert"\]) \( ((not ((ENV \["pcall"\]) \( ((ENV \["err_on_n"\]) n) \)))) \))\;
                                         ((ENV \["dummy"\]) \( ((n - 1.0)) \)))
                                        else 
                                         void
                                        end)
                                       \}) 
                                      end))))
      end))\;
      ((ENV \["dummy"\]) \( (10.0) \)))\;
      (((ENV \["deep"\])) = ((function func2088 (\( (n) \)
                                    ((:: $ret ::) \{
                                     (if (n > 0.0) then 
                                      ((ENV \["deep"\]) \( ((n - 1.0)) \))
                                     else 
                                      void
                                     end)
                                    \}) 
                                   end)))))\;
      ((ENV \["deep"\]) \( (10.0) \)))\;
      ((a) = (nil)))\;
      ((\((function func2582(\( (x) \) ((:: $ret ::) \{   ((a) = (x))  \})end))\)) \( (23.0) \)))\;
      ((ENV \["assert"\]) \( (((a == 23.0) and (((\((function func2628(\( (x) \) ((:: $ret ::) \{   (break $ret (< ((x * 2.0)) >))  \})end))\)) \( (20.0) \)) == 40.0))) \)))\;
      (((ENV \["Z"\])) = ((function func2719(\( (le) \)                                 ((:: $ret ::) \{                                                                   (local (a) = (nil) in 
(                                 ((a) = ((function func2745 (\((f)\)                                           ((:: $ret ::) \{
                                            (break $ret (< ((le \( ((function func2778(\( (x) \) ((:: $ret ::) \{   (break $ret (< (((f \( (f) \)) \( (x) \))) >))  \})end))) \))) >))
                                           \})
                                          end))))\;
                                      (break $ret (< ((a \( (a) \))) >)))
 end)                                  \})                                end)))))\;
      (((ENV \["F"\])) = ((function func2881(\( (f) \)                                 ((:: $ret ::) \{                                                                   (break $ret (< ((function func2907(\( (n) \) ((:: $ret ::) \{   (if (n == 0.0) then 
 (break $ret (< (1.0) >))
else 
 (break $ret (< ((n * (f \( ((n - 1.0)) \)))) >))
end)  \})end))) >))                                  \})                                end)))))\;
      (((ENV \["fat"\])) = (((ENV \["Z"\]) \( ((ENV \["F"\])) \)))))\;
      ((ENV \["assert"\]) \( ((((((ENV \["fat"\]) \( (0.0) \)) == 1.0) and (((ENV \["fat"\]) \( (4.0) \)) == 24.0)) and ((((ENV \["Z"\]) \( ((ENV \["F"\])) \)) \( (5.0) \)) == (5.0 * (((ENV \["Z"\]) \( ((ENV \["F"\])) \)) \( (4.0) \)))))) \)))\;
      (local (g) = (nil) in 
((((((       ((g) = ((function func3105 (\((z)\)                 ((:: $ret ::) \{
                  (local (f) = (nil) in 
(                   ((f) = ((function func3128 (\((a b c d)\)                             ((:: $ret ::) \{
                              (break $ret (< ((function func3160(\( (x y) \) ((:: $ret ::) \{   (break $ret (< ((((((((a + b) + c) + d) + a) + x) + y) + z)) >))  \})end))) >))
                             \})
                            end))))\;
                        (break $ret (< ((f \( (z (z + 1.0) (z + 2.0) (z + 3.0)) \))) >)))
 end)
                 \})
                end))))\;
            (((ENV \["f"\])) = ((g \( (10.0) \)))))\;
            ((ENV \["assert"\]) \( ((((ENV \["f"\]) \( (9.0 16.0) \)) == (((((((10.0 + 11.0) + 12.0) + 13.0) + 10.0) + 9.0) + 16.0) + 10.0))) \)))\;
            (((ENV \["Z"\]) (ENV \["F"\]) (ENV \["f"\])) = (nil)))\;
            (((ENV \["unlpack"\])) = ((function func3351 (\( (t i) \)
                                                   ((:: $ret ::) \{
(                                                    ((i) = ((i or 1.0)))\;
                                                    (if (\((i <= (\# t))\)) then 
                                                     (break $ret (< ((t \[ i \]) ((ENV \["unlpack"\]) \( (t (i + 1.0)) \))) >))
                                                    else 
                                                     void
                                                    end))
                                                   \}) 
                                                  end)))))\;
            (((ENV \["equaltab"\])) = ((function func3452 (\( (t1 t2) \)
                                                    ((:: $ret ::) \{
(                                                     ((ENV \["assert"\]) \( (((\# t1) == (\# t2))) \))\;
                                                     (do 
                                                      (local ($1 $2 $3) = (($builtIn toNumber \( (1.0 10) \)) 
                                                                           ($builtIn toNumber \( ((\# t1) 10) \)) 
                                                                           ($builtIn toNumber \( (1.0 10) \)))
                                                       in
                                                          ((if (not (($1 and $2) and $3)) then
                                                             ($builtIn error \( ("'for' limit must be a number") \))
                                                            else
                                                             void
                                                            end) \;
                                                          ((:: $while ::) \{ 
                                                           (while (((0 < $3) and ($1 <= $2)) or (($3 <= 0) and ($2 <= $1))) do
                                                             (local (i) = ($1) in
                                                              (                                                     ((ENV \["assert"\]) \( (((t1 \[ (ENV \["i"\]) \]) == (t2 \[ (ENV \["i"\]) \]))) \)) \; 
                                                              (($1) = (($1 + $3))))
                                                             end)
                                                           end)
                                                          \}))
                                                      end)
                                                     end))
                                                    \}) 
                                                   end)))))\;
            (local (pack) = ((function func3572(\( ( <<<) \)             ((:: $ret ::) \{                           (break $ret (< ((\((((ENV \["table"\]) \[ "pack" \]) \( (<<<) \))\))) >))              \})            end))) in 
((                  (((ENV \["f"\])) = ((function func3617 (\( () \)
                                                         ((:: $ret ::) \{
                                                          (break $ret (< (1.0 2.0 30.0 4.0) >))
                                                         \}) 
                                                        end))))\;
                  (((ENV \["ret2"\])) = ((function func3650 (\( (a b) \)
                                                            ((:: $ret ::) \{
                                                             (break $ret (< (a b) >))
                                                            \}) 
                                                           end)))))\;
                  (local (a b c d) = (((ENV \["unlpack"\]) \( ((\{ 1.0 2.0 3.0 \})) \))) in 
(((((((((((((                        ((ENV \["assert"\]) \( (((((a == 1.0) and (b == 2.0)) and (c == 3.0)) and (d == nil))) \))\;
                        ((a) = ((\{ 1.0 2.0 3.0 4.0 false 10.0 "alo" false (ENV \["assert"\]) \}))))\;
                        ((ENV \["equaltab"\]) \( ((pack \( (((ENV \["unlpack"\]) \( (a) \))) \)) a) \)))\;
                        ((ENV \["equaltab"\]) \( ((pack \( (((ENV \["unlpack"\]) \( (a) \)) (- 1.0)) \)) (\{ 1.0 (- 1.0) \})) \)))\;
                        ((a b c d) = (((ENV \["ret2"\]) \( (((ENV \["f"\]) \( () \))) \)) ((ENV \["ret2"\]) \( (((ENV \["f"\]) \( () \))) \)))))\;
                        ((ENV \["assert"\]) \( (((((a == 1.0) and (b == 1.0)) and (c == 2.0)) and (d == nil))) \)))\;
                        ((a b c d) = (((ENV \["unlpack"\]) \( ((pack \( (((ENV \["ret2"\]) \( (((ENV \["f"\]) \( () \))) \)) ((ENV \["ret2"\]) \( (((ENV \["f"\]) \( () \))) \))) \))) \)))))\;
                        ((ENV \["assert"\]) \( (((((a == 1.0) and (b == 1.0)) and (c == 2.0)) and (d == nil))) \)))\;
                        ((a b c d) = (((ENV \["unlpack"\]) \( ((pack \( (((ENV \["ret2"\]) \( (((ENV \["f"\]) \( () \))) \)) (\(((ENV \["ret2"\]) \( (((ENV \["f"\]) \( () \))) \))\))) \))) \)))))\;
                        ((ENV \["assert"\]) \( (((((a == 1.0) and (b == 1.0)) and (c == nil)) and (d == nil))) \)))\;
                        ((a) = (((ENV \["ret2"\]) \( ((\{ ((ENV \["unlpack"\]) \( ((\{ 1.0 2.0 3.0 \})) \)) ((ENV \["unlpack"\]) \( ((\{ 3.0 2.0 1.0 \})) \)) ((ENV \["unlpack"\]) \( ((\{ "a" "b" \})) \)) \})) \)))))\;
                        ((ENV \["assert"\]) \( ((((((a \[ 1.0 \]) == 1.0) and ((a \[ 2.0 \]) == 3.0)) and ((a \[ 3.0 \]) == "a")) and ((a \[ 4.0 \]) == "b"))) \)))\;
                        ((ENV \["rawget"\]) \( ((\{  \}) "x" 1.0) \)))\;
                        ((ENV \["rawset"\]) \( ((\{  \}) "x" 1.0 2.0) \)))
 end))
 end))
 end))
 end))

 
)))

(define (void? red)
  (and (redex-match core-lang
              (σ \; θ \; void)
              (first red))
       (eq? (length red) 1)))

(define (lua-calls-test-suite)
  (test-predicate void? (apply-reduction-relation* core-lan-stand-reduc-red-rel library))
  (test-results))

(provide lua-calls-test-suite)