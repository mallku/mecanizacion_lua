#lang scheme
(require redex
         "./fv.scm"
         "./substitution.scm"
         "./delta.scm"
         "./simpValStore.scm"
         "./objStore.scm"
         "./misc.scm")