#lang scheme
; Expressions that don't interact with some store

(require redex
         "../grammar.scm"
         "../Meta-functions/delta.scm"
         "../Meta-functions/misc.scm"
         "../Meta-functions/coercion.scm"
         "../Meta-functions/builtInProceduresMetaFunctions.scm"
         racket/format ; String conversion
         )

(define core-lang-expressions-red
  (reduction-relation
   core-lang
   #:domain exp
   ; Tuples
   [--> (in-hole Et empty)
        (in-hole Et nil)
        E-TruncateEmptyTuple]
   
   [--> (in-hole Eu empty)
        (in-hole Eu nil)
        E-UnwrapEmptyTuple]
   
   [--> (in-hole Et (< (simplevalue_1 simplevalue ...) >))
        (in-hole Et simplevalue_1)
        E-TruncateNonEmptyTuple]
   
   [--> (in-hole Eu (< (simplevalue ...) >))
        (fixUnwrap Eu (< (simplevalue ...) >))
        E-UnwrapNonEmptyTuple]
   
   [--> (in-hole Et void)
        (in-hole Et nil)
        E-ConvertVoidToNilWhereTruncate]
   
   [--> (in-hole Eu void)
        (in-hole Eu nil)
        E-ConvertVoidToNilWhereUnwrap]
   ; Operator ()
   [--> (\( simplevalue \))
        simplevalue
        E-ParenthesisAnyOtherValue
        (side-condition (not (equal? (term (simpValType simplevalue))
                                     "Tuple")))]
   
   ; Method call
   [--> (sv : Name \( explist \))
        ((sv \[ String \]) \( explist_2 \))
        E-MethodCall
        (where explist_2 ,(append (term (sv)) (term explist)))
        (where String ,(~a (term Name)))]
   
   ; Primitive Operations
   ; Arithmetic operations
   [--> (Number_1 + Number_2)
        (δ (+ Number_1 Number_2))
        E-Addition]
   [--> (Number_1 - Number_2)
        (δ (- Number_1 Number_2))
        E-Substraction]
   [--> (Number_1 * Number_2)
        (δ (* Number_1 Number_2))
        E-Multiplication]
   [--> (Number_1 / Number_2)
        (δ (/ Number_1 Number_2))
        E-Division]
   [--> (Number_1 ^ Number_2)
        (δ (^ Number_1 Number_2))
        E-Exponent]
   [--> (Number_1 % Number_2)
        (δ (% Number_1 Number_2))
        E-Module]
   [--> (- Number)
        (δ (- Number))
        E-Negation]
   ; Equality comparison
   [--> (sv_1 == sv_2)
        true
        E-EqualitySuccess
        (side-condition (equal? (term (δ (== sv_1 sv_2))) (term true)))]
   ; Number order comparison
   [--> (Number_1 < Number_2)
        (δ (< Number_1 Number_2))
        E-LessThanNumber]
   [--> (Number_1 <= Number_2)
        (δ (<= Number_1 Number_2))
        E-LessOrEqualThanNumber]
   ; String order comparison
   [--> (String_1 < String_2)
        (δ (< String_1 String_2))
        E-LessThanString]
   [--> (String_1 <= String_2)
        (δ (<= String_1 String_2))
        E-LessOrEqualThanString]
   ; Translation of expressions involving > and >=
   [--> (sv_1 > sv_2)
        (sv_2 < sv_1)
        E-GreaterThan]
   [--> (sv_1 >= sv_2)
        (sv_2 <= sv_1)
        E-GreaterOrEqualThan]
   ; String concatenation
   [--> (String_1 .. String_2)
        (δ (.. String_1 String_2))
        E-StringConcat]
   ; String length
   [--> (\# String)
        (δ (\# String))
        E-StringLength]
   ; Logical conectives
   [--> (sv and exp)
        (\( (δ (and sv exp)) \))
        E-And]
   [--> (sv or exp)
        (\( (δ (or sv exp)) \))
        E-Or]
   [--> (not sv)
        (δ (not sv))
        E-Not]
   ; Coercion in arithmetical expressions
   [--> (sv_1 + sv_2)
        (any_1 + any_2)
        E-AdditionCoercion
        (side-condition (or (not (equal? (term (simpValType sv_1)) 
                                         "number"))
                            (not (equal? (term (simpValType sv_2)) 
                                         "number"))))
        (where any_1 (toNumber sv_1 10))
        (where any_2 (toNumber sv_2 10))
        (side-condition (equal? (term (simpValType any_1)) "number"))
        (side-condition (equal? (term (simpValType any_2)) "number"))]
   [--> (sv_1 - sv_2)
        (any_1 - any_2)
        E-SubstractionCoercion
        (side-condition (or (not (equal? (term (simpValType sv_1)) 
                                         "number"))
                            (not (equal? (term (simpValType sv_2)) 
                                         "number"))))
        (where any_1 (toNumber sv_1 10))
        (where any_2 (toNumber sv_2 10))
        (side-condition (equal? (term (simpValType any_1)) "number"))
        (side-condition (equal? (term (simpValType any_2)) "number"))]
   [--> (sv_1 * sv_2)
        (any_1 * any_2)
        E-MultiplicationCoercion
        (side-condition (or (not (equal? (term (simpValType sv_1)) 
                                         "number"))
                            (not (equal? (term (simpValType sv_2)) 
                                         "number"))))
        (where any_1 (toNumber sv_1 10))
        (where any_2 (toNumber sv_2 10))
        (side-condition (equal? (term (simpValType any_1)) "number"))
        (side-condition (equal? (term (simpValType any_2)) "number"))]
   [--> (sv_1 / sv_2)
        (any_1 / any_2)
        E-DivisionCoercion
        (side-condition (or (not (equal? (term (simpValType sv_1)) 
                                         "number"))
                            (not (equal? (term (simpValType sv_2)) 
                                         "number"))))
        (where any_1 (toNumber sv_1 10))
        (where any_2 (toNumber sv_2 10))
        (side-condition (equal? (term (simpValType any_1)) "number"))
        (side-condition (equal? (term (simpValType any_2)) "number"))]
   [--> (sv_1 ^ sv_2)
        (any_1 ^ any_2)
        E-PowCoercion
        (side-condition (or (not (equal? (term (simpValType sv_1)) 
                                         "number"))
                            (not (equal? (term (simpValType sv_2)) 
                                         "number"))))
        (where any_1 (toNumber sv_1 10))
        (where any_2 (toNumber sv_2 10))
        (side-condition (equal? (term (simpValType any_1)) "number"))
        (side-condition (equal? (term (simpValType any_2)) "number"))]
   [--> (sv_1 % sv_2)
        (any_1 % any_2)
        E-ModuleCoercion
        (side-condition (or (not (equal? (term (simpValType sv_1)) 
                                         "number"))
                            (not (equal? (term (simpValType sv_2)) 
                                         "number"))))
        (where any_1 (toNumber sv_1 10))
        (where any_2 (toNumber sv_2 10))
        (side-condition (equal? (term (simpValType any_1)) "number"))
        (side-condition (equal? (term (simpValType any_2)) "number"))]
   [--> (- sv)
        (- any)
        (side-condition (not (equal? (term (simpValType sv)) "number")))
        E-NegationCoercion
        (where any (toNumber sv 10))
        (side-condition (equal? (term (simpValType any)) "number"))]
   ; Coercion in string operations
   [--> (sv_1 .. sv_2)
        (any_1 .. any_2)
        E-StringConcatCoercion
        (side-condition (or (equal? (term (simpValType sv_1)) 
                                         "number")
                            (equal? (term (simpValType sv_2)) 
                                         "number")))
        (where any_1 (toString sv_1))
        (where any_2 (toString sv_2))
        (side-condition (equal? (term (simpValType any_1)) "string"))
        (side-condition (equal? (term (simpValType any_2)) "string"))]
   ; Abnormal situations with primitive operators
   [--> (sv_1 + sv_2)
        ((sv_1 + sv_2)AdditionWrongOperands)
        E-AlertAdditionWrongOperands
        (side-condition (or (not (equal? (term (simpValType sv_1)) "number"))
                            (not (equal? (term (simpValType sv_2)) "number"))))
        (side-condition (or (not (equal? (term (simpValType (toNumber sv_1 10))) "number"))
                            (not (equal? (term (simpValType (toNumber sv_2 10))) "number"))))]
   [--> (sv_1 - sv_2)
        ((sv_1 - sv_2)SubstractionWrongOperands)
        E-AlertSubstractionWrongOperands
        (side-condition (or (not (equal? (term (simpValType sv_1)) "number"))
                            (not (equal? (term (simpValType sv_2)) "number"))))
        (side-condition (or (not (equal? (term (simpValType (toNumber sv_1 10))) "number"))
                            (not (equal? (term (simpValType (toNumber sv_2 10))) "number"))))]
   [--> (sv_1 * sv_2)
        ((sv_1 * sv_2)MultiplicationWrongOperands)
        E-AlertMultiplicationWrongOperands
        (side-condition (or (not (equal? (term (simpValType sv_1)) "number"))
                            (not (equal? (term (simpValType sv_2)) "number"))))
        (side-condition (or (not (equal? (term (simpValType (toNumber sv_1 10))) "number"))
                            (not (equal? (term (simpValType (toNumber sv_2 10))) "number"))))]
   [--> (sv_1 / sv_2)
        ((sv_1 / sv_2)DivisionWrongOperands)
        E-AlertDivisionWrongOperands
        (side-condition (or (not (equal? (term (simpValType sv_1)) "number"))
                            (not (equal? (term (simpValType sv_2)) "number"))))
        (side-condition (or (not (equal? (term (simpValType (toNumber sv_1 10))) "number"))
                            (not (equal? (term (simpValType (toNumber sv_2 10))) "number"))))]
   [--> (sv_1 ^ sv_2)
        ((sv_1 ^ sv_2)ExponentiationWrongOperands)
        E-AlertExponentiationWrongOperands
        (side-condition (or (not (equal? (term (simpValType sv_1)) "number"))
                            (not (equal? (term (simpValType sv_2)) "number"))))
        (side-condition (or (not (equal? (term (simpValType (toNumber sv_1 10))) "number"))
                            (not (equal? (term (simpValType (toNumber sv_2 10))) "number"))))]
   [--> (sv_1 % sv_2)
        ((sv_1 % sv_2)ModuleWrongOperands)
        E-AlertModuleWrongOperands
        (side-condition (or (not (equal? (term (simpValType sv_1)) "number"))
                            (not (equal? (term (simpValType sv_2)) "number"))))
        (side-condition (or (not (equal? (term (simpValType (toNumber sv_1 10))) "number"))
                            (not (equal? (term (simpValType (toNumber sv_2 10))) "number"))))]
   [--> (- sv)
        ((- sv)NegationWrongOperand)
        E-AlertNegationWrongOperand
        (side-condition (not (equal? (term (simpValType sv)) "number")))
        (side-condition (not (equal? (term (simpValType (toNumber sv 10))) "number")))]
   
   [--> (sv_1 .. sv_2)
        ((sv_1 .. sv_2)StringConcatWrongOperands)
        E-AlertStringConcatWrongOperands
        (side-condition (or (and (not (equal? (term (simpValType sv_1)) "string"))
                                 (not (equal? (term (simpValType sv_1)) "number")))
                            (and (not (equal? (term (simpValType sv_2)) "string"))
                                 (not (equal? (term (simpValType sv_2)) "number")))))]
   
   [--> (\# sv)
        ((\# sv)StringLengthWrongOperand)
        E-AlertStringLengthWrongOperand
        (side-condition (not (equal? (term (simpValType sv)) "string")))]
   
   [--> (sv_1 == sv_2)
        ((sv_1 == sv_2)EqualityFail)
        E-AlertEqualityFail
        (side-condition (equal? (term (δ (== sv_1 sv_2))) (term false)))]
   
   [--> (sv_1 < sv_2)
        ((sv_1 < sv_2)LessThanFail)
        E-AlertLessThanFail
        (side-condition (or (not (equal? (term (simpValType sv_1)) "string"))
                             (not (equal? (term (simpValType sv_2)) "string"))))
        (side-condition (or (not (equal? (term (simpValType sv_1)) "number"))
                             (not (equal? (term (simpValType sv_2)) "number"))))]
   
;   [--> (sv_1 > sv_2)
;        ((sv_2 < sv_1)LessThanFail)
;        E-AlertGreaterThanFail
;        (side-condition (and (not (equal? (term (simpValType sv_1)) "string"))
;                             (not (equal? (term (simpValType sv_2)) "string"))))
;        (side-condition (and (not (equal? (term (simpValType sv_1)) "number"))
;                             (not (equal? (term (simpValType sv_2)) "number"))))]
   
   [--> (sv_1 <= sv_2)
        ((sv_1 <= sv_2)LessThanOrEqualFail)
        E-AlertLessThanOrEqualFail
        (side-condition (or (not (equal? (term (simpValType sv_1)) "string"))
                             (not (equal? (term (simpValType sv_2)) "string"))))
        (side-condition (or (not (equal? (term (simpValType sv_1)) "number"))
                             (not (equal? (term (simpValType sv_2)) "number"))))]
   
;   [--> (sv_1 >= sv_2)
;        ((sv_2 <= sv_1)LessThanOrEqualFail)
;        E-AlertGreaterThanOrEqualFail
;        (side-condition (and (not (equal? (term (simpValType sv_1)) "string"))
;                             (not (equal? (term (simpValType sv_2)) "string"))))
;        (side-condition (and (not (equal? (term (simpValType sv_1)) "number"))
;                             (not (equal? (term (simpValType sv_2)) "number"))))]
;   
   ; Built-in services
   ; Error
   [--> ($builtIn error \( (sv) \))
        (err sv)
        E-BuiltInError
        ]
   
   ; Assert
   [--> ($builtIn assert \( (sv_1 sv_2) \))
        ($builtIn error \( (any) \))
        E-BuiltInAssertFail
        (side-condition (or (eq? (term sv_1) (term false))
                            (eq? (term sv_1) (term nil))))
        (where any ,(if (eq? (term sv_2) (term nil)) 
                        (term "assertion failed!")
                        (term sv_2)))]
   
   [--> ($builtIn assert \( (sv_1 sv_2) \))
        (< (sv_1 sv_2) >)
        E-BuiltInAssertSuccess
        (side-condition (and (not (eq? (term sv_1) (term false)))
                            (not (eq? (term sv_1) (term nil)))))]
   
   ; table.pack
   [--> ($builtIn tablePack \( (sv ...) \))
        (< (any) >)
        E-BuiltInTablePack
        (where any (pack (sv ...)))]
   
   ; pcall
   [--> ($builtIn pcall \( (sv sv_1 ...) \))
        (((sv \( (sv_1 ...) \)))ProtectedMode)
        E-BuiltInPcall]
   
   ; toNumber
   [--> ($builtIn toNumber \( (sv_1 sv_2) \))
        (< (any) >)
        E-BuiltInToNumber
        (where any (toNumber sv_1 sv_2))]
   
   ; toString
   [--> ($builtIn toString \( (sv) \))
        (< (any) >)
        E-BuiltInToString
        (where any (toString sv))]
   
   ; select
   [--> ($builtIn select \( (sv_1 sv_2 ...) \))
        (< any >)
        E-BuiltInSelectNormal
        (side-condition (or (eq? (term sv_1) "#")
                             (and (eq? (term (simpValType sv_1)) "number")
                                  (or (and (<= (term sv_1) (length (term (sv_2 ...))))
                                           (<= 1 (term sv_1)))
                                      (> (term sv_1) (length (term (sv_2 ...))))
                                      (and (<= (* -1 (length (term (sv_2 ...)))) 
                                                               (term sv_1))
                                                           (<= (term sv_1) -1))))))
        (where any (funcSelect sv_1 (sv_2 ...)))]
   
   [--> ($builtIn select \( (sv_1 sv_2 ...) \))
        ($builtIn error \( ("bad argument #1 to 'select'") \))
        E-BuiltInSelectError
        (side-condition (not (or (eq? (term sv_1) "#")
                             (and (eq? (term (simpValType sv_1)) "number")
                                  (or (and (<= (term sv_1) (length (term (sv_2 ...))))
                                           (<= 1 (term sv_1)))
                                      (> (term sv_1) (length (term (sv_2 ...))))
                                      (and (<= (* -1 (length (term (sv_2 ...)))) 
                                                               (term sv_1))
                                                           (<= (term sv_1) -1)))))))]
   ))

(provide core-lang-expressions-red)
