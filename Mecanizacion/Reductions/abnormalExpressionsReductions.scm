#lang scheme
(require redex
         "../grammar.scm"
         "../Meta-functions/objStoreMetafunctions.scm"
         "../Meta-functions/metaTableMechanismMetafunctions.scm"
         "../Meta-functions/tablesMetafunctions.scm"
         "../Meta-functions/delta.scm")

(define core-lang-abnormal-expressions-red
  (reduction-relation
   core-lang
   #:domain (θ \; exp)
   ; Call over a non-function value
   
   [--> (θ \; ((sv \( svlist \))WrongFunctionCall))
        (θ \; (any \( svlist_2 \)))
        E-WrongFunctionCallWithHandler
        ; Determine if sv has a meta-table
        (where any (indexMetaTable sv "__call" θ))
        (side-condition (not (eq? (term any) (term nil))))
        (side-condition (not (eq? (term any) (term false))))
        (where svlist_2 ,(append (term (sv)) (term svlist)))
        ]
   
   [--> (θ \; ((sv \( svlist \))WrongFunctionCall))
        (θ \; ($builtIn error \( (any_2) \)))
        E-WrongFunctionCallNoHandler
        ; Determine if sv has a meta-table
        (where any (indexMetaTable sv "__call" θ))
        (side-condition (or (eq? (term any) (term nil))
                            (eq? (term any) (term false))))
        (where any_2 ,(string-append "attempt to call a "
                                    (term (type sv θ))
                                    " value"))]
   
   [--> (θ \; ((objref \[ simplevalue \])KeyNotFound))
        (θ \; (any \( (objref simplevalue) \)))
        E-KeyNotFoundWithHandlerNormal
        ; Determine if the table efectively has a meta-table
        (where any (indexMetaTable objref "__index" θ))
        (side-condition (eq? (term (type any θ)) (term "function")))
        ]
   
   [--> (θ \; ((objref \[ simplevalue \])KeyNotFound))
        (θ \; (any \[ simplevalue \]))
        E-KeyNotFoundWithHandlerRepeat
        ; Determine if the table efectively has a meta-table
        (where any (indexMetaTable objref "__index" θ))
        (side-condition (not (eq? (term any) (term nil))))
        (side-condition (not (eq? (term (type any θ)) (term "function"))))]
   
   [--> (θ \; ((objref \[ simplevalue \])KeyNotFound))
        (θ \; nil)
        E-KeyNotFoundNoHandler
        ; Determine if the table doesn't has a meta-table or
        ; its meta-table doesn't have a field with key "__index"
        (where any (indexMetaTable objref "__index" θ))
        (side-condition (eq? (term any) (term nil)))]
   
   [--> (θ \; ((simplevalue \[ simplevalue_1 \])NonTableIndexed))
        (θ \; (any \( (simplevalue simplevalue_1) \)))
        E-NonTableIndexedWithHandlerNormal
        ; Determine if simplevalue efectively has a meta-table
        (where any (indexMetaTable simplevalue "__index" θ))
        ; Determine if in that field we a have a reference to a function
        (side-condition (eq? (term (type any θ)) (term "function")))]
   
   [--> (θ \; ((simplevalue \[ simplevalue_1 \])NonTableIndexed))
        (θ \; (any \[ simplevalue_1 \]))
        E-NonTableIndexedWithHandlerRepeat
        ; Determine if simplevalue efectively has a meta-table
        (where any (indexMetaTable simplevalue "__index" θ))
        (side-condition (and (not (eq? (term any) (term nil)))
                             (not (eq? (term (type any θ)) (term "function")))))]
   
   [--> (θ \; ((simplevalue \[ simplevalue_2 \])NonTableIndexed))
        (θ \; ($builtIn error \( (any_2) \)))
        E-NonTableIndexedNoHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any (indexMetaTable simplevalue "__index" θ))
        (side-condition (eq? (term any) (term nil)))
        (where any_2 ,(string-append "attempt to index a "
                                    (term (type simplevalue θ))
                                    " value"))]
   
   ; Abnormal expressions with arithmetic operators
   [--> (θ \; ((sv_1 + sv_2)AdditionWrongOperands))
        (θ \; (any_1 \( (sv_1 sv_2) \)))
        E-AdditionWrongOperandsWithHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__add" θ))
        (side-condition (not (equal? (term any_1) (term nil))))]
   
   [--> (θ \; ((sv_1 + sv_2)AdditionWrongOperands))
        (θ \; ($builtIn error \( (any_2) \)))
        E-AdditionWrongOperandsNoHandler
        ; Determine if sv_1 or sv_2 efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__add" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (where any_2 ,(string-append 
                      "attempt to perform arithmetic on operands of the wrong type"))
        ]
   
   [--> (θ \; ((sv_1 - sv_2)SubstractionWrongOperands))
        (θ \; (any_1 \( (sv_1 sv_2) \)))
        E-SubstractionWrongOperandsWithHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__sub" θ))
        (side-condition (not (equal? (term any_1) (term nil))))]
   
   [--> (θ \; ((sv_1 - sv_2)SubstractionWrongOperands))
        (θ \; ($builtIn error \( (any_2) \)))
        E-SubstractionWrongOperandsNoHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__sub" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (where any_2 ,(string-append 
                      "attempt to perform arithmetic on operands of the wrong type"))]
   
   [--> (θ \; ((sv_1 * sv_2)MultiplicationWrongOperands))
        (θ \; (any_1 \( (sv_1 sv_2) \)))
        E-MultiplicationWrongOperandsWithHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__mul" θ))
        (side-condition (not (equal? (term any_1) (term nil))))]
   
   [--> (θ \; ((sv_1 * sv_2)MultiplicationWrongOperands))
        (θ \; ($builtIn error \( (any_2) \)))
        E-MultiplicationWrongOperandsNoHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__mul" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (where any_2 ,(string-append 
                      "attempt to perform arithmetic on operands of the wrong type"))]
   
   [--> (θ \; ((sv_1 / sv_2)DivisionWrongOperands))
        (θ \; (any_1 \( (sv_1 sv_2) \)))
        E-DivisionWrongOperandsWithHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__div" θ))
        (side-condition (not (equal? (term any_1) (term nil))))]
   
   [--> (θ \; ((sv_1 / sv_2)DivisionWrongOperands))
        (θ \; ($builtIn error \( (any_2) \)))
        E-DivisionWrongOperandsNoHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__div" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (where any_2 ,(string-append 
                      "attempt to perform arithmetic on operands of the wrong type"))]
   
   [--> (θ \; ((sv_1 ^ sv_2)ExponentiationWrongOperands))
        (θ \; (any_1 \( (sv_1 sv_2) \)))
        E-ExponentiationWrongOperandsWithHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__pow" θ))
        (side-condition (not (equal? (term any_1) (term nil))))]
   
   [--> (θ \; ((sv_1 ^ sv_2)ExponentiationWrongOperands))
        (θ \; ($builtIn error \( (any_2) \)))
        E-ExponentiationWrongOperandsNoHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__pow" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (where any_2 ,(string-append 
                      "attempt to perform arithmetic on operands of the wrong type"))]
   
   [--> (θ \; ((sv_1 % sv_2)ModuleWrongOperands))
        (θ \; (any_1 \( (sv_1 sv_2) \)))
        E-ModuleWrongOperandsWithHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__mod" θ))
        (side-condition (not (equal? (term any_1) (term nil))))]
   
   [--> (θ \; ((sv_1 % sv_2)ModuleWrongOperands))
        (θ \; ($builtIn error \( (any_2) \)))
        E-ModuleWrongOperandsNoHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__mod" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (where any_2 ,(string-append 
                      "attempt to perform arithmetic on operands of the wrong type"))]
   
   [--> (θ \; ((- sv)NegationWrongOperand))
        (θ \; (any_1 \( (sv) \)))
        E-NegationWrongOperandWithHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getUnaryHandler sv "__unm" θ))
        (side-condition (not (equal? (term any_1) (term nil))))]
   
   [--> (θ \; ((- sv)NegationWrongOperand))
        (θ \; ($builtIn error \( (any_2) \)))
        E-NegationWrongOperandNoHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getUnaryHandler sv "__unm" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (where any_2 ,(string-append "attempt to perform arithmetic on a "
                                     (term (type sv θ))
                                     " value"))]
   
   ; Abnormal expressions with string operations
   [--> (θ \; ((sv_1 .. sv_2)StringConcatWrongOperands))
        (θ \; (any_1 \( (sv_1 sv_2) \)))
        E-StringConcatWrongOperandsWithHandler
        ; Determine we have a handler for the operation
        (where any_1 (getBinHandler sv_1 sv_2 "__concat" θ))
        (side-condition (not (equal? (term any_1) (term nil))))]
   
   [--> (θ \; ((sv_1 .. sv_2)StringConcatWrongOperands))
        (θ \; ($builtIn error \( (any_2) \)))
        E-StringConcatWrongOperandsNoHandler
        ; Determine we have a handler for the operation
        (where any_1 (getBinHandler sv_1 sv_2 "__concat" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (where any_2 ,(string-append 
                      "attempt to apply string concatenation over operands of the wrong type"))]
   
   [--> (θ \; ((\# sv)StringLengthWrongOperand))
        (θ \; (any_1 \( (sv) \)))
        E-StringLengthWrongOperandWithHandler
        ; Determine we have a handler for the operation
        (where any_1 (getUnaryHandler sv "__len" θ))
        (side-condition (not (equal? (term any_1) (term nil))))]
   
   [--> (θ \; ((\# sv)StringLengthWrongOperand))
        (θ \; (δ (\# tableconstructor)))
        E-StringLengthWrongOperandTableLength
        ; Determine we have a handler for the operation
        (where any_1 (getUnaryHandler sv "__len" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (side-condition (equal? (term (type sv θ)) "table"))
        (where tableconstructor (getTable (derefTheta θ sv)))]
   
   [--> (θ \; ((\# sv)StringLengthWrongOperand))
        (θ \; ($builtIn error \( (any_2) \)))
        E-StringLengthWrongOperandNoHandler
        ; Determine we have a handler for the operation
        (where any_1 (getUnaryHandler sv "__len" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (side-condition (not (equal? (term (type sv θ)) "table")))
        (where any_2 ,(string-append "attempt to get length of a "
                                    (term (type sv θ))
                                    " value"))]
   
   ; Abnormal expressions with relational operators
   
   [--> (θ \; ((sv_1 == sv_2)EqualityFail))
        (θ \; (any \( (sv_1 sv_2) \)))
        E-EqualityFailWithHandler
        ; Determine the type of sv_1
        (where any (getEqualHandler sv_1 sv_2 θ))
        (side-condition (not (equal? (term any) (term nil))))]
   
   [--> (θ \; ((sv_1 == sv_2)EqualityFail))
        (θ \; false)
        E-EqualityFailNoHandler
        ; Determine the type of sv_1
        (where any (getEqualHandler sv_1 sv_2 θ))
        (side-condition (equal? (term any) (term nil)))]
   
   [--> (θ \; ((sv_1 < sv_2)LessThanFail))
        (θ \; (any_1 \( (sv_1 sv_2) \)))
        E-LessThanFailWithHandler
        ; Obtain a handler for the operation
        (where any_1 (getBinHandler sv_1 sv_2 "__lt" θ))
        (side-condition (not (equal? (term any_1) (term nil))))]
   
   [--> (θ \; ((sv_1 < sv_2)LessThanFail))
        (θ \; ($builtIn error \( (any_2) \)))
        E-LessThanFailNoHandler
        ; Obtain a handler for the operation
        (where any_1 (getBinHandler sv_1 sv_2 "__lt" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (where any_2 ,(string-append "attempt to compare "
                                    (term (type sv_1 θ))
                                    " with "
                                    (term (type sv_2 θ))))]
   
   [--> (θ \; ((sv_1 <= sv_2)LessThanOrEqualFail))
        (θ \; (any_1 \( (sv_1 sv_2) \)))
        E-LessThanOrEqualFailWithHandler
        ; Obtain a handler for the operation
        (where any_1 (getBinHandler sv_1 sv_2 "__le" θ))
        (side-condition (not (equal? (term any_1) (term nil))))]
   
   [--> (θ \; ((sv_1 <= sv_2)LessThanOrEqualFail))
        (θ \; (not (any_2 \( (sv_2 sv_1) \))))
        E-LessThanOrEqualFailWithAltHandler
        ; Obtain a handler for the operation
        (where any_1 (getBinHandler sv_1 sv_2 "__le" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (where any_2 (getBinHandler sv_1 sv_2 "__lt" θ))
        (side-condition (not (equal? (term any_2) (term nil))))]
   
   [--> (θ \; ((sv_1 <= sv_2)LessThanOrEqualFail))
        (θ \; ($builtIn error \( (any_3) \)))
        E-LessThanOrEqualFailNoHandler
        ; Determine if simplevalue efectively has a meta-table
        (where any_1 (getBinHandler sv_1 sv_2 "__le" θ))
        (side-condition (equal? (term any_1) (term nil)))
        (where any_2 (getBinHandler sv_1 sv_2 "__lt" θ))
        (side-condition (equal? (term any_2) (term nil)))
        (where any_3 ,(string-append "attempt to compare "
                                    (term (type sv_1 θ))
                                    " with "
                                    (term (type sv_2 θ))))]
   ))

(provide core-lang-abnormal-expressions-red)
