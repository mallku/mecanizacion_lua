#lang racket
; Black-box testing for abnormal statements

(require redex
         "../grammar.scm"
         "../Reductions/abnormalStatementsReductions.scm")

(define (abnormal-statements-red-test-suite)
  
  ; E-TableAssignmentWrongKeyNormal
  (test-->> core-lang-abnormal-statements-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__newindex" \] = (objr 6)) \}) \, nil \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ \}) \, (objr 1) \)) \))
                    (\( (objr 6) \, 
                        (function X (\( () \) ((svr 1) \( () \)) end)) \))) 
                   \; (((((objr 2) \[ 1 \])) = (2))TableAssignmentWrongKey)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__newindex" \] = (objr 6)) \}) \, nil \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ \}) \, (objr 1) \)) \))
                    (\( (objr 6) \, 
                        (function X (\( () \) ((svr 1) \( () \)) end)) \))) 
                   \; ((objr 6) \( ((objr 2) 1 2) \)))))
  
  ; E-TableAssignmentWrongKeyRepeat
  (test-->> core-lang-abnormal-statements-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__newindex" \] = (objr 3)) \}) \, nil \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ \}) \, (objr 1) \)) \))
                    (\( (objr 3) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, nil \)) \)))
                   \; (((((objr 2) \[ 1 \])) = (2))TableAssignmentWrongKey)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__newindex" \] = (objr 3)) \}) \, nil \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ \}) \, (objr 1) \)) \))
                    (\( (objr 3) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, nil \)) \))) 
                   \; ((((objr 3) \[ 1 \])) = (2)))))
  
  ; E-TableAssignmentWrongKeyNoHandler
  (test-->> core-lang-abnormal-statements-red
            (term (((\( (objr 1) \, 
                        (\( (\{ \}) \, nil \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ \}) \, (objr 1) \)) \))) 
                   \; (((((objr 2) \[ 1 \])) = (2))TableAssignmentWrongKey)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ \}) \, nil \)) \))
                    (\( (objr 2) \, 
                        (\( (\{ (\[ 1 \] = 2) \}) \, (objr 1) \)) \))) 
                   \; void)))
  
    (test-->> core-lang-abnormal-statements-red
            (term (((\( (objr 1) \, 
                        (\( (\{ \}) \, nil \)) \))) 
                   \; (((((objr 1) \[ 1 \])) = (2))TableAssignmentWrongKey)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 2) \}) \, nil \)) \))) 
                   \; void)))
  
  ; E-TableAssignOverNonTableValNormal
  (test-->> core-lang-abnormal-statements-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__newindex" \] = (objr 6)) \}) \, nil \)) \))
                    (\( (objr 6) \, 
                        (function X (\( () \) ((svr 1) \( () \)) end)) \)))
                   \; ((((1 \[ 2 \])) = (2))TableAssignOverNonTableVal)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__newindex" \] = (objr 6)) \}) \, nil \)) \))
                    (\( (objr 6) \, 
                        (function X (\( () \) ((svr 1) \( () \)) end)) \)))
                   \; ((objr 6) \( (1 2 2) \)))))
  
  ; E-TableAssignOverNonTableValRepeat
  (test-->> core-lang-abnormal-statements-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__newindex" \] = (objr 6)) \}) \, nil \)) \))
                    (\( (objr 6) \, 
                        (\( (\{ \}) \, nil \)) \)))
                   \; ((((1 \[ 2 \])) = (3))TableAssignOverNonTableVal)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ "__newindex" \] = (objr 6)) \}) \, nil \)) \))
                    (\( (objr 6) \, 
                        (\( (\{ \}) \, nil \)) \)))
                   \; ((((objr 6) \[ 2 \])) = (3)))))
  
  ; E-TableAssignOverNonTableValNoHandler
  (test-->> core-lang-abnormal-statements-red
            (term (() \; ((((1 \[ 2 \])) = (3))TableAssignOverNonTableVal)))
            
            (term (() \; ($builtIn error \( ("attempt to index a number value") \)))))
  
  (test-->> core-lang-abnormal-statements-red
            (term (((\( (objr 1) \, 
                        (\( (\{ \}) \, nil \)) \)))
                   \; ((((1 \[ 2 \])) = (3))TableAssignOverNonTableVal)))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ \}) \, nil \)) \)))
                   \; ($builtIn error \( ("attempt to index a number value") \)))))
  
  (test-results))

(provide abnormal-statements-red-test-suite)