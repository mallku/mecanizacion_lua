#lang racket
; Statements that interact with the object store

(require redex
         "../grammar.scm"
         "../Meta-functions/objStoreMetafunctions.scm"
         "../Meta-functions/misc.scm"
         "../Meta-functions/tablesMetafunctions.scm")

(define core-lang-stat-obj-store-red
  (reduction-relation
   core-lang
   ;#:domain (θ \; block)
   ; Table assignment
   [--> (θ_1 \; (((objref \[ simplevalue \])) = (simplevalue_2)))
        (θ_2 \; void)
        E-AssignTable
        ; The key is not nil
        (side-condition (not (eq? (term simplevalue) (term nil))))
        ; The value assigned is not nil
        (side-condition (not (eq? (term simplevalue_2) (term nil))))
        ; objref points to a table
        (side-condition (eq? (term (type objref θ_1)) (term "table")))
        (where tableconstructor (getTable (derefTheta θ_1 objref)))
        ; The key belongs to the table
        (side-condition (term (keyBelongsTo? tableconstructor simplevalue)))
        (where tableconstructor_2 (assignTableField tableconstructor 
                                                  simplevalue 
                                                  simplevalue_2))
        (where θ_2 (thetaAlter θ_1 objref tableconstructor_2))]
   
   [--> (θ_1 \; (((objref \[ simplevalue \])) = (nil)))
        (θ_2 \; void)
        E-DeleteTableField
        ; The key is not nil
        (side-condition (not (eq? (term simplevalue) (term nil))))
        ; objref points to a table
        (side-condition (eq? (term (type objref θ_1)) (term "table")))
        (where tableconstructor (getTable (derefTheta θ_1 objref)))
        ; The key is present in the table
        (side-condition (term (keyBelongsTo? tableconstructor simplevalue)))
        (where tableconstructor_2 (assignTableField tableconstructor 
                                                  simplevalue 
                                                  nil))
        (where θ_2 (thetaAlter θ_1 objref tableconstructor_2))]
   
   [--> (θ \; (((objref \[ nil \])) = (simplevalue)))
        (θ \; error) ; table index is nil
        E-TableAssignmentIndexNil
        (side-condition (eq? (term (type objref θ)) (term "table")))]
   
   [--> (θ \; (((objref \[ simplevalue \])) = (simplevalue_1)))
        (θ \; ((((objref \[ simplevalue \])) = (simplevalue_1))TableAssignmentWrongKey))
        E-AlertTableAssignmentWrongKey
        ; Determine if objref points to a table
        (side-condition (eq? (term (type objref θ)) (term "table")))
        ; simplevalue is not nil
        (side-condition (not (eq? (term simplevalue) (term nil))))
        ; The table doesn't have simplevalue as key
        (side-condition (not (term (keyBelongsTo? (getTable (derefTheta θ objref))
                                                  simplevalue))))]
   
   [--> (θ \; (((simplevalue \[ simplevalue_1 \])) = (simplevalue_2)))
        (θ \; ((((simplevalue \[ simplevalue_1 \])) = (simplevalue_2))TableAssignOverNonTableVal))
        E-AlertTableAssignOverNonTableVal
        ; Determine if simplevalue is not an reference pointing to a table
        (side-condition (not (eq? (term (type simplevalue θ)) (term "table"))))]
   ))

(provide core-lang-stat-obj-store-red)
