#lang racket
(require redex
         "../grammar.scm")

; Substitution function over expressions
; PARAMS:
; exp : the expression to which the substitution is applied
; parameters : a list of identifiers to be subtituted
; explist : a list of expressions to substitute the identifiers in
; parameters. The identifier in the position i of parameters will
; be replaced by the expression in the position i of expressions
(define-metafunction core-lang
  ;substExp : exp parameters explist -> exp
  
  ; Variable identifier or vararg expression
  [(substExp parameter parameters explist)
   (applySubst parameter parameters explist)]
  
  ; Function call
  [(substExp (exp \( explist_1 \)) parameters explist_2)
   ((substExp exp parameters explist_2) \( 
                                       (substexplist explist_1 parameters explist_2) 
                                       \))]
  
  [(substExp (exp : Name \( explist_1 \)) parameters explist_2)
   ((substExp exp parameters explist_2) : Name \( 
                                       (substexplist explist_1 parameters explist_2) 
                                       \))]
  ; Protected mode
  [(substExp ((exp)ProtectedMode) parameters explist_2)
   (((substExp exp parameters explist_2))ProtectedMode)]
  
  ; Built-in procedure call
  [(substExp ($builtIn Name \( explist_1 \)) parameters explist_2)
   ($builtIn Name \( (substexplist explist_1 parameters explist_2) \))]
  
  ; Operator '( ')'
  [(substExp (\( exp \)) parameters explist)
   (\( (substExp exp parameters explist) \))]
  
  ; Table indexing
  [(substExp (exp_1 \[ exp_2 \]) parameters explist)
   ((substExp exp_1 parameters explist) \[ (substExp exp_2 parameters explist) \])]
  
  ; Tuple
  ; Non empty tuple
  [(substExp (< explist_1 >) parameters explist_2)
   (< (substexplist explist_1 parameters explist_2) >)]
  
  ; Function definition
  ; We are assuming that the identifiers parlist occur in the 
  ; same order as in namelist.
  [(substExp (function Name (\( namelist_1 \) block end)) namelist_2 explist_1)
   (function Name (\( namelist_1 \) 
                 (substBlock block namelist_3 explist_2)
                 end))
   (where namelist_3 ,(term (extractDifVar namelist_2 namelist_1)))
   (where explist_2 ,(term (extractExp namelist_3 namelist_2 explist_1)))]
  
  [(substExp (function Name (\( (Name_1 ... <<<) \) block end)) namelist_2 explist_1)
   (function Name (\( (Name_1 ... <<<) \) 
                 (substBlock block namelist_3 explist_2)
                 end))
   (where namelist_3 ,(term (extractDifVar namelist_2 (Name_1 ...))))
   (where explist_2 ,(term (extractExp namelist_3 namelist_2 explist_1)))]
  
  ; When the substitution defines a substitute to a vararg expression, it is
  ; discarded
  [(substExp (function Name_1 (\( parlist \) block end)) (Name ... <<<) (exp ... exp_2))
   (substExp (function Name_1 (\( parlist \) block end)) (Name ...) (exp ...))]
  
  ; Table constructor
  ; substfield receives and returns a list of the form (field ...)
  ; so in this case, to reconstruct the original expression that has the form
  ; of a list of symbols, we must escape to scheme code an use the append
  ; function put the result of substfield with the others symbols in one list
  [(substExp (\{ field ... \}) parameters explist)
   ,(append (term (\{)) (append (term (substfield (field ...) parameters explist)) 
                                (term (\}))))]
  
  ; Binary operators
  [(substExp (exp_1 binop exp_2) parameters explist)
   ((substExp exp_1 parameters explist) binop (substExp exp_2 parameters explist))]
  
  ; Unary operators
  [(substExp (unop exp) parameters explist)
   (unop (substExp exp parameters explist))]
  
  ; These case holds for every expression without an structure, different than
  ; a variable or a vararg exp: nil, empty, boolean, number, string, 
  ; simpvalref, objref
  [(substExp any parameters explist)
   any])

; Substitution function for blocks
(define-metafunction core-lang
  substBlock : block parameters explist -> any
  
  ; Void statement
  [(substBlock void parameters explist)
   void]
  ; Protected mode
  [(substBlock ((block)ProtectedMode) parameters explist_2)
   (((substBlock block parameters explist_2))ProtectedMode)]
  ; Function call
  [(substBlock functioncall parameters explist)
   (substExp functioncall parameters explist)]
  ; Built-in procedure call
  [(substBlock ($builtIn Name \( explist \)) parameters explist)
   (substExp ($builtIn Name \( explist \)) parameters explist)]
  ; Concatenation of statements
  [(substBlock (block_1 \; block_2) parameters explist)
   ((substBlock block_1 parameters explist) \; (substBlock block_2 
                                                             parameters 
                                                             explist))]
  ; Labelled block
  [(substBlock (label \{ block \}) parameters explist)
   (label \{ (substBlock block parameters explist) \})]
  ; Block Do...End
  [(substBlock (do block end) parameters explist)
   (do (substBlock block parameters explist) end)]
  ; Break statement
  [(substBlock (break Name tuple) parameters explist)
   (break Name (substExp tuple parameters explist))]
  ; Conditional
  [(substBlock (if exp then block_1 else block_2 end) parameters explist)
   (if (substExp exp parameters explist) then (substBlock block_1 
                                                          parameters 
                                                          explist)
       else (substBlock block_2 parameters explist) end)]
  
  ; While loop
  [(substBlock (while exp do block end) parameters explist)
   (while (substExp exp parameters explist) do 
          (substBlock block parameters explist) 
          end)]
  
  ; Local statement
  [(substBlock (local namelist_1 = explist_1 in block end) namelist_2 explist_2)
   (local namelist_1 = (substexplist explist_1 namelist_2 explist_2) in 
     (substBlock block namelist_3 explist_3) end)
   (where namelist_3 ,(term (extractDifVar namelist_2 namelist_1)))
   (where explist_3 ,(term (extractExp namelist_3 namelist_2 explist_2)))]
  
  [(substBlock (local namelist_1 = explist_1 in block end) parameters explist_2)
   (local namelist_1 = (substexplist explist_1 parameters explist_2) in 
     (substBlock block parameters_2 explist_3) end)
   (where parameters_2 ,(term (extractDifVar parameters namelist_1)))
   (where explist_3 ,(term (extractExp parameters_2 parameters explist_2)))]
  
  ; Variable assignment
  [(substBlock (varlist = explist_1) parameters explist_2)
   ((substexplist varlist parameters explist_2) = (substexplist explist_1 
                                                                 parameters 
                                                                 explist_2))])


;                                                                  
;                                                                  
;                                                                  
;                              ;     ;;;       ;                   
;     ;;                               ;                           
;     ;;                               ;                           
;     ;;    ;    ;  ;;   ;   ;;;       ;     ;;;     ;;;;    ; ;;; 
;    ;  ;   ;    ;   ;  ;      ;       ;       ;         ;   ;;    
;    ;  ;   ;    ;    ;;       ;       ;       ;         ;   ;     
;   ;;;;;;  ;    ;    ;;       ;       ;       ;     ;;;;;   ;     
;   ;    ;  ;    ;    ;;       ;       ;       ;    ;    ;   ;     
;   ;    ;  ;   ;;   ;  ;      ;       ;       ;    ;   ;;   ;     
;  ;      ;  ;;; ;  ;    ;   ;;;;;   ;;;;;   ;;;;;   ;;; ;   ;     
;                                                                  
;                                                                  
;                                                                  
;                                                                  

(define-metafunction core-lang
  applySubst : parameter parameters explist -> exp
  
  [(applySubst parameter () ())
   parameter]
  
  [(applySubst parameter_1 (parameter_1 parameter_2 ...) (exp_1 exp_2 ...))
   exp_1]
  
  [(applySubst parameter_1 (parameter_2 parameter_3 ...) (exp_1 exp_2 ...))
   (applySubst parameter_1 (parameter_3 ...) (exp_2 ...))]
  )

; PARAMS:
; explist: a list of expressions
; parameters1, parameters2: list of parameters
;
; RETURNS:
; The expressions from explist, that are in correspondence with the parameters
; in parameters1, as indicated, by position, of the parameters in parameters2.
; We assume that all the parameters from parameters1 are also present in
; parameters2, and appear in the same order, and that #parameters2 = #explist.
(define-metafunction core-lang
  extractExp : parameters parameters explist -> explist
  ; Base case
  [(extractExp () parameters explist)
   ()]
  
  ; Inductive case
  
  [(extractExp (parameter_1 parameter_2 ...) (parameter_1 parameter_3 ...) 
               (exp_1 exp_2 ...))
   ,(append (term (exp_1)) (term (extractExp (parameter_2 ...) (parameter_3 ...) 
                                             (exp_2 ...))))]
  
  [(extractExp (parameter_1 ...) (parameter_2 parameter ...) (exp_1 exp_2 ...))
   (extractExp (parameter_1 ...) (parameter ...) (exp_2 ...))]
  )

; Auxiliar meta-function that extract those identifiers, from the first list
; that it receives, that don't appear in the second list that it receives
(define-metafunction core-lang
  extractDifVar : parameters parameters -> parameters
  
  [(extractDifVar parameters ())
   parameters]
  
  [(extractDifVar parameters (parameter_1 parameter ...))
   (extractDifVar (removeElem parameter_1 parameters) (parameter ...))
   ])

; Axiliar metafunction that removes every occurrence of a given identifier
; from a list of identifiers
(define-metafunction core-lang
  removeElem : parameter parameters -> parameters
  ; Base case
  [(removeElem parameter ())
   ()]
  ; Inductive case
  [(removeElem parameter_1 (parameter_1 parameter_2 ...))
  (removeElem parameter_1 (parameter_2 ...))]
  
  [(removeElem parameter_1 (parameter_2 parameter_3 ...))
   ,(append (term (parameter_2)) (term (removeElem parameter_1 
                                                   (parameter_3 ...))))])

; Auxiliar meta-function to perform a substitution over list
; of exp constructions.
(define-metafunction core-lang
  substexplist : explist parameters explist -> explist
  
  [(substexplist () parameters explist)
   ()]
  
  [(substexplist (exp_1) parameters explist)
   ((substExp exp_1 parameters explist))]
  
  [(substexplist (exp_1 exp_2 ...) parameters explist)
   ,(append (term ((substExp exp_1 parameters explist))) 
            (term (substexplist (exp_2 ...) parameters explist)))])

; Auxiliar meta-function to perform a substitution over list
; of table fields.
(define-metafunction core-lang
  substfield : (field ...) parameters explist -> (field ...)
  
  [(substfield ((\[ exp_1 \] = exp_2)) parameters explist)
   ((\[ (substExp exp_1 parameters explist) \] = (substExp exp_2 
                                                           parameters 
                                                           explist)))]
  
  [(substfield (exp) parameters explist)
   ((substExp exp parameters explist))]
  
  [(substfield (field_1 field_2 ...) parameters explist)
   ,(append (term (substfield (field_1) parameters explist)) 
            (term (substfield (field_2 ...) parameters explist)))]
  
  [(substfield () parameters explist)
   ()])

; Export subst meta-function
(provide substBlock
         substExp)