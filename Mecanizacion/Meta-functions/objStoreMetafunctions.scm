#lang racket
(require redex
         "../grammar.scm"
         "./tablesMetafunctions.scm"
         "./misc.scm")

; Access to an object store
; PRE : {the store received satisfy the invariant of representation
;        and the reference dereferenced belongs to the domain of the
;        store}
; ret = (derefTheta θ ref)
; POS : {the correspondent value mapped to the reference}
(define-metafunction core-lang
  derefTheta : θ objref -> object
  [(derefTheta (objste_1 ... (\( l_1 \, object \)) 
                         objste_2 ...) l_1)
   object]
  )

(provide derefTheta)

; Determine if a reference belongs to the domain of a store
; PRE : {the store received satisfy the invariant of representation}
(define-metafunction core-lang
  ;refBelongsToTheta : objref θ -> any
  ; Base case
  [(refBelongsToTheta objref ())
   #f]
  ; Inductive cases
  [(refBelongsToTheta objref_1 ((\( objref_1 \, object \)) objste ...))
   #t]
  
  [(refBelongsToTheta objref_1 ((\( objref_2 \, object \)) objste ...))
   (refBelongsToTheta objref_1 (objste ...))])

(provide refBelongsToTheta)

; First location in the object store where values can be stored
; (because the locations since 1 to objStoreFirstLocation-1 are
; reserved to meta-tables of types different than table)
(define objStoreFirstLocation 6)
(provide objStoreFirstLocation)

; Meta-function that generates a fresh objref. Its definition depends
; heavily on the fact that references are implicit generated only by this
; function and that we don't have any kind of garbage collection.
(define-metafunction core-lang
  freshObjRef : θ -> objref
  ; Empty Store
  [(freshObjRef ())
   (objr ,objStoreFirstLocation)]
  ; An store with at least one reference
  [(freshObjRef (objste_1 ... (\( (objr Number_1) \, object \))))
   (objr Number_2)
   (where Number_2 ,(+ (term Number_1) 1))])

(provide freshObjRef)

; To add objects to the store
(define-metafunction core-lang
  addObject : θ any -> θ
  
  [(addObject (objste ...) functiondef)
   (objste ... (\( (freshObjRef (objste ...)) \, 
                   functiondef \)))]
  
  [(addObject (objste ...) evaluatedtable)
   (objste ... (\( (freshObjRef (objste ...)) \, 
                   (newTable evaluatedtable) \)))])

(provide addObject)

; Modify a reference-value mapping on a given store
; PRE : {the store received satisfy the invariant of representation
;        and the reference belongs to the domain of the
;        store}
(define-metafunction core-lang
  ;thetaAlter : θ objref exp -> θ
  
  [(thetaAlter (objste ... (\( l_1 \, intreptable \)) objste_1 ...) l_1 
               tableconstructor)
   
   (objste ... (\( l_1 \, (modifyTable intreptable tableconstructor) \)) 
           objste_1 ...)]
  
  [(thetaAlter (objste ... (\( l_1 \, intreptable_1 \)) objste_1 ...) l_1 
               intreptable_2)
   
   (objste ... (\( l_1 \, intreptable_2 \)) 
           objste_1 ...)]
  )

(provide thetaAlter)


; Auxiliar meta-function that deals with closure storing
(define-metafunction core-lang
  addFunction : θ functiondef -> θ
  
  [(addFunction (objste ...) functiondef)
   (objste ... (\( (freshObjRef (objste ...)) \, 
                   functiondef \)))])

(provide addFunction)

; Auxiliar meta-function that deals with table storing
(define-metafunction core-lang
  addTable : θ evaluatedtable -> θ
  
  [(addTable (objste ...) evaluatedtable)
   (objste ... (\( (freshObjRef (objste ...)) \, 
                   (\( evaluatedtable \, nil \)) \)))])

(provide addTable)

; Determines if there is a closured stored with a given tag and the same body
; PRE : {θ is a proper store && functiondef is the function to be stored}
; ret = (functionIsStored? θ functiondef)
; POS : {if there is a function stored with the same name and the same body as 
;        functiondef => ret is its objref && any other case => ret == #f}
(define-metafunction core-lang
  functionIsStored? : θ functiondef -> any
  ; Base case
  [(functionIsStored? () functiondef)
   ,#f]
  ; Inductive cases
  ; A function with the same tag and the same body is already stored 
  [(functionIsStored? ((\( objref \, (function Name_1 (\( parlist_1 \) block_1 end)) \)) 
                       objste ...) 
                      (function Name_1 (\( parlist_1 \) block_1 end)))
   objref]
  
  ; A function with the same tag but different body is stored
  [(functionIsStored? ((\( objref \, (function Name_1 (\( parlist \) block_1 end)) \)) objste ...) 
                      (function Name_1 (\( parlist \) block_2 end)))
   (functionIsStored? (objste ...) 
                      (function Name_1 (\( parlist \) block_2 end)))]
  
  ; A function with different tag is stored 
  [(functionIsStored? ((\( objref \, (function Name_1 funcbody_1) \)) objste ...)
                      (function Name_2 funcbody))
   (functionIsStored? (objste ...) (function Name_2 funcbody))]
  
  ; This case is for objects that are not functions
  [(functionIsStored? ((\( objref \, object \)) objste ...) functiondef)
   (functionIsStored? (objste ...) functiondef)]
  )

(provide functionIsStored?)

; Type function
(define-metafunction core-lang
  type : any θ -> any
  
  [(type Number θ) ,(term "number")]
  [(type nil θ) ,(term "nil")]
  [(type Boolean θ) ,(term "boolean")]
  [(type String θ) ,(term "string")]
  ;[(type tuple θ) ,(term "Tuple")]
  [(type objref θ)
   (typeAux (derefTheta θ objref))])

(provide type)

(define-metafunction core-lang
  
  [(typeAux functiondef)
   "function"]
  
  [(typeAux intreptable)
   "table"])