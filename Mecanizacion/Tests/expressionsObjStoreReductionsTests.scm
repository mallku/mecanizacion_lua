#lang racket
(require redex
         "../grammar.scm"
         "../Reductions/expressionsObjStoreReductions.scm")

(define (exp-obj-store-red-test-suite)
  ; Function creation
  (test-->> core-lang-exp-obj-store-red
            (term (() \; (function X (\( () \) (Y \( () \)) end))))
            (term (((\( (objr 6) \, (function X (\( () \) (Y \( () \)) end)) \))) 
                   \; (objr 6))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, 
                        (function X (\( () \) (Y \( () \)) end)) \))) \; 
                                                              (function X 
                                                                        (\( () \) 
                                                                            (Y \( () \)) 
                                                                            end))))
            (term (((\( (objr 1) \, 
                        (function X (\( () \) (Y \( () \)) end)) \))) \; 
                                                              (objr 1))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, 
                        (function X (\( () \) ((svr 1) \( () \)) end)) \))) \; 
                                                              (function X (\( () \) 
                                                                              ((svr 2) \( () \)) 
                                                                              end))))
            (term (((\( (objr 1) \, 
                        (function X (\( () \) ((svr 1) \( () \)) end)) \))
                    (\( (objr 2) \, 
                        (function X (\( () \) ((svr 2) \( () \)) end)) \))) \; 
                                                              (objr 2))))
  
  ; Table creation
  (test-->> core-lang-exp-obj-store-red
            (term (() \; (\{ \})))
            (term (((\( (objr 6) \, (\( (\{ \}) \, nil \)) \))) \; (objr 6))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (() \; (\{ 1 (\[ 1 \] = 2) 2 (\[ 2 \] = 3) nil 4 \})))
            (term (((\( (objr 6) \, 
                        (\( (\{ (\[ 1 \] = 1) 
                                (\[ 2 \] = 2) 
                                (\[ 3 \] = nil) 
                                (\[ 4 \] = 4) \}) \, nil \)) \))) \; (objr 6))))
  
  ; Table indexing
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) 
                                (\[ 2 \] = 2) 
                                (\[ 3 \] = nil) 
                                (\[ 4 \] = 4) \}) \, nil \)) \))) 
                   \; ((objr 1) \[ 4 \])))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) 
                                (\[ 2 \] = 2) 
                                (\[ 3 \] = nil) 
                                (\[ 4 \] = 4) \}) \, nil \)) \))) \; 4)))
  
  ; E-AlertKeyNotFound
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, (objr 2) \)) \))) 
                   \; ((objr 1) \[ 2 \])))
            
            (term (((\( (objr 1) \, 
                        (\( (\{ (\[ 1 \] = 1) \}) \, (objr 2) \)) \))) 
                   \; (((objr 1) \[ 2 \])KeyNotFound))))
  ; E-AlertNonTableIndexed
  (test-->> core-lang-exp-obj-store-red
            (term (() \; (1 \[ 2 \])))
            
            (term (() \; ((1 \[ 2 \])NonTableIndexed))))
  
  ; Built-in procedures
  ; E-BuiltInGetMetaTable
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ \}) \, nil \)) \))) 
                   \; ($builtIn getMetatable \( (1 true) \))))
            
            (term (((\( (objr 1) \, (\( (\{ \}) \, nil \)) \))) 
                   \; (< ((objr 1)) >))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (() \; ($builtIn getMetatable \( (1 true) \))))
            
            (term (() \; (< (nil) >))))
  
  ; E-BuiltInType
  (test-->> core-lang-exp-obj-store-red
            (term (() \; ($builtIn type \( (1) \))))
            
            (term (() \; (< ("number") >))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ \}) \, nil \)) \))) 
                   \; ($builtIn type \( ((objr 1)) \))))
            
            (term (((\( (objr 1) \, (\( (\{ \}) \, nil \)) \))) 
                   \; (< ("table") >))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (function X (\( () \) ((svr 1) \( () \)) end)) \))) 
                   \; ($builtIn type \( ((objr 1)) \))))
            
            (term (((\( (objr 1) \, (function X (\( () \) ((svr 1) \( () \)) end)) \))) 
                   \; (< ("function") >))))
  ; E-BuiltInNext
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ \}) \, nil \)) \))) 
                   \; ($builtIn next \( ((objr 1) nil) \))))
            
            (term (((\( (objr 1) \, (\( (\{ \}) \, nil \)) \))) 
                   \; (< (nil) >))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) (\[ 3 \] = 4) \}) 
                                        \, nil \)) \))) 
                   \; ($builtIn next \( ((objr 1) 1) \))))
            
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) (\[ 3 \] = 4) \}) 
                                        \, nil \)) \))) 
                   \; (< (3 4) >))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) (\[ 3 \] = 4) \}) 
                                        \, nil \)) \))) 
                   \; ($builtIn next \( ((objr 1) 2) \))))
            
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) (\[ 3 \] = 4) \}) 
                                        \, nil \)) \))) 
                   \; ($builtIn error \( ("invalid key to 'next'") \)))))
  ; E-BuiltInRawEqual
  (test-->> core-lang-exp-obj-store-red
            (term (() 
                   \; ($builtIn rawEqual \( (true false) \))))
            
            (term (() 
                   \; (< (false) >))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (() 
                   \; ($builtIn rawEqual \( ((objr 1) 1) \))))
            
            (term (() 
                   \; (< (false) >))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (() 
                   \; ($builtIn rawEqual \( (1 1) \))))
            
            (term (() 
                   \; (< (true) >))))
  
  
  ; rawget
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, nil \)) \))) 
                   \; ($builtIn rawGet \( ((objr 1) 1) \))))
            
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, nil \)) \))) 
                   \; (< (2) >))))
  
  ; E-BuiltInRawLen
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, nil \)) \))) 
                   \; ($builtIn rawLen \( ((objr 1)) \))))
            
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, nil \)) \))) 
                   \; (< (1) >))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, nil \)) \))) 
                   \; ($builtIn rawLen \( ("asd") \))))
            
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, nil \)) \))) 
                   \; (< (3) >))))
  
  ; E-BuiltInRawSet
  ; Alter field
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, nil \)) \))) 
                   \; ($builtIn rawSet \( ((objr 1) 1 3) \))))
            
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 3) \}) \, nil \)) \))) 
                   \; (< ((objr 1)) >))))
  ; Add new field
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, nil \)) \))) 
                   \; ($builtIn rawSet \( ((objr 1) 2 3) \))))
            
            (term (((\( (objr 1) \, (\( (\{ (\[ 2 \] = 3) (\[ 1 \] = 2) \}) \, nil \)) \))) 
                   \; (< ((objr 1)) >))))
  ; setMetatable
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, nil \)) \))
                    (\( (objr 2) \, (\( (\{ \}) \, nil \)) \))) 
                   \; ($builtIn setMetatable \( ((objr 1) (objr 2)) \))))
            
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, (objr 2) \)) \))
                    (\( (objr 2) \, (\( (\{ \}) \, nil \)) \))) 
                   \; (< ((objr 1)) >))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, nil \)) \))) 
                   \; ($builtIn setMetatable \( ((objr 1) 1) \))))
            
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, nil \)) \))) 
                   \; ($builtIn error \( 
                                ("bad argument #2 to 'setmetatable' (nil or table expected)") \)))))
  
  (test-->> core-lang-exp-obj-store-red
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, (objr 2) \)) \))
                    (\( (objr 2) \, (\( (\{ (\[ "__metatable" \] = false) \}) \, nil \)) \))) 
                   \; ($builtIn setMetatable \( ((objr 1) nil) \))))
            
            (term (((\( (objr 1) \, (\( (\{ (\[ 1 \] = 2) \}) \, (objr 2) \)) \))
                    (\( (objr 2) \, (\( (\{ (\[ "__metatable" \] = false) \}) \, nil \)) \))) 
                   \; ($builtIn error \( 
                                ("cannot change a protected metatable") \)))))
  (test-results))

(provide exp-obj-store-red-test-suite)